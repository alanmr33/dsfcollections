package com.nikita.mobile.finalphase.printer;

public interface OnFinishPrint {
	public void OnFinish(String status, String message, String deviceName, String deviceMAC);
}
