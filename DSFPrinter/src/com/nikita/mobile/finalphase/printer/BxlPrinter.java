package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;
import java.util.Set;

import org.json.JSONObject;

import com.bixolon.printer.BixolonPrinter;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.service.NikitaFuseLocation;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class BxlPrinter extends MobilePrinter {
//	public static String deviceName;
//	public static String deviceMAC;
	public static String firmwareVersion;
	public static String manufacturer;
	public static String printerModel;
	public static String codePage;
	private Context context;
	static BixolonPrinter bixolonPrinter;
	private int paper = 2;
	private String company;
	private Bitmap logo;
	private HashMap<String, String> printText;
	private boolean isConnected = false;
	private boolean testConn = false;
	private boolean testPrint = false;
//	private boolean battStatus = false;	
	
	public BxlPrinter(Context context) {
		this.context = context;
		BxlPrinter.bixolonPrinter = new BixolonPrinter(context, mHandler, null);
	}

//	private void getBattStatus() {
//		if (!isThread) {
//			new Handler().postDelayed(new Runnable() {
//				public void run() {
//					BxlPrinter.bixolonPrinter.getBatteryStatus();
//					isThread = false;
//					getBattStatus();
//				}
//			}, 1*30*1000); // 30sec
//			isThread = true;
//		}
//	}
	
	private final Handler mHandler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case BixolonPrinter.MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BixolonPrinter.STATE_CONNECTING:
					Log.i("Printer", deviceName + " is connecting");
					break;

				case BixolonPrinter.STATE_CONNECTED:
					// bixolonPrinter.setPowerSavingMode(false, 3000000);
					bixolonPrinter.automateStatusBack(true);
					bixolonPrinter.getBatteryStatus();
					isConnected = true;					
					break;

				case BixolonPrinter.STATE_NONE:
					Log.i("Printer", deviceName + " is disconnected");
					//Toast.makeText(context, deviceName + " is disconnected", Toast.LENGTH_SHORT).show();
					//finishMessage("false", deviceName + " is disconnected", battery);
					disconnect();
					break;
				}
				return true;

			case BixolonPrinter.MESSAGE_DEVICE_NAME:
				deviceName = msg.getData().getString(BixolonPrinter.KEY_STRING_DEVICE_NAME);
				deviceMAC = Utility.getSetting(context, "PRINTER-MAC-ADDRESS", "");
				if (deviceName.contains("2")) {
					paper = 2;
				} else {
					paper = 3;
				}
				return true;

			case BixolonPrinter.MESSAGE_TOAST:
				// if (!testConn) {
				Toast.makeText(context, msg.getData().getString(BixolonPrinter.KEY_STRING_TOAST), Toast.LENGTH_LONG).show();
				// }
				return true;

			case BixolonPrinter.MESSAGE_READ:
				dispatchMessage(msg);
				
				if ((msg.arg1 == BixolonPrinter.PROCESS_GET_BATTERY_STATUS)) {
					if (!testConn && !testPrint) {
						Toast.makeText(context, deviceName + " is connected", Toast.LENGTH_LONG).show();
						startPrinting();
					} else if (testConn) {
						Toast.makeText(context, "Printer Connection Success", Toast.LENGTH_LONG).show();
//						finishMessage("true", "Printer Connection Success", deviceName, deviceMAC);
						testConn = false;
						disconnect();
					} else if (testPrint) {
						printDipoTestPage();
					}
				}
				return true;

			case BixolonPrinter.MESSAGE_PRINT_COMPLETE:
				finishMessage("true", "Print Complete", deviceName, deviceMAC);				
				if (testPrint) {
					testPrint = false;
					disconnect();
				}
				return true;

			case BixolonPrinter.MESSAGE_ERROR_INVALID_ARGUMENT:
				finishMessage("false", "Invalid Argument", deviceName, deviceMAC);
				return true;

			case BixolonPrinter.MESSAGE_ERROR_OUT_OF_MEMORY:
				finishMessage("false", "Out of Memory", deviceName, deviceMAC);
				return true;
			}
			return false;
			// return true;
		}
	});
	private void dispatchMessage(Message msg) {
		switch (msg.arg1) {
		case BixolonPrinter.PROCESS_GET_PRINTER_ID:
			Bundle data = msg.getData();
			Toast.makeText(context, data.getString(BixolonPrinter.KEY_STRING_PRINTER_ID), Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_GET_BS_CODE_PAGE:
			data = msg.getData();
			Toast.makeText(context, data.getString(BixolonPrinter.KEY_STRING_CODE_PAGE), Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_AUTO_STATUS_BACK:
			StringBuffer buffer = new StringBuffer(0);			
			//if (msg.arg2 == BixolonPrinter.AUTO_STATUS_COVER_OPEN) {
			if ((msg.arg2 & BixolonPrinter.AUTO_STATUS_COVER_OPEN) == BixolonPrinter.AUTO_STATUS_COVER_OPEN) {
				buffer.append("Cover is open.\n");
			}
			//if (msg.arg2 == BixolonPrinter.AUTO_STATUS_NO_PAPER) {
			if ((msg.arg2 & BixolonPrinter.AUTO_STATUS_NO_PAPER) == BixolonPrinter.AUTO_STATUS_NO_PAPER) { 
				buffer.append("Paper end sensor: no paper present.\n");
			}

			if (buffer.capacity() > 0) {
				finishMessage("false", buffer.toString(), deviceName, deviceMAC);
				Toast.makeText(context, buffer.toString(), Toast.LENGTH_SHORT).show();
			} 
			break;

		case BixolonPrinter.PROCESS_GET_BATTERY_STATUS:
			String batteryLevel = "";
			switch (msg.arg2) {
				case BixolonPrinter.STATUS_BATTERY_FULL:
//					setBatteryStatus("Battery is full");
					batteryLevel = "FULL";
					break;
				case BixolonPrinter.STATUS_BATTERY_HIGH:
//					setBatteryStatus("Battery is high");
					batteryLevel = "HIGH";
					break;
				case BixolonPrinter.STATUS_BATTERY_MIDDLE:
//					setBatteryStatus("Battery is middle");
					batteryLevel = "MIDDLE";
					break;
				case BixolonPrinter.STATUS_BATTERY_LOW:
//					setBatteryStatus("Battery is low");
					batteryLevel = "LOW";
					break;
			}
			finishMessage("battery", batteryLevel, deviceName, deviceMAC);
			Intent filter = new Intent();
		    filter.setAction("com.nikita.generator");
		    filter.putExtra("uuid"	  , "batterychanged");
		    filter.putExtra("provider", "printer");
		    Nset nBattery = Nset.newObject();
		    nBattery.setData("battery_level", batteryLevel);
		    filter.putExtra("data", nBattery.toJSON());
		    Log.i("Battery", batteryLevel);
//		    battStatus = true;
		    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter);
		    break;

		case BixolonPrinter.PROCESS_GET_STATUS:
			if (msg.arg2 == BixolonPrinter.STATUS_NORMAL) {
				Toast.makeText(context, "No error", Toast.LENGTH_SHORT).show();
				finishMessage("true", "No Error", deviceName, deviceMAC);
			} else {
				StringBuffer buffer1 = new StringBuffer();
				if ((msg.arg2 & BixolonPrinter.STATUS_COVER_OPEN) == BixolonPrinter.STATUS_COVER_OPEN) {
					buffer1.append("Cover is open.\n");
				}
				if ((msg.arg2 & BixolonPrinter.STATUS_PAPER_NOT_PRESENT) == BixolonPrinter.STATUS_PAPER_NOT_PRESENT) {
					buffer1.append("Paper end sensor: paper not present.\n");
				}

				if (buffer1.capacity() > 0) {
					finishMessage("false", buffer1.toString(), deviceName, deviceMAC);
					Toast.makeText(context, buffer1.toString(), Toast.LENGTH_SHORT).show();
				}
			}
			break;

		case BixolonPrinter.PROCESS_GET_PRINT_SPEED:
			switch (msg.arg2) {
			case BixolonPrinter.PRINT_SPEED_LOW:
				Toast.makeText(context, "Print speed: low", Toast.LENGTH_SHORT).show();
				break;
			case BixolonPrinter.PRINT_SPEED_MEDIUM:
				Toast.makeText(context, "Print speed: medium", Toast.LENGTH_SHORT).show();
				break;
			case BixolonPrinter.PRINT_SPEED_HIGH:
				Toast.makeText(context, "Print speed: high", Toast.LENGTH_SHORT).show();
				break;
			}
			break;

		case BixolonPrinter.PROCESS_GET_POWER_SAVING_MODE:
			String text = "Power saving mode: ";
			if (msg.arg2 == 0) {
				text += false;
			} else {
				text += true + "\n(Power saving time: " + msg.arg2 + ")";
			}
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_EXECUTE_DIRECT_IO:
			buffer = new StringBuffer();
			data = msg.getData();
			byte[] response = data.getByteArray(BixolonPrinter.KEY_STRING_DIRECT_IO);
			for (int i = 0; i < response.length && response[i] != 0; i++) {
				buffer.append(Integer.toHexString(response[i]) + " ");
			}

			Toast.makeText(context, buffer.toString(), Toast.LENGTH_SHORT).show();
			break;
		}
	}
	public void checkBattery() {
		if (!isConnected) {
			//String mac = this.getDeviceMAC();
			if (deviceMAC != null && !deviceMAC.equals("")) {
				bixolonPrinter.connect(deviceMAC);
			} else {
				String defMAC = Utility.getSetting(context, "PRINTER-MAC-ADDRESS", "");
				if (defMAC != null && !defMAC.equals("")) {
					bixolonPrinter.connect(defMAC);
				}
				//bixolonPrinter.findBluetoothPrinters();
			}
		} else {
			bixolonPrinter.getBatteryStatus();
		}
	}
	public void setupPrintJob(String company, HashMap<String, String> printText) {
		this.company = company;
		this.printText = printText;
		Log.i("PrintJob ", "for " + company);
	}
	public void setLogo(Bitmap logo) {
		this.logo = logo;
	}
	public void startPrinting() {
		if (!isConnected) {
			//String mac = this.getDeviceMAC();
			if (deviceMAC != null && !deviceMAC.equals("")) {
				bixolonPrinter.connect(deviceMAC);
			} else {
				String defMAC = Utility.getSetting(context, "PRINTER-MAC-ADDRESS", "");
				if (defMAC != null && !defMAC.equals("")) {
					bixolonPrinter.connect(defMAC);
				}
				//bixolonPrinter.findBluetoothPrinters();
			}
		 } else {
			if (printText.get("footer").equalsIgnoreCase("ORIGINAL&COPY")) {
				printText.put("footer", "ORIGINAL");
				this.printDipoText(true);
			} else {
				this.printDipoText(false);
			}
		}
	}
	public void printDipoText(boolean printCopy) {
		// TemplatePrinter template = new TemplatePrinter(context, company, paper);
		// print logo
		bixolonPrinter.printBitmap(logo, BixolonPrinter.ALIGNMENT_CENTER, BixolonPrinter.BITMAP_WIDTH_FULL, 88, true, false, true);

		// print title
		int attribute = 0;
		int size = 0;
		
		// print content
		paper = Integer.valueOf(printText.get("paper"));
		String toPrint = printText.get("body");
		String[] printLines = toPrint.split("\n");
		toPrint = "";
		String title = "";
		String afterTitle = "";
		String beforeFooter = "";
		boolean printedBy = false;

		for (int i = 0; i < printLines.length; i++) {
			if (i > 2) {
				if (printLines[i].contains("Printed By")) {
					printedBy = true;
				}
				if (printedBy) {
					beforeFooter += printLines[i] + "\n";
				} else {
					toPrint += printLines[i] + "\n";
				}
			} else {
				if (i == 2) {
					afterTitle = printLines[i] + "\n";
				} else if (i == 0) {
					title = printLines[i];
				}
			}
		}

		TextView headerView = new TextView(activity);
		Typeface dipoFont = Typeface.createFromAsset(context.getAssets(), "dipo.ttf");
		headerView.layout(0, 0, 256, 15);
		headerView.setTypeface(dipoFont);
		headerView.setTextSize(11);
		headerView.setGravity(Gravity.CENTER_HORIZONTAL);
		headerView.setDrawingCacheEnabled(true);
		headerView.setText(title);
		bixolonPrinter.printBitmap(headerView.getDrawingCache(), BixolonPrinter.ALIGNMENT_CENTER, BixolonPrinter.BITMAP_WIDTH_FULL, 88, true, false, true);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(afterTitle, BixolonPrinter.ALIGNMENT_CENTER, attribute, size, false);

		attribute = 0;
		size = 0;
		if (paper == 3) {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		} else if (paper == 1) {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_C;
		} else {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
		}
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(toPrint, BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(beforeFooter, BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);

		// print footer
		TextView footerView = new TextView(activity);
		// Typeface dipoFont = Typeface.createFromAsset(context.getAssets(), "dipo.ttf");
		footerView.layout(0, 0, 256, 30);
		footerView.setTypeface(dipoFont);
		footerView.setTextSize(11);
		footerView.setGravity(Gravity.CENTER_HORIZONTAL);
		footerView.setDrawingCacheEnabled(true);
		footerView.setText("---" + printText.get("footer") + "---");
		bixolonPrinter.printBitmap(footerView.getDrawingCache(), BixolonPrinter.ALIGNMENT_CENTER, BixolonPrinter.BITMAP_WIDTH_FULL, 88, true, false, true);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		// bixolonPrinter.printText("------------------------------------------", BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);
		bixolonPrinter.printText("------------------------------------------------", BixolonPrinter.ALIGNMENT_CENTER, attribute, size, false);

		bixolonPrinter.lineFeed(2, false);
		bixolonPrinter.cutPaper(true);

		if (printCopy == true) {
			printText.put("footer", "CUSTOMER COPY");
			this.printDipoText(false);
		}
	}
	public void testConnection() {
		//String mac = this.getDeviceMAC();
		testConn = true;
		if (deviceMAC != null && !deviceMAC.equals("")) {
			bixolonPrinter.connect(deviceMAC);
		} else {
			String defMAC = Utility.getSetting(context, "PRINTER-MAC-ADDRESS", "");
			if (defMAC != null && !defMAC.equals("")) {
				bixolonPrinter.connect(defMAC);
			}
			//bixolonPrinter.findBluetoothPrinters();
		}
	}
	public void printTest() {
		testPrint = true;
		if (!isConnected) {
			//String mac = this.getDeviceMAC();
			if (deviceMAC != null && !deviceMAC.equals("")) {
				bixolonPrinter.connect(deviceMAC);
			} else {
				String defMAC = Utility.getSetting(context, "PRINTER-MAC-ADDRESS", "");
				if (defMAC != null && !defMAC.equals("")) {
					bixolonPrinter.connect(defMAC);
				}
				//bixolonPrinter.findBluetoothPrinters();
			}
		 } else {
			 this.printDipoTestPage();
		 }
	}
	private void printDipoTestPage(){
		HashMap<String, String> hashmap = new HashMap<String, String>();
		hashmap.put("body"  , "PRINT A TEST PAGE");
		hashmap.put("footer", "DIPO STAR FINANCE");
		hashmap.put("paper" , "2");
		this.setupPrintJob("DSF", hashmap);
		this.setLogo(null);
		this.startPrinting();
	}
	public void disconnect() {
		if (isConnected) {
			isConnected = false;
//			testConn = false;
//			testPrint = false;
			bixolonPrinter.automateStatusBack(false);
			bixolonPrinter.disconnect();
		}
	}
//	public void setDeviceName(String deviceName) {
//		this.deviceName = deviceName;
//	}
//	public String getDeviceName() {
//		return deviceName;
//	}
//	public void setDeviceMAC(String deviceMAC) {
//		this.deviceMAC = deviceMAC;
//	}
//	public String getDeviceMAC() {
//		return deviceMAC;
//	}

}
