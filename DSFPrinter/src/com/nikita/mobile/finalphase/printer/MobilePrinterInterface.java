package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

public interface MobilePrinterInterface {
	public void setOnFinishPrint(OnFinishPrint onFinishPrint);
	public void setOnGetBatteryStatus(OnGetBatteryStatus onGetBatteryStatus);
	public void finishMessage(String status, String message, String deviceName, String deviceMAC);
	public void checkBattery();
	public void setBatteryStatus(String message);
	public void setupPrintJob(String company,HashMap<String, String> printText);
	public void startPrinting();
	public void setLogo(Bitmap logo);	
	public void disconnect();
	public void testConnection();
	public void printTest();
	public void setActivity(Activity activity);
	public void setDeviceName(String deviceName);	
	public void setDeviceMAC(String deviceMAC);
	public String getDeviceName();
	public String getDeviceMAC();	
}
