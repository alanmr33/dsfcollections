package com.nikita.mobile.finalphase.printer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.zebra.sdk.graphics.internal.ZebraImageAndroid;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class LOSPrinter extends MobilePrinter {
	public static String deviceName;
	public static String deviceMAC;
	public static String battery;
	private Activity activity;
	static ZebraPrinter printer;
	private String  company;
	private int paper=2;
	private HashSet pairedPrinters = new HashSet();
	private HashMap<String, String> printText;
	private boolean isConnected=false;
	 private Connection printerConnection;
	private Bitmap logo;
	public LOSPrinter(Activity activity){
		this.activity=activity;
	}
	public void startPrinting(){
		pairedPrinters=new HashSet();
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter == null) {
			Toast.makeText(activity,"Bluetooth is not available",Toast.LENGTH_SHORT).show();
		} else if(!mBluetoothAdapter.isEnabled()) {
			Toast.makeText(activity,"Requested that Bluetooth be enabled",Toast.LENGTH_SHORT).show();
		} else {
			Set pairedDevices = mBluetoothAdapter.getBondedDevices();
			if(pairedDevices.size() > 0) {
				Iterator var4 = pairedDevices.iterator();

				label33:
				while(true) {
					BluetoothDevice device;
					if(!var4.hasNext()) {
						break label33;
					}

					device = (BluetoothDevice)var4.next();
					pairedPrinters.add(device);
				}
			}
		}
		Set<BluetoothDevice> set = pairedPrinters;
		final String[] items = new String[pairedPrinters.size()];
		int i=0;
		for (BluetoothDevice device :
			 set) {
			items[i]=device.getAddress();
			i++;
		}
		Log.i("Devices", items.length+" device");
		new AlertDialog.Builder(activity).setTitle("Paired Bluetooth printers")
				.setItems(items, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						deviceName=items[which];
						deviceMAC=items[which];
						connectPrinter();
					}
				}).show();
	}
	public void connectPrinter(){
		if(isConnected){
			sendPrintJob();
		}else{
			printer = connect();
	        if (printer != null) {
	        	isConnected=true;
	        	setBatteryStatus("NORMAL");
	            sendPrintJob();
	        } else {
	            disconnect();
	        }
		}
	}
	public ZebraPrinter connect() {
        printerConnection = null;
        printerConnection = new BluetoothConnection(deviceMAC);

        try {
            printerConnection.open();
        } catch (ConnectionException e) {
        	finishMessage(false,"COMM Error! Disconnected");
            disconnect();
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(printerConnection);
            } catch (ConnectionException e) {
                finishMessage(false,"Unknown Printer Language");
                printer = null;
            } catch (ZebraPrinterLanguageUnknownException e) {
                finishMessage(false,"Unknown Printer Language");
                printer = null;
                disconnect();
            }
        }

        return printer;
    }
	public void disconnect() {
        try {
            if (printerConnection != null) {
                printerConnection.close();
            }
            finishMessage(false, "Not Connected");
        } catch (ConnectionException e) {
        	finishMessage(false,"COMM Error! Disconnected");
        }
    }
	public void sendPrintJob(){
		try {
			String command="! U1 setvar \"device.languages\" \"zpl\"\n";
			printerConnection.write(command.getBytes());
//			printerConnection.write("ALANMR".getBytes());
			PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();
            byte[] configLabel = getConfigLabel();
			Log.i("LEANGUAGE", "Using "+printerLanguage.name().toString());
            printerConnection.write(configLabel);
           finishMessage(true, "Print Complete");
        } catch (ConnectionException e) {
        	finishMessage(false,e.getMessage());
        } 
	}
	private byte[] getConfigLabel() {
		String title="";
		String toPrint=printText.get("body");
		String[] printLines=toPrint.split("\n");
		toPrint="";
		for(int i=0;i<printLines.length;i++){
			if(i>1){
				int yPosition=222+((i-1)*28);
				toPrint+="^FT33,"+yPosition+"^A0N,23,24^FH\\^FD"+printLines[i]+"^FS\n";
			}else{
				title=printLines[i]+"\n";
			}
		}
		String footer;
		if(printText.get("footer").contains("copy")){
			footer="^FO192,1024^GFA,01536,01536,00024,:Z64:\n" +
					"eJztzrEOgjAQgOFrmgBDA4zd4BFg6+DDNDHRjZi4MEkNu8/j2MjAazQ46GRgc2AQNZqYa59A/u2+XHIHMPcXCTBpBwr5jvbrC9HIz6EolgTtE8V4me9/MQCoFWdlXiv1HN75/VhFDXC2SZuonYaPD2OVnEAEUnZJO3z9lQcxl3KF3vHokUtzQ+57cSb1Ajk9CLNV2Mk90RVw5HANVWHzKen0zO7M4aGxu6/tTpXjrr3U4XNzrh606zEP:20A2\n";
		}else{
			footer="^FO192,1024^GFA,01536,01536,00024,:Z64:\n" +
					"eJxjYBgFgwhIYBETAGIZEsQN8JiDTRyLvYwFqQ0xAq4MMWjmM39IbvhjcLz5D5p6donaw3MM7h5nQxPn4ag4xmPw4xCKuAMDi4xMgY+MwYcDbDIsID4I2Lc//lEnwZPAIWNw4ECdRB2IDxbvf/zjn4BMgYSMwcGD1yT+gfgIr0okSNgYHDiGbq+BTIJAjcGBM3xo7jcwSDDIMThwQg5NvWRBgeAdg4N3ytDE5Q8kyPdYHDh/DM0cdocCGZkAh+MuWMKfA1MIDLBFFQjgiBKZAuzi/A+wi7MfwGH+KBgFuAEAxtw+3w==:A473";
		}
		String print="^XA\n"+
				"^MMT"+
				"^PW575\n"+
				"^LL1119"+
				"^LS0"+
				"^FO32,32^GFA,06144,06144,00064,:Z64:\n" +
				"eJztVk1oG0cUfrM/XlWbaiXQVuQgrfAppMZubyHI9piuyDES1daXxG56yjEHm4YgRRMHitCl5NReCqpPQYXEx1ACWYWWHnpZH4yPUXsyLTQb2FBRJE/frBNFslyl1qVQ9AntzJvdN998772ZXYAppphiiikmB0n8t/yqUwJFdGyKrY0DVBhgo40jtjBsBUJDPEbDe8NzRCanJ47jFNLY0XgPKhr3CO8ApPhPnHdJGTseEN6ziE98qIDhadTiv1T5MPsZvMq8Czmdt+HJgZjsLsd5OmDwBki8a0g+tOEx6K7OdN6oHrK+fNu2SyH/XpWWtb1A824DrKt552VB7YK16oP8smTJAfFIL+S/ra5USsMi5gT/0yfIv+XrLvpngMkHQP6C2N02SG0wpA7yH4Lh6rAEjQOp7+okEsVihQDeMryusRIswzpAGRfmg4b8KFv2wJJ7xJN7YPkGCQDHhnEe/yjMfaSzzgJkGFzEwLZB/h4y5ABkF+blruTKD8DwdfABx/rATNLNz6sUDKq5gUGDMlgAKEFtw3trsIaN5sKi1pNd7TJYgYGrwamH6ZsPkR95fAP8HMQYagS5DTMZ5G8f8T+QXd2AmB/D8QF/tRBPmNazHoBFtd3Akn3UzGDdBa0NGQvWUtjz4ovauuxZFqQracMD9UWhPwEmMqI3H2H8Qn7J7wj9OIXcgEwGljSGScfEfCm7MQPmHsfQGtCvYFU7+T+9kL8VWNVWIOKf8oXqKxZUD3Fm7pa1y5dozqC5xbRBQeXDCZCaLOR/6s8vuD4mH7QD0BpwIQbL6K9zN6cbd9mcAbncGQOXxvsBMBMkYRZ7gh+Mlr/cw9wiPzmElEtupO5UsBIsh5YNbZ9uGG7OSlvI/4IO8WcF/wJg8S10pJAfKw+FX4+xW7iZkPGRof/MdnR2YWHOCEPzGknc13ZxH/kXwfrIq4jaQkosgBQjnN8pr6E/BUz7PuXc3TDKaGFohvX/inrmscjbOUD+Wzi0hPzk+R/s5kURGdiJSb8Df+5eMDAIIjQD5Z9wVou+4K/Igj+Amzi+jilU957duWmJyJAgTTjZe+ZtaF3LxdD0/cOTR2Y7ADlYkt2QHyVjDGJMbj5knUzI78eAS799hxukg/oxNK9RspWkkk8mccupXc1F6V2sTgX1W/iY1rqRYrAMEKSBa1TzAjkwPCX1xl+ox1VkkV/qCn7oSu1QfwxmmMYOcDPMI/8cHMpMdndkX29Lb/iJUySrxaKD+axyT3O7omVYYB0RAdBaPmpdxKRgdCyQd30SyPzQEkdkH++zaI2FO0Fi2EbvYx8LIiJiwxrAwhbXKBqGPbxX6yegZNPqj3nBj68BleK5pm7gf5VCCX/qCsW3QRIITUJSwaYABXCo6QzWX4xlszApHCe5buIb4MiKn9ofRWVZvT45v207eWdIz6khzc4OWhLaA0B7HP+Kk0D9lOBrwHkLionRTwVR/5Fa88hgtfvNZvPo3//VBERhnIhSSTnSb+fzY8nztvggGEVU6M/+iyidDPOqKWYfHIrHE68g7phHSJiJhGke2eLW60qRWKRWr9dGJxaqI7WTEBn8WjFLgmToda5gSYyH/eb7RwKR7sn1q+YI/+lQF5Iak/OHggpvf/CfkBU1ziZ2J6P8SSJqIB4ncTBBnAnxccuL1uvHtr848kDKikMvshWOuGP8nePlB6UVqihJqqg2dX5AO2+O84ft4+n/4BzMwIz0RZQ1Pt2ePZ8dHx51hP9G0VlNptSvzHuta/vO6qaToOP4o7X7w/Nj1W/VoluRczX2bW17+3xk9mTHV3COn37vmsV8MjVTKN5bubZ7yf7E+XAs/wgijfpWLbIdib7DPouE5TH2cfNY9ZGru9/sfX22dOXq2dZmetNxNj8+Ff3QUsSFndKpgOnHj2K80KRSUMjE7FNMMcUU/1f8DQSnEsc=:83D6\n" +
//				"^FO16,14^GB546,1090,8^FS"+
				footer+
				"^FT159,166^A0N,28,28^FH\\^FD"+title+"^FS\n" +
				toPrint+
				"^PQ1,0,1,Y^XZ\n"; //zpl
//		String print="^XA^JUF^XZ^XA^JUN^XZ^XA^JUS^XZ"; // line print
        byte[] configLabel = print.getBytes();
        Log.i("TO PRINT", print);
        return configLabel;
    }
	public void setupPrintJob(String company,HashMap<String, String> printText){
		this.company=company;
		this.printText=printText;
		Log.i("Prinjob ", "for "+company);
	}
	public void setLogo(Bitmap logo){
		this.logo=logo;
	}
}
