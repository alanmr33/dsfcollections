package com.nikita.mobile.finalphase.component;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;

import org.apache.commons.lang.StringEscapeUtils;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.database.SingleRecordset;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.generator.action.MobileAction;
import com.nikita.mobile.finalphase.generator.ui.TextboxUI;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class Component {
	public final static int TYPE_LABEL=0;
	public final static int TYPE_TEXTBOX=1;
	 
	 //new data
    public interface OnActionListener {
        public boolean OnAction(NikitaControler form, Component component, String action);
    }      
    private OnActionListener actionListener;
    public void setOnActionListener(OnActionListener actionListener){
        this.actionListener=actionListener;
    }
    public OnActionListener getOnActionListener(){
        return this.actionListener;
    } 
    
	private String list="";
	 
	private void saveComponent(){
		//?
	}
	public void saveFileComponent(){
		 
	}
	
	private boolean event = true;
	protected void setActivateEvent(boolean event){
		this.event=event;
	}
	protected boolean isEventActivated(){
		return event;
	}	
	public boolean isFileComponent(){
		return false;
	}
	public static void setImageResource(ImageView img, String data){
		if (data.equals("up")) {
        	img.setImageResource(R.drawable.up);
		}else if (data.equals("down")) {
        	img.setImageResource(R.drawable.down);
		}else if (data.equals("edit")) {
        	img.setImageResource(R.drawable.edit);
		}else if (data.equals("add")) {
        	img.setImageResource(R.drawable.add);
		}else if (data.equals("delete")) {
        	img.setImageResource(R.drawable.delete);
		}else if (data.equals("remove")) {
        	img.setImageResource(R.drawable.remove);
		}else if (data.equals("copy")) {
        	img.setImageResource(R.drawable.copy);
		}else if (data.equals("run")) {
        	img.setImageResource(R.drawable.run);
		}else if (data.equals("play")) {
        	img.setImageResource(R.drawable.play);
		}else if (data.equals("view")) {
        	img.setImageResource(R.drawable.view);
		}else if (data.equals("paste")) {
        	img.setImageResource(R.drawable.paste);
		}else if (data.equals("new")) {
        	img.setImageResource(R.drawable.new1);
		}else if (data.equals("find")) {
        	img.setImageResource(R.drawable.find);
		}else if (data.equals("search")) {
        	img.setImageResource(R.drawable.search);
		}else if (data.equals("clear")) {
        	img.setImageResource(R.drawable.clear);
		}else{
		}
	}
	 
	public static String getBaseUrl(String urlinput){ 
		String base = "";
		if (Generator.getNikitaParameters().getData("INIT-MULTIUSER").equals("true")) {
			base = "/user";//userlogin
		}else{
			base = "/def";
		}
		
        if (urlinput.startsWith("http")) {
            return urlinput;
        }else if (urlinput.startsWith("/res/asset")) {
            return "/sdcard" +urlinput;
        }else if (urlinput.startsWith("/res/resource")) {
            return "/sdcard" +urlinput;
        }else if (urlinput.startsWith("/static")) {
            return "/sdcard" + urlinput;
        }else if (urlinput.startsWith("img/")) {
            return "/sdcard" +"/static/"+ urlinput; 
        }else if (urlinput.startsWith("/@+CONTEXT/")) {
            return getBaseContext() + urlinput.substring(10);
        }else if (urlinput.startsWith("@+CONTEXT/")) {
            return getBaseContext() + urlinput.substring(9);
        }else if (urlinput.startsWith("/@+BASE/")) {
            return getBaseContext() + urlinput.substring(7);
        }else if (urlinput.startsWith("@+BASE/")) {
            return getBaseContext() + urlinput.substring(6);
        }else if (urlinput.startsWith("/context/")) {
            return getBaseContext() + urlinput.substring(8);
        }else if (urlinput.startsWith("/base/")) {
            return getBaseContext() + urlinput.substring(5);
        }else if (urlinput.startsWith("/sdcard/")) {//external
        	return urlinput;
        }else{
            return urlinput;
        }
        
        
        
    }
	
	private static String getBaseContext(){
		return "/sdcard";
	}
	public int convertPixel(String val) {
		if (getNikitaComponent()!=null && getNikitaComponent().getActivity()!=null) {
			if (val.trim().toLowerCase().endsWith("dp")) {
				return Math.round(Utility.ConvertDptoPx(getNikitaComponent().getActivity(), Utility.getNumberOnlyInt(val)));
			}else if (val.trim().toLowerCase().endsWith("px")) {
				return Utility.getNumberOnlyInt(val);
			}else{
				return Utility.getInt(val);
			}
		}
		return Utility.getInt(val);		 
	}
	
	public void mobiledefWidth(){
		String width = "320dp";
		if (Utility.isTablet(getNikitaComponent().getActivity())) {
			if (Generator.getNikitaParameters().containsKey("INIT-TABLET-COMP-WIDTH")) {
				width=Generator.getNikitaParameters().getData("INIT-TABLET-COMP-WIDTH").toString();
			}
		}else{
			width = "MATCH_PARENT";
			if (Generator.getNikitaParameters().containsKey("INIT-SMARTPHONE-COMP-WIDTH")) {
				width=Generator.getNikitaParameters().getData("INIT-SMARTPHONE-COMP-WIDTH").toString();
			}
		}		
		
		if (getStyle()==null) {
			setStyle(new Style().setStyle("n-width", width));
		}else{
			if (!getStyle().getInternalObject().getData("style").containsKey("n-width")) {
				getStyle().setStyle("n-width", width);
			}
		}
	}
	public void mobileLabelOrientation(View label){
		try {
			View v = label;
			if (v instanceof View) {
				boolean vertical = true;
				String labelwidth = "120dp";
				if (Utility.isTablet(getNikitaComponent().getActivity())) {
					vertical = false;
					if (Generator.getNikitaParameters().getData("INIT-TABLET-COMP-LABEL-ORIENTATION").toString().toLowerCase().equals("vertical")) {
						vertical=true;
					}
					if (Generator.getNikitaParameters().containsKey("INIT-TABLET-COMP-LABEL-WIDTH")) {
						labelwidth=Generator.getNikitaParameters().getData("INIT-TABLET-COMP-LABEL-WIDTH").toString();
					}
				}else{
					vertical = true;
					if (Generator.getNikitaParameters().getData("INIT-SMARTPHONE-COMP-LABEL-ORIENTATION").toString().toLowerCase().equals("horizontal")) {
						vertical=false;
					}
					if (Generator.getNikitaParameters().containsKey("INIT-SMARTPHONE-COMP-LABEL-WIDTH")) {
						labelwidth=Generator.getNikitaParameters().getData("INIT-SMARTPHONE-COMP-LABEL-WIDTH").toString();
					}
				}
				 
				
				if (!vertical  & v.getParent() instanceof LinearLayout) {
					v.getLayoutParams().width=convertPixel(labelwidth);
					((LinearLayout)v.getParent()).setOrientation(LinearLayout.HORIZONTAL);
				}
			}
		} catch (Exception e) { }
	}
	 
	//add for mobile
	protected void StyleOnUI(View view){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);

		if (getStyle()==null) {
			return ;
		}		
		Nset style = getStyle().getInternalObject().getData("style");
			
		
		int width = LayoutParams.WRAP_CONTENT; int height = LayoutParams.WRAP_CONTENT;
		String  val =  style.getData("n-width").toString().trim().toUpperCase(Locale.ENGLISH);
		
		if (val.equals("100%")||val.equals("MATCH")||val.equals("FULL")||val.equals("MATCH_PARENT")||val.equals("FILL_PARENT")) {
			width=LayoutParams.MATCH_PARENT;
		}else if (val.equals("")||val.equals("AUTO")||val.equals("WRAP")||val.equals("WRAP_CONTENT")) {
			width=LayoutParams.WRAP_CONTENT;
		}else if (val.equals("")){
			//def
			if (view.getLayoutParams() != null ) {
				width = view.getLayoutParams().width ;
			}		
		}else {
			width=convertPixel(val);
		}
		
		val =  style.getData("n-height").toString().trim().toUpperCase(Locale.ENGLISH);
		if (val.equals("100%")||val.equals("MATCH")||val.equals("FULL")||val.equals("MATCH_PARENT")||val.equals("FILL_PARENT")) {
			height=LayoutParams.MATCH_PARENT;
		}else if (val.equals("AUTO")||val.equals("WRAP")||val.equals("WRAP_CONTENT")) {
			height=LayoutParams.WRAP_CONTENT;
		}else if (val.equals("") ){
			//def
			if (view.getLayoutParams() != null ) {
				height = view.getLayoutParams().height ;
			}
		}else {
			height=convertPixel(val);
		}	
		if (view.getLayoutParams() != null ) {
			view.getLayoutParams().height = height;
			view.getLayoutParams().width = width;
		}
		if (view.getLayoutParams() == null || view.getLayoutParams() instanceof MarginLayoutParams) {
			MarginLayoutParams layoutParams;
			if (view.getLayoutParams() instanceof MarginLayoutParams) {
				layoutParams = ( MarginLayoutParams) view.getLayoutParams();
			}else{
				layoutParams = new MarginLayoutParams(width,height);
			}
			view.setLayoutParams(layoutParams);
			
			int iMarginLeft = layoutParams.leftMargin;
			int iMarginTop = layoutParams.topMargin;
			int iMarginBottom = layoutParams.bottomMargin;
			int iMarginRight = layoutParams.rightMargin;
			
			val =  style.getData("n-margin").toString();
			if (val.length()>=1) {
				iMarginLeft = convertPixel(val);
				iMarginTop = iMarginLeft;
				iMarginBottom = iMarginLeft;
				iMarginRight = iMarginLeft;	
			}
			
			val =  style.getData("n-margin-left").toString();
			if (val.length()>=1) {
				iMarginLeft = convertPixel(val);			
			}
			val =  style.getData("n-margin-top").toString();
			if (val.length()>=1) {
				iMarginTop = convertPixel(val);			
			}
			val =  style.getData("n-margin-bottom").toString();
			if (val.length()>=1) {
				iMarginBottom = convertPixel(val);			
			}
			val =  style.getData("n-margin-right").toString();
			if (val.length()>=1) {
				iMarginRight = convertPixel(val);			
			}
			layoutParams.setMargins(iMarginLeft, iMarginTop, iMarginRight, iMarginBottom);
		}
		
		

		val =  style.getData("n-background-color").toString().trim();
 
		if (val.length()>=1) {
			try {	 
				if (val.startsWith("#")) {					 
					view.setBackgroundColor((int)Long.parseLong(val.substring(1), 16));
				}				
			} catch (Exception e) { Log.i("n-background-color:",e.getMessage()); }			
		}
		
		
		val =  style.getData("n-background-image").toString();
		if (val.length()>=1) {
			try {
				if (val.startsWith("/drawable/")) {
					  
				}else if (val.startsWith("/asset/")) {
					view.setBackgroundDrawable(BitmapDrawable.createFromStream( getNikitaComponent().getActivity().getAssets().open(val.substring(7)) , "background"));
				}else{
					view.setBackgroundDrawable(BitmapDrawable.createFromPath(val));
				}
				
			} catch (Exception e) {}
		}
		
		val =  style.getData("n-opacity").toString();
		if (val.length()>=1) {
			view.setAlpha(Utility.getInt(val));
		}
		
		val =  style.getData("n-padding").toString();
		if (val.length()>=1) {
			int i = Utility.getInt(val);
			view.setPadding(i, i, i, i);
		}
		
		int iPaddingLeft = view.getPaddingLeft();
		int iPaddingTop = view.getPaddingTop();
		int iPaddingBottom = view.getPaddingBottom();
		int iPaddingRight = view.getPaddingRight();
		
		val =  style.getData("n-padding-left").toString();
		if (val.length()>=1) {
			iPaddingLeft = convertPixel(val);
			iPaddingTop = iPaddingLeft;
			iPaddingBottom = iPaddingLeft;
			iPaddingRight = iPaddingLeft;	
		}
		
		val =  style.getData("n-padding-left").toString();
		if (val.length()>=1) {
			iPaddingLeft = convertPixel(val);			
		}
		val =  style.getData("n-padding-top").toString();
		if (val.length()>=1) {
			iPaddingTop = convertPixel(val);			
		}
		val =  style.getData("n-padding-bottom").toString();
		if (val.length()>=1) {
			iPaddingBottom = convertPixel(val);			
		}
		val =  style.getData("n-padding-right").toString();
		if (val.length()>=1) {
			iPaddingRight = convertPixel(val);			
		}
		view.setPadding(iPaddingLeft, iPaddingTop, iPaddingRight, iPaddingBottom);
		
		val =  style.getData("n-gravity").toString();
		if (val.length()>=1) {
				
		}
		
		
		validateFontStyle(view, style);
		
		//view.getParent()
	}
	public void validateGravity(TextView view, Nset style){
	    String  val =   style.getData("n-gravity").toString();
		if (val.length()>=1) {
			val=val.trim().toLowerCase(Locale.ENGLISH);
			if (val.equals("center")||val.equals("center center")) {					
				view.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL); 
			}else if (val.equals("bottom")||val.equals("bottom center")||val.equals("center bottom")) {
				view.setGravity(Gravity.CENTER | Gravity.BOTTOM);
			}else if (val.equals("left")||val.equals("left center")||val.equals("center left")) {
				view.setGravity(Gravity.CENTER | Gravity.LEFT);
			}else if (val.equals("right")||val.equals("right center")||val.equals("center right")) {
				view.setGravity( Gravity.CENTER | Gravity.RIGHT);
			}else if (val.equals("top")||val.equals("top center")||val.equals("center top")) {
				view.setGravity(Gravity.CENTER | Gravity.TOP);
			 
				
			}else if (val.equals("bottom left")||val.equals("left bottom")) {
				view.setGravity( Gravity.LEFT | Gravity.BOTTOM);
			}else if (val.equals("bottom right")||val.equals("right bottom")) {
				view.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
			}else if (val.equals("top left")||val.equals("left top")) {
				view.setGravity( Gravity.LEFT | Gravity.TOP);
			}else if (val.equals("top right")||val.equals("right top")) {
				view.setGravity(Gravity.RIGHT | Gravity.TOP);	

					
				
			}else  {
				view.setGravity( Utility.getInt(val));
			}			
		}			
   }
	public void validateFontStyle(View view, Nset style){
		String val =  style.getData("n-font-size").toString();
		if (val.length()>=1) {
			int size = convertPixel(val);			
			if (view instanceof TextView) {
				((TextView)view).setTextSize(size);
			}
		}
		val =  style.getData("n-font-weight").toString().toLowerCase().trim();
		if (val.length()>=1) {
			int size =  (val.equals("bold")? Typeface.BOLD: (val.equals("italic")? Typeface.ITALIC: (val.equals("bold_italic")? Typeface.BOLD_ITALIC:Typeface.NORMAL)));			
			if (view instanceof TextView) {
				((TextView)view).setTypeface(null, size);
			}
		}
		val =  style.getData("n-font-color").toString();//ARGB
		if (val.length()>=1) {
			if (view instanceof TextView) {
				try {	 
					if (val.startsWith("#")) {			
						String s = val.substring(1) ;
						if (s.length()==8) {
							((TextView)view).setTextColor((int)Long.parseLong(val.substring(1), 16));
						}else{
							((TextView)view).setTextColor((int)Long.parseLong( "ff"+val.substring(1), 16));
						}
					}				
				} catch (Exception e) { }		
			}
		}
		// Added by VS on 18/09/2017
		val =  style.getData("n-text-decoration").toString().toLowerCase().trim();
		if (val.length()>=1) {
			if (view instanceof TextView) {
				try {
					if (val.equals("underline")) {					
						((TextView)view).setPaintFlags(((TextView)view).getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG); 
					}else{
						((TextView)view).setPaintFlags(((TextView)view).getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
					}
				} catch (Exception e) { }
			}
		}
	}
	
	public NikitaControler currForm;
	protected Nset currRecordset;
	
	public NikitaControler getNikitaComponent(){
		 return currForm;
	}
	public NikitaControler getNikitaControler(){
		 return currForm;
	}
	
	public Component(NikitaControler form){
		 this.currForm=form;
	}
	
	public boolean isBusy(){
		return false;
	}
	 
	public void runOnUI(Runnable run){
		getNikitaComponent().runOnUiThread(run);
	}	
	
    public interface OnClickListener {
        public void OnClick(Component component);
    }      
    
    protected OnClickListener listener;
    private String label="";
    private String text="";
    private boolean visible=true;
    private boolean enable=true;
    private boolean mandatory=false;
    
    private String name="";
    private String type="";
    private String id="";
    private String tag="";
    private String formid="";
    private String src="";
    private Nset data = Nset.newNull();
    private Style style;
    
    
    private String tooltip="";
    private String hint="";
    
    public boolean isMandatory() {
        return mandatory;
    }
    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }
    
    public String getHint() {
        return hint;
    }
    
    public void setTooltip(String tip) {
        this.tooltip = tip;
    }
    
    public String getTooltip() {
        return tooltip;
    }
  
    public void setStyle(Style style){
        this.style=style;
    }
    public Style getStyle(){
        return this.style;
    }
    public Style getAutoInstanceStyle(){
        return this.style!=null?this.style:new Style();
    }
    public String getViewStyle(){        
        return style!=null?style.getViewStyle():"";
    }
    public String getViewClass(){        
        return style!=null?style.getViewClass():"";
    }
    public String getViewAttribut(){        
        return style!=null?style.getViewAttr():"";
    }
     
    public void setOnClickListener(OnClickListener listener){
        this.listener=listener;
    }
    
    protected OnClickListener getOnClickListener(){
        return listener;
    }
    public static String escapeSql(String s){
        return StringEscapeUtils.escapeSql(s);
    }
 
    public static String escapeHtml(String s){
        return StringEscapeUtils.escapeHtml(s);
    }
    
    public static String unescapeHtml(String s){
        return StringEscapeUtils.unescapeHtml(s);
    }
     
    StringBuffer buffered = new StringBuffer();
     

    private Component parent;
    protected void setParent(Component parent){
         this.parent=parent;
    }
    public Component getParent(){
         return this.parent;
    }
   
    private String parentname="";
    protected void setParentName(String parent){
         this.parentname=parent;
    }
    public String getParentName(){
         return this.parentname;
    }
    public void clearParentName(){
        this.parentname="";
    }
    
    private Component form ;
    private String jsid = "";
    private String newinstance = "";
    
    public void setForm(Component form){
        this.form=form;
    }
    public Component getForm(){
        return this.form;
    }
    public String getFormJsId() {
        formid=form!=null ?form.getId():"";
        newinstance=form!=null ?form.newinstance:"";
        return formid+"-"+newinstance;//eq = form.getJsId()
    }
    public String getJsId() {
        if (jsid.equals("")) {
            return getFormJsId()+"-"+id.replace("[", "-").replace("]", "-");
        }
        return jsid;
    }
    public void setInstanceId(String newinstance) {
        this.newinstance=newinstance;
    }
    public String getInstanceId() {
        return this.newinstance;
    }
    //=====================================================================//
    
    
 

    public String getFormId() {
        return formid;
    }
    
    private void setFormId(String formid) {
        this.formid = formid;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getId() {
        if (id.contains("[")&& id.contains("]")) {
            return id.substring(0,id.indexOf("["));//reservet generator
        }        
        return id;
    }
    
    private Hashtable<String, String> extratag ;
    public void setExtraTag(String key, String value) {
    	if (this.extratag==null) {
    		extratag = new Hashtable<String, String>();
    	}
        this.extratag.put(key, value);
    }
    
    public String getExtraTag(String key) {
    	if (this.extratag!=null) {
    		return this.extratag.get(key)!=null?this.extratag.get(key):"";
		}
        return "";
    }
    
    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public String getTag() {
        return this.tag;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void onChangeViewUi(){
    	
    }
    
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        onChangeViewUi();
    }
    public Nset getData() {
        return data;
    }

    public void setData(Nset data) {
        this.data = data;
        onChangeViewUi();
    }
     

    public String getLabel() {
        return label;
    }
    
    public void setLabel(String label) {
        this.label = label;
        onChangeViewUi();
    }
    
    public boolean isVisible() {
        return visible;
    }

    
    
    public void setVisible(boolean visible) {
        this.visible = visible;
        onChangeViewUi();
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
        onChangeViewUi();
    }
    
    protected String getVisibleEnable() {
        StringBuffer sb = new StringBuffer();
        if (!enable) {
            sb.append(" ndisable "); 
        }
        if (!visible) {
            sb.append(" nhidden "); 
        }        
        return sb.toString();
    }
 
 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
			 
    public Component(NikitaControler form, String id){
    	this(form);
    	setId(id);
    }
	 
	public String getType() {
		return type;
	}
	public String getDefault() {//?
		return "";
	}
	public View view;
	
	private ArrayList<View> getAllChildren(View v) {
	    if (!(v instanceof ViewGroup)) {
	        ArrayList<View> viewArrayList = new ArrayList<View>();
	        viewArrayList.add(v);
	        return viewArrayList;
	    }

	    ArrayList<View> result = new ArrayList<View>();

	    ViewGroup viewGroup = (ViewGroup) v;
	    for (int i = 0; i < viewGroup.getChildCount(); i++) {

	        View child = viewGroup.getChildAt(i);

	        ArrayList<View> viewArrayList = new ArrayList<View>();
	        viewArrayList.add(v);
	        //viewArrayList.addAll(getAllChildren(child));

	        result.addAll(viewArrayList);
	    }
	    return result;
	}
	private void setGoneChildren(View v) {
		 if (!(v instanceof ViewGroup)) {			 
		 }else{
			 ViewGroup viewGroup = (ViewGroup) v;
			 for (int i = 0; i < viewGroup.getChildCount(); i++) {
				 viewGroup.getChildAt(i).setVisibility(View.GONE);
			 }
		 }
			 
	}
	private View internalview ;
	public View startCreateUI(NikitaControler form){
		if (replace!=null && form.getActivity() instanceof NikitaActivity) {
			View parent = new FrameLayout(form.getActivity());
			//parent.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			
			replace.setActivity((NikitaActivity)form.getActivity(), parent);
			if (replace.isLoaded()) {
				replace.onCreateUI();
			}else{
				replace.setOnLoadListener(new Runnable() {
					public void run() {
						replace.onCreateUI();						
					}
				});
			}
			
			 
			internalview = parent;
		}else{
			internalview = onCreate(form);
		}
		return internalview;
	}
	public View onCreate(NikitaControler form){
		view = new View(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		  
		
		return view;
	}
	
	private NikitaControler replace ;
	public void setReplaceComponent(NikitaControler component){
		replace=component; 
	}
	public void replaceCreateUI(){
		Log.i("Nikita", "replaceCreateUI");
		if (internalview instanceof LinearLayout || internalview instanceof FrameLayout) {		
			setGoneChildren(internalview);
			Log.i("Nikita", "replaceCreateUI.ViewGroup");
			View parent = new FrameLayout(getNikitaComponent().getActivity());
			replace.setActivity((NikitaActivity)getNikitaComponent().getActivity(), parent);
			replace.onCreateUI();	
			((ViewGroup) internalview).addView(parent);
		}
	}
	
	public View getView(){
		return view;
	}
	protected View setView(View v){
		view = v;
		return view;
	}
	  
	public void setList(String list){
		this.list=list;
	}
	public void setType(String type){
		this.type=type;
	}	
	private String def = "";
	
	public boolean getVisible(){
		return visible;
	}
	public boolean getEnable(){
		return enable;
	}
	 
	public String getList(){
		return list;
	}
	private String validation="";
    protected void setValidation(String validation){
         this.validation=validation;
    }
    public String getValidation(){
         return this.validation;
    }
    
	public void setFocus(){
		 
	}
	public boolean runRoute() {		
		return runRouteAction("click");
	}
	public boolean runRouteAction(String action) {
		return runRouteAction(action, true);
	}
	public boolean runRouteAction(String action, boolean iscomp) {		
		if (iscomp && actionListener!=null) {	
			AppNikita.setUserInteraction();
			if (!actionListener.OnAction(getNikitaComponent(), this, action)) {
				return false;
			}
		}
		return runActionListener(action, iscomp);
	}
	protected boolean defaultshowBusy() {
		return true;
	}
	private boolean runActionListener(String action, boolean iscomp) {
		boolean result = false;		 
		if (!Thread.currentThread().getName().equalsIgnoreCase("main")) {
			boolean showbusy = true;
			if (getStyle()!=null ) {
				if (getStyle().getInternalStyle().getData("n-event-"+action+"-busy").toString().equalsIgnoreCase("false")) {
					showbusy = false;
				}
			}		
			if (showbusy) {
				getNikitaComponent().showBusywithDelay(500);
			}			
		} 
		
		if (iscomp) {
			getNikitaComponent().setInterupt(false);
			NCore.actionActivityTimeout();
			
			getNikitaComponent().setVirtualRegistered("@+ACTION", action);			
			
		}
		
	    int ir = getNikitaComponent().getCurrentRow();
        int lr = getNikitaComponent().getLoopCount();
        boolean b =getNikitaComponent().expression;
		
		
		if ( action.startsWith("item-")) {
            String row = action.substring(5);
            if (row.contains("-")) {
                row=row.substring(0,row.indexOf("-"));
            }                    
            getNikitaComponent().updateParameter("item-row", row);
            getNikitaComponent().setVirtualRegistered("@+SELECTEDROW", row);
        }
		
		NikitaConnection nc = Generator.getConnection(NikitaConnection.LOGIC);
		//Nikitaset nikitaset = nc.Query("SELECT * FROM web_route  WHERE compid = ? ORDER BY routeindex ASC;", getId());
		Nikitaset nikitaset = getNikitaComponent().getNikitaEngine().getComponentLogic(getId());
		if (nikitaset.getRows()>=1 ) {			 
			 getNikitaComponent().setVirtual("@EXPESSION", false);
			 getNikitaComponent().expression=false;
	         getNikitaComponent().setLoopCount(0);
	         
	         for (int i = 0; i < nikitaset.getRows(); i++) {
	            	getNikitaComponent().setCurrentRow(i);
	            	getNikitaComponent().setVirtualRegistered("@+LOGICCOUNT", getNikitaComponent().getLoopCount());
	            	getNikitaComponent().setVirtualRegistered("@+LOGICCOUNTB1", getNikitaComponent().getLoopCount()+1);
	            	
	                if (!getNikitaComponent().isInterupted() ) {  
	                    Nset expr = Nset.readJSON(nikitaset.getText(i, "expression"));
	                    Nset actn = Nset.readJSON(nikitaset.getText(i, "action"));
	                    if ( expr.get("flag").toString().equals("hide")||(expr.get("class").toString().equals("") && actn.get("class").toString().equals("")) ) {
	                        //abaikan dan lanjut next logic [flag=hide or exxpresion=accion=emtyclass]
	                    }else if ( (actn.get("class").toString().equals("")&& actn.get("code").toString().equals("") && !actn.get("args").get("param1").toString().equals(""))  ) {
	                        //abaikan dan lanjut next logic [comment dan commnet tidak kosong, alias ada sisinya]
	                    }else if ( Generator.runExpressionClass(expr, Component.this)) {
	                    	getNikitaComponent().expression=true;
	                        getNikitaComponent().setVirtual("@EXPESSION", true);    
	                        getNikitaComponent().setVirtual(expr.getData("result").toString(), true);
	                       
	                        if (!Generator.runActionClass(actn,  Component.this)) {
	                            break;
	                        }
	                        if (getNikitaComponent().getCurrentRow()!=i) {
	                            i = getNikitaComponent().getCurrentRow()-1;
	                            getNikitaComponent().expression=false;
	                            getNikitaComponent().setVirtual("@EXPESSION", false);
	                            getNikitaComponent().setVirtual(expr.getData("result").toString(), false);
	                        }                        
	                    }else{
	                    	getNikitaComponent().expression=false;
	                        getNikitaComponent().setVirtual("@EXPESSION", false);
	                        getNikitaComponent().setVirtual(expr.getData("result").toString(), false);
	                    }            
	                } 
	        } 	
	        result = true;
        }
		
		getNikitaComponent().expression=b;
		getNikitaComponent().setCurrentRow(ir);
		getNikitaComponent().setLoopCount(lr);
		getNikitaComponent().setVirtualRegistered("@+LOGICCOUNT", getNikitaComponent().getLoopCount());
		getNikitaComponent().setVirtualRegistered("@+LOGICCOUNTB1", getNikitaComponent().getLoopCount()+1);   
         
		if (!Thread.currentThread().getName().equalsIgnoreCase("main")) {
			getNikitaComponent().showBusy(false);
		}
		return result;
	}
	
	public void onSaveState() {
		if (replace!=null) {
			replace.onSaveState();
		}
	}
	 
	private void onRestoreState() {}
	
	 
}
