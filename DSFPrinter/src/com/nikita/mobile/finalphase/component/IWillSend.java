package com.nikita.mobile.finalphase.component;

public interface IWillSend {
	public boolean onWillSend();	
}
