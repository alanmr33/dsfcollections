package com.nikita.mobile.finalphase.component;

import com.nikita.mobile.finalphase.database.SingleRecordset;

public interface IValidation {
	public String onValidation (Component comp, SingleRecordset data);
}
