package com.nikita.mobile.finalphase.component;

 

import android.widget.Button;

import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.ui.AccordionUI;
import com.nikita.mobile.finalphase.generator.ui.ButtonUI;
import com.nikita.mobile.finalphase.generator.ui.CameraUI;
import com.nikita.mobile.finalphase.generator.ui.CheckboxUI;
import com.nikita.mobile.finalphase.generator.ui.CollapsibleUI;
import com.nikita.mobile.finalphase.generator.ui.ComboboxUI;
import com.nikita.mobile.finalphase.generator.ui.CombolistUI;
import com.nikita.mobile.finalphase.generator.ui.ContentUI;
import com.nikita.mobile.finalphase.generator.ui.DateUI;
import com.nikita.mobile.finalphase.generator.ui.DocumentUI;
import com.nikita.mobile.finalphase.generator.ui.FileUploadUI;
import com.nikita.mobile.finalphase.generator.ui.ImageUI;
import com.nikita.mobile.finalphase.generator.ui.LabelUI;
import com.nikita.mobile.finalphase.generator.ui.ListViewUI;
import com.nikita.mobile.finalphase.generator.ui.MapViewUI;
import com.nikita.mobile.finalphase.generator.ui.RadioboxUI;
import com.nikita.mobile.finalphase.generator.ui.ReceiverUI;
import com.nikita.mobile.finalphase.generator.ui.TableGridUI;
import com.nikita.mobile.finalphase.generator.ui.TextAutoCompleteUI;
import com.nikita.mobile.finalphase.generator.ui.TextareaUI;
import com.nikita.mobile.finalphase.generator.ui.TextboxUI;
import com.nikita.mobile.finalphase.generator.ui.TimeUI;
import com.nikita.mobile.finalphase.generator.ui.WebViewUI;
import com.nikita.mobile.finalphase.generator.ui.layout.BorderLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.DivLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.FrameLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.GridLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.HorizontalLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.nikita.mobile.finalphase.generator.ui.layout.TabLayout;
import com.nikita.mobile.finalphase.generator.ui.layout.VerticalLayout;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;

public class ComponentManager {

	public static Component createComponent(NikitaControler form, Nikitaset ns, int row) {
		Component comp = null;
		
		if (ns.getText(row, "comptype").equals("button")) {
            ButtonUI btn = new ButtonUI(form);  
            comp = btn;
        }else if (ns.getText(row, "comptype").equals("label")) {
            LabelUI lbl = new LabelUI(form);
            comp = lbl;
        }else if (ns.getText(row, "comptype").equals("txt")||ns.getText(row, "comptype").equals("text")) {
            TextboxUI tBox = new TextboxUI(form);
            
            tBox.showFinder(Utility.getInt(ns.getText(row, "countid"))>1);
            comp = tBox;
        }else if (ns.getText(row, "comptype").equals("checkbox")) {
            CheckboxUI cBox = new CheckboxUI(form);
            comp = cBox;
        }else if (ns.getText(row, "comptype").equals("radiobox")) {
            comp =  new RadioboxUI(form);
        }else if (ns.getText(row, "comptype").equals("collapsible")) {
            CollapsibleUI collaps = new CollapsibleUI(form);
            comp = collaps;
        }else if (ns.getText(row, "comptype").equals("listview")) {
            comp = new ListViewUI(form);;
        }else if (ns.getText(row, "comptype").equals("combobox")) {
            comp =  new ComboboxUI(form);;
         }else if (ns.getText(row, "comptype").equals("datetime")) {
             comp =  new DateUI(form);;
         }else if (ns.getText(row, "comptype").equals("time")) {
             comp =  new TimeUI(form);;
        }else if (ns.getText(row, "comptype").equals("combolist")) {
            CombolistUI comList = new CombolistUI(form);
            comp = comList;
        }else if (ns.getText(row, "comptype").equals("formlayout")) {
        	comp = new NikitaForm(form);
       
        }else if (ns.getText(row, "comptype").equals("framelayout")) {
            FrameLayout framLay = new FrameLayout(form);
            comp = framLay;          
        }else if (ns.getText(row, "comptype").equals("verticallayout")) {
            VerticalLayout vLay = new VerticalLayout(form);
            comp = vLay;
        }else if (ns.getText(row, "comptype").equals("horizontallayout")) {
            HorizontalLayout hLay = new HorizontalLayout(form);
            comp = hLay;
         }else if (ns.getText(row, "comptype").equals("gridlayout")) {
            GridLayout hLay = new GridLayout(form);
            comp = hLay;
        }else if (ns.getText(row, "comptype").equals("image")) {
            ImageUI img = new ImageUI(form);
            comp = img;
        }else if (ns.getText(row, "comptype").equals("area")) {
            TextareaUI tArea = new TextareaUI(form);
            comp = tArea;
        }else if (ns.getText(row, "comptype").equals("tablegrid")) {
            comp =  new TableGridUI(form);
        }else if (ns.getText(row, "comptype").equals("borderlayout")) {
            comp = new BorderLayout(form);
        }else if (ns.getText(row, "comptype").equals("map")) {
            comp =  new MapViewUI(form);
        }else if (ns.getText(row, "comptype").equals("webview")) {
            comp =  new WebViewUI(form);
        }else if (ns.getText(row, "comptype").equals("file")) {
            comp =  new FileUploadUI(form);
        }else if (ns.getText(row, "comptype").equals("camera")) {
            comp =  new CameraUI(form);
        }else if (ns.getText(row, "comptype").equals("content")) {
            comp = new ContentUI(form);   
        }else if (ns.getText(row, "comptype").equals("tablayout")) {
            comp = new TabLayout(form);  
        }else if (ns.getText(row, "comptype").equals("txtauto")) {
            comp = new TextAutoCompleteUI(form);   
        }else  if (ns.getText(row, "comptype").startsWith("accordionmenu")) {
            comp = new AccordionUI(form); 
        }else  if (ns.getText(row, "comptype").startsWith("com.")) {
            try {
                String classname = ns.getText(row, "comptype");
                comp = (Component) Class.forName(classname).newInstance();               
            } catch (Exception e) {    }
        }else  if (ns.getText(row, "comptype").startsWith("div")) {
            comp = new DivLayout(form);
        }else  if (ns.getText(row, "comptype").startsWith("navreceiver")) {
            comp = new ReceiverUI(form);      
        }else  if (ns.getText(row, "comptype").startsWith("document")) {
            comp = new DocumentUI(form);      
        } else{
            comp = new Component(form);
        }
		 
		
		
		
		
		
		if (comp != null) {
            //comp.setFormId(ns.getText(row, "formid")); Not used
            comp.setId(ns.getText(row, "compid"));
            comp.setName(ns.getText(row, "compname"));
            comp.setLabel(ns.getText(row, "complabel"));
            comp.setText(ns.getText(row, "comptext"));
            comp.setHint(ns.getText(row, "comphint"));            
            comp.setParentName(ns.getText(row, "parent"));
            if (ns.getText(row, "visible").equals("1")) {
                comp.setVisible(true);
            }else {
                comp.setVisible(false);
            }
            if (ns.getText(row, "enable").equals("1")) {
                comp.setEnable(true);
            }else {
                comp.setEnable(false);
            }
            if (ns.getText(row, "mandatory").equals("1")) {
                comp.setMandatory(true);
            }else {
                comp.setMandatory(false);
            }
            comp.setValidation(ns.getText(row, "validation"));
        }
        return comp != null ? comp:new Component(form);
	}

}
