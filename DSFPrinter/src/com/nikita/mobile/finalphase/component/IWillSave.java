package com.nikita.mobile.finalphase.component;

public interface IWillSave {
	public void onWillSave();
}
