package com.nikita.mobile.finalphase.database;

import java.util.Vector;

import android.database.Cursor;

//depreketed
public class ANikitaset implements Recordset {
	final Vector<String> header = new Vector<String>();
	final Vector<Vector<String>> data = new Vector<Vector<String>>();

	public void addHeader(String text) {
		header.addElement(text);
	}

	public void addHeader(String... headers) {
		for (int i = 0; i < headers.length; i++) {
			header.addElement(headers[i]);
		}
	}

	public void addRow(String... fields) {
		Vector<String> field = new Vector<String>();
		for (int i = 0; i < header.size(); i++) {
			if (i > fields.length) {
				field.addElement("");
			} else {
				field.addElement(fields[i]);
			}
		}
		data.addElement(field);
	}

	public ANikitaset() {

	}

	public String getText(int row, String colname) {
		return getText(row, header.indexOf(colname));
	}

	public String getText(int row, int col) {
		try {
			return data.elementAt(row).elementAt(col);
		} catch (Exception e) {
		}
		return "";
	}

	public int getRows() {
		return data.size();
	}

	public String getHeader(int col) {
		return header.elementAt(col);
	}

	public int getCols() {
		return header.size();
	}

	@Override
	public Vector<String> getAllHeaderVector() {
		return header;
	}

	public static Recordset getRecordset(Cursor curr) {
		final Vector<String> header = new Vector<String>();
		final Vector<Vector<String>> data = new Vector<Vector<String>>();
		try {
			for (int i = 0; i < curr.getColumnCount(); i++) {
				header.addElement(curr.getColumnName(i));
			}

			while (curr.moveToNext()) {
				Vector<String> field = new Vector<String>();
				for (int i = 0; i < curr.getColumnCount(); i++) {
					field.addElement(curr.getString(i));
				}
				data.addElement(field);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Recordset() {
			public String getText(int row, String colname) {
				return getText(row, header.indexOf(colname));
			}

			public String getText(int row, int col) {
				try {
					return data.elementAt(row).elementAt(col);
				} catch (Exception e) {
				}
				return "";
			}

			public int getRows() {
				return data.size();
			}

			public String getHeader(int col) {
				return header.elementAt(col);
			}

			public int getCols() {
				return header.size();
			}

			@Override
			public Vector<String> getAllHeaderVector() {
				return header;
			}

			@Override
			public Vector<String> getRecordFromHeader(String colname) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Vector<Vector<String>> getAllDataVector() {
				// TODO Auto-generated method stub
				return data;
			}
		};
	}

	@Override
	public Vector<String> getRecordFromHeader(String colname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector<Vector<String>> getAllDataVector() {
		// TODO Auto-generated method stub
		return data;
	}

}
