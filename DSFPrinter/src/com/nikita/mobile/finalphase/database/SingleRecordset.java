package com.nikita.mobile.finalphase.database;

import java.util.Vector;

import com.nikita.mobile.finalphase.stream.Model;

import android.content.ContentValues;

public class SingleRecordset {
	Recordset rst;int currRow=-1;
	public static SingleRecordset get(int row, Recordset rst){
		return new SingleRecordset(row, rst);
	}
	public SingleRecordset(int row, Recordset rst){
		this.currRow=row;
		this.rst=rst;
	}
	public SingleRecordset(String...fieldwithvalue){
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> cols = new Vector<String>();
		Vector<String> dats = new Vector<String>();
		for (int i = 0; i < fieldwithvalue.length; i++) {
			int key = fieldwithvalue[i].indexOf("=");
		
			if (key!=-1) {
				cols.addElement(fieldwithvalue[i].substring(0, key));
				dats.addElement(fieldwithvalue[i].substring(key+1));
			}
		}
		data.addElement(dats);
		this.rst = new Model(cols, data);
		this.currRow=0;
	}
	public int getCols(){
		return rst.getCols();
	}
	public String getText(int col){
		return rst.getText(currRow, col);
	}
	public String getText(String colname){
		return rst.getText(currRow, colname);
	}	
	public String getHeader(int col){
		return rst.getHeader(col);
	}
	public Recordset getRecordset(){
		return rst;
	}
}
