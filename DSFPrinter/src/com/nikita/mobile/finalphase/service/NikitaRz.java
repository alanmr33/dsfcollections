package com.nikita.mobile.finalphase.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.ui.ReceiverUI;
import com.nikita.mobile.finalphase.service.helper.WebSocketClient;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

public class NikitaRz extends IntentService implements WebSocketClient.Listener{
	static private WebSocketClient webSocketClient;
	public NikitaRz() {
		super("NikitaRz");		
		Log.i("NikitaRz",  Thread.currentThread().getName());
	}
	public static void send(String msg){
		Set<Thread> th = Thread.getAllStackTraces().keySet();
		Thread[] ath = th.toArray(new Thread[th.size()]);
        for (int i = 0; i < ath.length; i++) {
			if (ath[i].getName().equalsIgnoreCase("IntentService[NikitaRz]")) {
				if (webSocketClient!=null && !webSocketClient.isClosed()) {
					webSocketClient.send(msg);
				}
				return;
			}
        }
		NikitaRz.startNikitaWebSocket( AppNikita.getInstance());
	}
	@Override
	protected void onHandleIntent(Intent intent) {		
		Log.i("NikitaWebSocket onHandleIntent", Thread.currentThread().getName());		 
		if (Generator.getNikitaParameters().containsKey("INIT-NIKITARZ")) {
			try {
				String s = Generator.getNikitaParameters().getData("INIT-NIKITARZ").toString();
				if (s.toLowerCase().contains("/clienthost/")) {
					s = s.replace("/clienthost/", "/" + new URI(AppNikita.getInstance().getBaseUrl()).getHost() + "/");
				}
				if (s.contains("?")) {
					s=s+"&receiver="+Utility.urlEncode("MOBILE-PLAY-"+AppNikita.getInstance().getDeviceId())+"&receiver="+Utility.urlEncode("NIKITA-MOBILE")+"&receiver="+Utility.urlEncode("NIKITA-MOBILE-"+AppNikita.getInstance().getDeviceId());
				}else{
					s=s+"?receiver="+Utility.urlEncode("MOBILE-PLAY-"+AppNikita.getInstance().getDeviceId())+"&receiver="+Utility.urlEncode("NIKITA-MOBILE")+"&receiver="+Utility.urlEncode("NIKITA-MOBILE-"+AppNikita.getInstance().getDeviceId());
				}
				 
				
				webSocketClient = new WebSocketClient(new URI(s), this, null) ;
				webSocketClient.connectOnThisThread();
			} catch (URISyntaxException e) {			
				e.printStackTrace();
			}
		}
	}
	public static void restartNikitaWebSocket(Context context){
		 Log.i("NikitaWebSocket restartNikitaWebSocket", Thread.currentThread().getName());
		 Intent service= new Intent(context, NikitaRz.class);		 
		 context.stopService(service);
		 
		 startNikitaWebSocket(context);
	}
	public static void startNikitaWebSocket(Context context){
		Intent service= new Intent(context, NikitaRz.class);	
		context.startService(service);
	}
	
	
	@Override
	public void onConnect() {
		Log.i("Nikita NR onConnect", Thread.currentThread().getName());			
	}
	@Override
	public void onMessage(String message) {
		Log.i("Nikita NR:"+Thread.currentThread().getName(), message);
		
		if (message.startsWith("{")||message.startsWith("[")) {
            Nset nV = Nset.readJSON(message);
            if (nV.getData("action").toString().equalsIgnoreCase("broadcast")) {
            	String receiver = nV.getData("receiver").toString();
            	String msgData = nV.getData("message").toString();
            	String[] nckeys =  AppNikita.getInstance().getNikitaControlerNames();
                for (int i = 0; i < nckeys.length; i++) {
                	NikitaControler nikitaControler =  AppNikita.getInstance().getNikitaComponent(nckeys[i]);
                	for (int j = 0; j < nikitaControler.getContent().getComponentCount(); j++) {
    					if (nikitaControler.getContent().getComponent(j) instanceof ReceiverUI) {
    						if (nikitaControler.getContent().getComponent(j).getText().equals(receiver)) {
    							nikitaControler.setVirtualRegistered("@+RESULT", Nset.readJSON(msgData));
    							nikitaControler.runOnActionThread(new Runnable() {
    								Component component;
    								public Runnable get(Component component){
    									this.component=component;
    									return this;
    								}
									public void run() { 
										component.runRoute();
									}
								}.get(nikitaControler.getContent().getComponent(j)));
    						}
    					}
    				}
    			}
            }else if (nV.getData("action").toString().equalsIgnoreCase("listener")) {
                String receiver = nV.getData("receiver").toString();  
                String msgData = nV.getData("message").toString(); 
                if (receiver.equalsIgnoreCase("MOBILE-PLAY-"+AppNikita.getInstance().getDeviceId())) {
                	
					DevActivity.newDebugMode( nV.getData("message").toString());
				}
                if (receiver.equals("NIKITA-MOBILE")||receiver.equalsIgnoreCase("NIKITA-MOBILE-"+AppNikita.getInstance().getDeviceId())) {
                	//receiver callback, location                	
                	Nset n  = Nset.readJSON(msgData);
                	final String recCallback = n.getData("receiver").toString();
                	if (n.getData("MODE").toString().equalsIgnoreCase("gps")) {
                		NikitaFuseLocation	nikitaFuseLocation = new NikitaFuseLocation(getApplicationContext(), new NikitaFuseLocation.OnLocationListener() {
            				public void onLocationChanged(Location location) { }            	 
            				public void onLocationDone(Location location, int status) {
            					if (location!=null) {	
            						Nset nData = Nset.newObject();
            						nData.setData("action", "listener");
            						nData.setData("receiver", recCallback);
            						nData.setData("message", NikitaFuseLocation.locationToNset(location, status).toJSON());
            						if (webSocketClient.isClosed()) {
            							restartNikitaWebSocket(getApplicationContext());
									}
            						webSocketClient.send(nData.toJSON());
            					}            					
            				}
            			});
					}else if (n.getData("MODE").toString().equalsIgnoreCase("track")) {
						long timeout = 60000; //1minute
						if (n.containsKey("timeout")) {
							timeout = n.getData("timeout").toLong()*10000;
						}
						NikitaFuseLocation	nikitaFuseLocation = new NikitaFuseLocation(getApplicationContext(), new NikitaFuseLocation.OnLocationListener() {
            				public void onLocationChanged(Location location) {
            					if (location!=null) {	
            						Nset nData = Nset.newObject();
            						nData.setData("action", "listener");
            						nData.setData("receiver", recCallback);
            						nData.setData("message", NikitaFuseLocation.locationToNset(location, 0).toJSON());
            						if (webSocketClient.isClosed()) {
            							startNikitaWebSocket(getApplicationContext());
									}
            						webSocketClient.send(nData.toJSON());
            					}   
            				}            	 
            				public void onLocationDone(Location location, int status) {
            					if (location!=null) {	
            						Nset nData = Nset.newObject();
            						nData.setData("action", "listener");
            						nData.setData("receiver", recCallback);
            						nData.setData("message", NikitaFuseLocation.locationToNset(location, status).toJSON());
            						if (webSocketClient.isClosed()) {
            							startNikitaWebSocket(getApplicationContext());
									}
            						webSocketClient.send(nData.toJSON());
            					}            					
            				}
            			},timeout, true);
					}
				} else{
					String[] nckeys =  AppNikita.getInstance().getNikitaControlerNames();
	                for (int i = 0; i < nckeys.length; i++) {
	                	NikitaControler nikitaControler =  AppNikita.getInstance().getNikitaComponent(nckeys[i]);
	                	for (int j = 0; j < nikitaControler.getContent().getComponentCount(); j++) {
	    					if (nikitaControler.getContent().getComponent(j) instanceof ReceiverUI) {
	    						if (nikitaControler.getContent().getComponent(j).getText().equals(receiver)) {
	    							nikitaControler.setVirtualRegistered("@+RESULT", Nset.readJSON(msgData));
	    							nikitaControler.runOnActionThread(new Runnable() {
	    								Component component;
	    								public Runnable get(Component component){
	    									this.component=component;
	    									return this;
	    								}
										public void run() { 
											component.runRoute();
										}
									}.get(nikitaControler.getContent().getComponent(j)));
	    							
	    						}
	    					}
	    				}
	    			}
				}
 
                
            }else if (nV.getData("action").toString().equalsIgnoreCase("chat")) {
                
            }else if (nV.getData("action").toString().equalsIgnoreCase("push")) {  
                
            }else{
                
            }
        }else{
            
        }
	}
	@Override
	public void onMessage(byte[] data) {
		Log.i("Nikita NR:+", "X");		
	}
	@Override
	public void onDisconnect(int code, String reason) {
		Log.i("Nikita NR onDisconnect", Thread.currentThread().getName());	
	}
	@Override
	public void onError(Exception error) {
		Log.i("Nikita NR onError:", error.getMessage());		
	}
	
	 
}
