package com.nikita.mobile.finalphase.service;

 
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.generator.action.MobileAction;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;




public class LocationService extends IntentService {
	private static double latitude;
	private static double longitude;

	public LocationService() {
		super("Fused Location");
	}	
	public LocationService(String name) {
		super("Fused Location");		 
	}	
	protected void onHandleIntent(Intent intent) {
	    String action = intent.getAction()!=null?intent.getAction():"";
	    if (action.equals("gpssave")) {
	    	NikitaFuseLocation.savelocationtofile(intent.getStringExtra("locationdata"));
	    }else if (action.equals("gpssend")) {	
	    	NikitaFuseLocation.sendlocationfile();
		}else if (action.equals("gps")) {
			NikitaFuseLocation	nikitaFuseLocation = new NikitaFuseLocation(getApplicationContext(), new NikitaFuseLocation.OnLocationListener() {
				public void onLocationChanged(Location location) {

				}
				public void onLocationDone(Location location, int status) {
					if (location!=null && location.getAccuracy() < 500.0) {	
						Intent intent = new Intent(getApplicationContext(), LocationService.class); 
				        intent.putExtra("locationdata", NikitaFuseLocation.locationToNset(location, status).toJSON());
				        intent.putExtra("locationstatus", status);
				        intent.setAction("gpssave");
						startService(intent);
					}
				}
			});	   
		}else{
			//nothing
		}
	}
	
	public static double getLatitude() {
		return latitude;
	}

	public static double getLongitude() {
		return longitude;
	}

}
