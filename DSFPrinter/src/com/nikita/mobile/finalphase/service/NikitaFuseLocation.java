package com.nikita.mobile.finalphase.service;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.utility.NetworkUtil;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

public class NikitaFuseLocation implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	private static final long INTERVAL = 1000 * 30;
	private static final long FASTEST_INTERVAL = 1000 * 5;
	private static final long ONE_MIN = 1000 * 60;
	private static final float MINIMUM_ACCURACY = 50.0f;
	//Edit Kevin
	//float MAXIMUM_ACCURACY;
 
	private LocationRequest locationRequest;
	private GoogleApiClient googleApiClient;
	private Location location;
	private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

	 
	public static void requestLocationOnce(Context context, OnLocationListener locationListener){
		new NikitaFuseLocation(context, locationListener);			
	}
	
	public interface OnLocationListener{
		public void onLocationChanged(Location location) ;
		public void onLocationDone(Location location, int status) ;
	}
	private OnLocationListener locationListener;
	private boolean unlimited = true;
	private long timeout = ONE_MIN;
	public NikitaFuseLocation(Context context, OnLocationListener locationListener) {
		this(context, locationListener, ONE_MIN, false);
	}
	public NikitaFuseLocation(Context context, OnLocationListener locationListener, long oneMin, boolean unlimited) {
		//Edit Kevin
		//MAXIMUM_ACCURACY = Float.parseFloat(Generator.getNikitaParameters().getData("INIT-MAX-ACCURACY").toString());
		timeout = oneMin;
		this.locationListener=locationListener;
		if (isGooglePlayServicesAvailable(context)) {
			locationRequest = LocationRequest.create();
			locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			locationRequest.setInterval(INTERVAL);
			locationRequest.setFastestInterval(FASTEST_INTERVAL);
			
			googleApiClient = new GoogleApiClient.Builder(context)
			        .addApi(LocationServices.API)
			        .addConnectionCallbacks(this)
			        .addOnConnectionFailedListener(this)
			        .build();
			
			if (googleApiClient != null) {
				 googleApiClient.connect();					  
			}
			
			AppNikita.getInstance().getMainHandler().postDelayed(new Runnable() {
				private NikitaFuseLocation nikitaFuseLocation;
				public void run() {
					nikitaFuseLocation.callBackListener(nikitaFuseLocation.getLocation(), 1);					
				}
				public Runnable get(NikitaFuseLocation nikitaFuseLocation){
					this.nikitaFuseLocation=nikitaFuseLocation;
					return this;
				}
			}.get(this), oneMin);
        }else{
        	if (this.locationListener!=null) {
              	 this.locationListener.onLocationDone(null, -1);
       		}
        }	
	}
	private long startdate = 0;
	public void onConnected(Bundle connectionHint) {	
		location = fusedLocationProviderApi.getLastLocation(googleApiClient);	
	   
		Intent filter= new Intent();
	    filter.setAction("com.nikita.generator");
	    filter.putExtra("uuid", "lastlocation");
	    filter.putExtra("provider", "fused");
	    filter.putExtra("data", NikitaFuseLocation.locationToNset(location).toJSON());
	    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
	   
	    fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest,  NikitaFuseLocation.this  );
	    startdate = System.currentTimeMillis();
	}
	protected void callBackListener(Location location, int status){
		    	
    	if (googleApiClient!=null && googleApiClient.isConnected()) {
    		if (fusedLocationProviderApi!=null) {
        		fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
    		}	
    		googleApiClient.disconnect();
		}			
		if (locationListener!=null) {
         	locationListener.onLocationDone(location, status);
         	locationListener=null;
 		}   
	}
	public void onLocationChanged(Location location) {
		this.location = location;//mainthread
		
		Intent filter= new Intent();
	    filter.setAction("com.nikita.generator");
	    filter.putExtra("uuid", "locationchanged");
	    filter.putExtra("provider", "fused");
	    filter.putExtra("data", NikitaFuseLocation.locationToNset(location).toJSON());
	    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
	    
	    //add13k track
	    if (locationListener!=null) {
         	locationListener.onLocationChanged(location);
	    }
	    //Edit Kevin
	    //accuracy >  INIT-MAX-ACCURACY
	    //if (this.location.getAccuracy() > MAXIMUM_ACCURACY) {
		//	 callBackListener(location, 2);
	     //} 
	   	    
	    if (!unlimited) {
	    	if (Math.abs(System.currentTimeMillis()-startdate)>=timeout) {
		    	 callBackListener(location, 1);
			}
//	    	else{
//				 if (this.location.getAccuracy() < MINIMUM_ACCURACY) {
//					 callBackListener(location, 2);
//			     } 
//			}
		}
	}
	
	public Location getLocation() {
		return this.location;
	}
	
	public void onConnectionSuspended(int i) {	 }	
	
	public void onConnectionFailed(ConnectionResult connectionResult) {	 }
	public static Nset locationToNset(Location  location) {
		return locationToNset(location, 0);
	}
	public static Nset locationToNset(Location  location, int status) {
		return locationToNset(location, status, "");
	}
	
	//20-07-2016, parse latitude longitude to address
	public static String getAddress(Location location){
		
		 Geocoder geocoder = new Geocoder(Utility.getAppContext(), Locale.getDefault());
		 String result = null;
		 
		 try {
			List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			if (list != null && list.size() > 0) {
				Address address = list.get(0);
				// sending back first address line and locality
				result = address.getAddressLine(0) + ", " + address.getAddressLine(1)+ ", " + address.getAddressLine(2)+ ", " + address.getAddressLine(3);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} 
		
		return result;
	}
	
	public static Nset locationToNset(Location  location, int status, String info) {
 		 Nset n = Nset.newObject();
 		 String address = "";
		 if (null != location) {
			 
			 /*if(NetworkUtil.isNetworkConnected(AppNikita.getInstance().getApplicationContext()) == true){
//				 address = getAddress(location);
				 address = "";
			 }else{
				 address = "";
			 }*/
			 
			 n.setData( "latitude", location.getLatitude());
			 n.setData( "longitude", location.getLongitude());
//			 n.setData( "address", address);
			 n.setData( "provider", location.getProvider());
			 n.setData( "accuracy", location.getAccuracy());
			 n.setData( "time",  location.getTime());
			 n.setData( "altitude", location.getAltitude());
			 n.setData( "speed", location.getSpeed());
			 n.setData( "date",  Utility.formatDate(location.getTime(), "yyyy-MM-dd HH:mm:ss"));
			 n.setData( "status", status);
			 n.setData( "info", info);
			 
		 }
		 
		 Nset v = Nset.newArray();
		 if (null != location) {
			 /*if(NetworkUtil.isNetworkConnected(AppNikita.getInstance().getApplicationContext()) == true){
//				 address = getAddress(location);
				 address = "";
			 }else{
				 address = "";
			 }*/
			 
			 v.addData( location.getLatitude());
			 v.addData( location.getLongitude());
//			 v.addData( address );
			 v.addData( location.getProvider());
			 v.addData( location.getAccuracy()); //in meters
			 v.addData( location.getTime());
			 v.addData( location.getAltitude());
			 v.addData( location.getSpeed());
			 v.addData( Utility.formatDate(location.getTime(), "yyyy-MM-dd HH:mm:ss"));
			 v.addData( status );
			 v.addData( info );
			 
		 }
		 return n;
	}
	
 
	public static void savelocationtofile( String append ) {	
		//fusedthread
		try {
			File file = new File(Utility.getDefaultPath("gps.txt"));
			 
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
            //if file >64KB, trancate
    		if (file.length()>65798) {
				//truncate
    			/*
    			String line;int count = 0;
    			FileWriter fileWritter = new FileWriter(new File(Utility.getDefaultPath("gps.tmp")));
    			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    			
    			FileReader reader = new FileReader(file);
    			BufferedReader br = new BufferedReader(reader);
    			while ((line = br.readLine()) != null) {
    				bufferWritter.write(line);
    				bufferWritter.newLine();
    		    }
    			bufferWritter.flush();
    			bufferWritter.close();
    			fileWritter.close();
    			
    			count++;
    			
    			br.close();
    			reader.close();
    			*/
			}
    		
    		//true = append file
    		FileWriter fileWritter = new FileWriter(file, true);
	        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
	        bufferWritter.write(append);
	        bufferWritter.newLine();
	        bufferWritter.flush();
	        bufferWritter.close();
	        
	        fileWritter.close();
		} catch (Exception e) { }
	}
	
	public static void sendlocationfile() {
		//fusedthread
		Hashtable<String, String> hs = AppNikita.getInstance().getArgsData();
		try {
			hs.put("gpsfile", Utility.readInputStreamAsString(new FileInputStream(Utility.getDefaultPath("gps.txt"))));
		} catch (Exception e) { }
		Nset n = NikitaInternet.getNset( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mgpstrack?" , hs) );
		if (n.getData("status").toString().equalsIgnoreCase("OK")||(n.getData("error").toString().equalsIgnoreCase("") && n.containsKey("error")) ) {
			new File(Utility.getDefaultPath("gps.txt")).delete();
		}				
	}

    private static boolean isGooglePlayServicesAvailable(Context context ) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            //GooglePlayServicesUtil.getErrorDialog(status, context, 0).show();
            return false;
        }
    }
}
