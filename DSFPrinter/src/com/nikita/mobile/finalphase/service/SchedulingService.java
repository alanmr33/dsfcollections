package com.nikita.mobile.finalphase.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.activity.ChatActivity;
import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.activity.OrderCurrentActivity;
import com.nikita.mobile.finalphase.activity.SettingActivity;
import com.nikita.mobile.finalphase.activity.StarterActivity;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.action.MobileAction;
import com.nikita.mobile.finalphase.service.helper.WebSocketClient;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

/**
 * This {@code IntentService} does the app's actual work.
 * {@code SampleAlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class SchedulingService extends IntentService {
    public SchedulingService() {
        super("SchedulingService");
    }
    
    public static final String TAG = "Nikita Scheduling";
    // An ID used to post the notification.
    public static final int NOTIFICATION_ID = 1;
    // The string the app searches for in the Google home page content. If the app finds 
    // the string, it indicates the presence of a doodle.  
    public static final String SEARCH_STRING = "nikita";
    // The Google home page URL from which the app fetches content.
    // You can find a list of other Google domains with possible doodles here:
    // http://en.wikipedia.org/wiki/List_of_Google_domains
    public static final String URL = "http://www.google.com";
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder builder;

    public static  WebSocketClient webSocketClient;
     
    
    protected void onHandleIntent(Intent intent) { 
    	Log.i("AlarmReceiver-onHandleIntent", Thread.currentThread().getName());
    	Log.i("AlarmReceiver-onHandleIntent-getMainLooper", Looper.getMainLooper().getThread().getName());
    	Log.i("AlarmReceiver-onHandleIntent-myLooper", Looper.myLooper().getThread().getName());
    	
    	Intent  filter = new Intent();
	     filter.setAction("com.nikita.activity.menuactivity");

    	
    	sendBroadcast(filter);
    	 
    	
    	
    	 Log.i("AlarmReceiver", "onHandleIntenta:"+intent.getStringExtra("threadname")+" : "+ Utility.Now() );
    	/*
    	 try {
    		//Thread.sleep(200);
    		
    		Handler a= new Handler(Looper.getMainLooper());
    		a.post(new Runnable() {
				
				@Override
				public void run() {
					//Log.i("UI------", Thread.currentThread().getName());
					
					//Intent intent = new Intent(AppNikita.getInstance().getApplicationContext(),  SettingActivity.class );	
					//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					//AppNikita.getInstance().getApplicationContext().startActivity(intent);
					try {
						//Thread.sleep(20000);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
			});
    		
		} catch (Exception e) { }
    	 */
    	 //Log.i("AlarmReceiver", "onHandleIntentb:"+intent.getStringExtra("threadname")+" : "+ Utility.Now() );
    	 
    	 AlarmReceiver.completeWakefulIntent(intent);
    }
    
    private void takegps(){
    	 NikitaFuseLocation.requestLocationOnce(this, new NikitaFuseLocation.OnLocationListener() {
				public void onLocationChanged(Location location) {
					
				}

				public void onLocationDone(Location location, int status) {
					// TODO Auto-generated method stub
					Generator.out("GPS"+status, NikitaFuseLocation.locationToNset(location).toJSON());
					savegps(NikitaFuseLocation.locationToNset(location).toJSON(), false);
				}
		});   
    }
    private final String gpsfname = "n.gps";
    private void savegps(String append, boolean send){    	
    	
    	if (append.length()>=3) {
			try {
				File file = new File(Utility.getDefaultPath(gpsfname));
	    		if(!file.exists()){
	    			file.createNewFile();
	    		}
	    		FileWriter fileWritter = new FileWriter(file, true);
		        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		        bufferWritter.write(append);
		        bufferWritter.newLine();
		        bufferWritter.flush();
		        bufferWritter.close();
		        
		        fileWritter.close();
			} catch (Exception e) { }
		}
    	
    	if (send) {
    		Vector<String> vector = new Vector<String>();
    		try {
    			File file = new File(Utility.getDefaultPath(gpsfname));
        		FileReader fileReader = new FileReader(file);
        		BufferedReader bufferedReader = new BufferedReader(fileReader);
        		String read ;        		
        		while ((read=bufferedReader.readLine())!=null) {
        			vector.addElement(read);
        			if (vector.size()>=1000) {
        				vector.remove(0);
					}
				}        		
        		bufferedReader.close();
        		fileReader.close();
			} catch (Exception e) { }   
    		if (vector.size()>=1) {
    			String version = "";
    			try {
    				PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
    				version = pInfo.versionName  ;
    			} catch (NameNotFoundException e) {  }
    			
    			Hashtable<String, String> hashtable = new Hashtable<String, String>();
        		
    			hashtable.put("nauth",  Utility.getSetting(getApplicationContext(), "N-AUTH", "") );
    			hashtable.put("userid",  Utility.getSetting(getApplicationContext(), "N-USER", "") );
    			hashtable.put("imei",    ( (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase() );
    			hashtable.put("version", version );
        		hashtable.put("gps",     new Nset(vector).toJSON() );
        		
    			
        		String BASE=Utility.getSetting(SchedulingService.this.getApplicationContext(), "BASE-URL", "");		
        		Nset nlog = Nset.readJSON(  NikitaInternet.getString( NikitaInternet.postHttp( BASE+"mobile.mgpstrack", hashtable ))  );					
        		if (nlog.getData("status").toString().equals("OK")) {
					try {
						 new File(Utility.getDefaultPath(gpsfname)).delete();
					} catch (Exception e) { }
				}
			}    		
		}
    }    
    
    private void runscheduler(){
    	 
    	
    }
    
    private int getTimeNumber(String time, String timenow){
    	String h = "0";
    	if (time.startsWith(":") && timenow.contains(":")) {
			h=timenow.substring(0,timenow.indexOf(":"));
			time = time.substring(time.indexOf(":")+1);
		}else if (time.contains(":") ) {
			h=time.substring(0,time.indexOf(":"));
			time = time.substring(time.indexOf(":")+1);
		}
    	String m = "0";
    	if (time.contains(":")) {
    		m=time.substring(0,time.indexOf(":"));
			time = time.substring(time.indexOf(":")+1);
		}else{
			m=time;
			time="0";
		}
    	String d = time;
    	if (m.equals("")) {
    		m=timenow.substring(3,5);//01:34:6
    	}
    	
    	return Utility.getInt(h)*3600+Utility.getInt(m)*60+Utility.getInt(d);
    }
    private void ontriger(String param, Runnable callback){
    	Log.i("ALARM", "ontriger:"+param);
    	if (param.equals("")) {
		}else if (param.equals("[]")) {
    		callback.run();
		}else{
			Nset n = Nset.readJSON(param);
			for (int i = 0; i < n.getArraySize(); i++) {
				String now = Utility.Now().substring(11, 19);//YYYY-MM-DD HH:NN:SS
				
				if (n.getData(i).toString().toUpperCase().equals("WORK")) {
					boolean found = false;
					String get = "";//2011
					if ("07|08|09|10|11|12|13|14|15|16|17|18|19".contains(now.substring(0,2))) {
						 get = now.substring(0,6)+"00";
						 int nn =  getTimeNumber(now, now);
						 int gg =  getTimeNumber(get, now);
						 if ((nn+10)>=gg && (nn-10)<=gg) {
							 callback.run();;break;
						}
					}else{
						 get = now.substring(0,0)+":00:00";
						 int nn =  getTimeNumber(now, now);
						 int gg =  getTimeNumber(get, now);
						 if ((nn+10)>=gg && (nn-10)<=gg) {
							 callback.run();;break;
						}
					}
				}else{		
					String get = n.getData(i).toString();
					if (get.contains(":")) {
						 int nn =  getTimeNumber(now, now);
						 int gg =  getTimeNumber(get, now);
						 if ((nn+10)>=gg && (nn-10)<=gg) {
							 callback.run();;break;
						}
					}					
				}				
			}
		}     
    }
    protected void onHandleIntenta(Intent intent) { 
    	//Generator.initParam = Nset.readJSON(Utility.getSetting(SchedulingService.this, "INIT", ""));
    	/*
        //gps track
        ontriger(Generator.initParam.getData("INIT-GPS-TRACK").toString(), new Runnable() {
			public void run() {
				takegps();
			}
		});
        
        //gps send
        ontriger(Generator.initParam.getData("INIT-GPS-SEND").toString(), new Runnable() {
			public void run() {
				savegps("", true);;
			}
		}); 

        //donload order
        ontriger(Generator.initParam.getData("INIT-SYNC-ORDER").toString(), new Runnable() {
			public void run() {
				LoginActivity.downloadorder(SchedulingService.this, true);
			}
		}); 

        //donload order
        ontriger(Generator.initParam.getData("INIT-SYNC-ACTIVITY").toString(), new Runnable() {
			public void run() {
				Log.i("ALARM", "ACTIVITY");
				MobileAction.sendMobileActivityData(SchedulingService.this);
			}
		}); 
        
        //scheduler
        ontriger(Generator.initParam.getData("INIT-SCHEDULER").toString(), new Runnable() {
			public void run() {
				runscheduler();
			}
		}); 
        
        if (Generator.initParam.containsKey("INIT-SCHEDULER-1")) {
			
		}
        
       */
         
        
        // Release the wake lock provided by the BroadcastReceiver.
        AlarmReceiver.completeWakefulIntent(intent);
        // END_INCLUDE(service_onhandle)
    }
    private void nikitadevA(Intent intent) { 
    	 WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
 		if (wifi!=null && wifi.isWifiEnabled() ) {
 			    
 		        if (webSocketClient!=null) {
 		       	    if (webSocketClient.isClosed()) {
 		       		  webSocketClient.connect();
 		       		  DevActivity.setupNikita(SchedulingService.this, "...");
 				    }
 				}else {
 					try {
 						webSocketClient = new WebSocketClient(new URI("ws://app.indocyber.co.id/examples/websocket/chat"),  new WebSocketClient.Listener() {
 							public void onMessage(byte[] data) {
 								Generator.out("WebSocketClient", "onMessagebyte");						
 							}
 							public void onMessage(String message) {
 								Generator.out("WebSocketClient", "onMessage:"+message);
 								
 								if (message.startsWith("*") ) { 
 									
 								}else if ( message.contains(":")) { 
 									
 								 message=message.substring(message.indexOf(":")+1).trim();
 		
 								 	TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
 									String imei=tm.getDeviceId().toUpperCase();	
 									
 									if (message.startsWith("*MOBILE-PLAY-"+imei)) {
 										Generator.out("WebSocketClient", "MOBILE-PLAY");
 										
 										PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
 										WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,  "MOBILE-PLAY");
 										wakeLock.acquire();
 										
 										String BASE_URL=Utility.getSetting(getApplicationContext(), "BASE-URL", "");
 										String s =Utility.getHttpConnection(BASE_URL+"res/mobiledebug/?code=MOBILE-PLAY-"+imei);
 										
 										Nikitaset ns = new Nikitaset(Nset.readJSON(s));
 										if (ns.getRows()>=1) {
 											Generator.out("DEBUG", "RUN");
 											
 											//DevActivity.syncInit(getApplicationContext());
 											Generator.closeAllConnection();				
 											 
 											Intent intent2 = new Intent(getApplicationContext(), StarterActivity.class);
 											intent2.putExtra("nikitaformname", Nset.readJSON(ns.getText(0, 2)).getData("fid").toString());
 											intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK );
 											//startActivity(intent2);
 										    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT );
 										    
 										    try {
 										    	pendingIntent.send();
 											} catch (Exception e) { e.printStackTrace(); }
 		
 										    
 										}
 										
 										wakeLock.release();
 									}
 									
 								}
 							}
 							public void onError(Exception error) {
 								Generator.out("WebSocketClient", "onError");
 								DevActivity.setupNikita(SchedulingService.this, "-");
 								
 							}
 							public void onDisconnect(int code, String reason) {
 								Generator.out("WebSocketClient", "onDisconnect");
 								DevActivity.setupNikita(SchedulingService.this, "");
 							}
 							
 							public void onConnect() {
 								Generator.out("WebSocketClient", "onConnect");
 								DevActivity.setupNikita(SchedulingService.this, ".");
 							}
 						} , null );			
 						
 						
 					} catch (URISyntaxException e) { }
 					 webSocketClient.connect();
 					 DevActivity.setupNikita(SchedulingService.this, "...");
 				}
 		
 		}
 		
 		  /*
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
		if (wifi!=null && wifi.isWifiEnabled() && DevActivity.pause==false) {
			
			
			
			
			
			
			
			
			
			
			TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
			String imei=tm.getDeviceId().toUpperCase();
			String BASE_URL=Utility.getSetting(getApplicationContext(), "BASE-URL", "");
			String s =Utility.getHttpConnection(BASE_URL+"res/mobiledebug/?code=MOBILE-PLAY-"+imei);
			
			final Nikitaset ns = new Nikitaset(Nset.readJSON(s));
			if (ns.getRows()>=1) {
				Generator.out("DEBUG", "RUN");
				
				DevActivity.syncInit(getApplicationContext());
				Generator.closeAllConnection();				
				 
				Intent intent2 = new Intent(this, StarterActivity.class);
				intent2.putExtra("nikitaformname", Nset.readJSON(ns.getText(0, 2)).getData("fid").toString());
				//intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				//startActivity(intent2);
			    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT );
			    
			    try {
			    	pendingIntent.send();
				} catch (Exception e) { }

			    
			}
		}	        
        */
	}
    // Post a notification indicating whether a doodle was found.
    private void sendNotification(String msg) {      
    }
    
    public static void  setupNikita(Context context, String message){
	 	TelephonyManager tm = (TelephonyManager)context. getSystemService(android.content.Context.TELEPHONY_SERVICE);
		String imei=tm.getDeviceId().toUpperCase();			 
		
		 
		
		NotificationManager  notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE); 
		NotificationCompat.Builder noti = new NotificationCompat.Builder(context);
		noti.setContentTitle("Mobile Chat");
		noti.setContentText(message);
		noti.setSmallIcon(R.drawable.generator);
		noti.setAutoCancel(false);//cant cancle
		
		
		Intent intent = new Intent(context, DevActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		noti.setContentIntent(pIntent);
		
		Notification nc = noti.getNotification();
		 
		notificationManager.notify(1, nc);
}

//
// The methods below this line fetch content from the specified URL and return the
// content as a string.
//
    /** Given a URL string, initiate a fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";
      
        try {
            stream = downloadUrl(urlString);
            str = readIt(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }      
        }
        return str;
    }

    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {
    
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
    }

    /** 
     * Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from www.google.com.
     * @return String version of InputStream.
     * @throws IOException
     */
    private String readIt(InputStream stream) throws IOException {
      
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        for(String line = reader.readLine(); line != null; line = reader.readLine()) 
            builder.append(line);
        reader.close();
        return builder.toString();
    }
}
