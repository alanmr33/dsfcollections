package com.nikita.mobile.finalphase.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.Calendar;

import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class AlarmReceiver extends WakefulBroadcastReceiver {
    public void onReceive(Context context, Intent intent) {     
        //uithread
    	//Log.i("AlarmReceiver", Thread.currentThread().getName()+" : "+Looper.myLooper().getThread().getName());
	    Log.i("AlarmReceiver", "action:"+intent.getAction()+" : "+ Utility.Now() );
	    
	   
        String action = intent.getAction()!=null?intent.getAction():"";
        if (action.equals("gps")||action.equals("gpssave")||action.equals("gpssend")) {
        	if (isTrue(intent)) { 
        		Log.i("AlarmReceiver", "startWakefulService" );
        	Intent service = new Intent(context, LocationService.class);
        	service.setAction(action);
        	startWakefulService(context, service); 
			}        	
		}else{
			if (isTrue(intent)) {  
				Log.i("AlarmReceiver", "startWakefulService" );
			Intent service = new Intent(context, NikitaService.class);
			service.setAction(action);
        	startWakefulService(context, service); 
			}
		}
    }
    private boolean isTrue( Intent intent){
    	if (intent.getStringExtra("NIKITAEXTRA")!=null) {    		
    		Nset n = Nset.readJSON(intent.getStringExtra("NIKITAEXTRA"));
    		if (n.containsKey("REGEX")) {
    			if (Utility.Now().matches(n.getData("REGEX").toString())) {
    				return true; 
    			} 
    			return false;
    		}else if (n.containsKey("NEX")) {
    		} 
    		return true;
		}else{
			return true; 
		}     	
    }
    public boolean isOnline(Context context)  {
	     ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	     NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	     return (networkInfo != null && networkInfo.isConnected());
    }  

    private static PendingIntent getPendingIntentGPSsend(Context context, Nset extra){
    	 Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
         intent.setAction("gpssend");
         putExtra(intent, extra);
         return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
    }
    
    private static PendingIntent getPendingIntentGPS(Context context, Nset extra){
    	 Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
         intent.setAction("gps");
         putExtra(intent, extra);
         return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
    }
    private static PendingIntent getPendingIntentOrder(Context context, Nset extra){
   	 Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.setAction("order");
        putExtra(intent, extra);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
    }
    private static PendingIntent getPendingIntentPending(Context context, Nset extra){
   	   Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.setAction("pending");
        putExtra(intent, extra);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
   }
    
   private static PendingIntent getPendingIntentBackground(Context context, Nset extra){
   	    Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.setAction("backgorund");
        putExtra(intent, extra);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
   }
   private static PendingIntent getPendingIntentNikitaNR(Context context, Nset extra){
  	   Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
       intent.setAction("nikitanr");
       putExtra(intent, extra);
       return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
  }
   private static PendingIntent getPendingIntentNikitaRz(Context context, Nset extra){
  	   Intent  intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
       intent.setAction("nikitarz");
       putExtra(intent, extra);
       return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);   
  }
   private static void putExtra(Intent intent, Nset extra){
	   /* String[] keys = extra.getObjectKeys();
	   for (int i = 0; i < keys.length; i++) {
		   intent.putExtra(keys[i], extra.getData().toString());		    
	   }*/
	   if (intent!=null && extra!=null) {
		  intent.putExtra("NIKITAEXTRA", extra.toJSON());
	   }
   
   }
    public static void setAlarm(Context context) {
    	AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
 
        /*
        Intent 
        intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.putExtra("threadname", "a");
        
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);    
        intent.addCategory("a");
        intent.setAction("backgorund");
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), 1500000, alarmIntent);
        
        
        intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.putExtra("threadname", "gps");
        intent.setAction("gps");
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);         
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), 6000000, alarmIntent);
       
        
        intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.putExtra("threadname", "gpssend");
        intent.setAction("gpssend");
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);         
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), 7000000, alarmIntent);
     
        
        intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.putExtra("threadname", "order");
        intent.setAction("order");
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);         
        //alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), 60000, alarmIntent);

        intent = new Intent(context, AlarmReceiver.class);//call AlarmReceiver.onReceive
        intent.putExtra("threadname", "pending");
        intent.setAction("pending");
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);         
        //alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), 60000, alarmIntent);
        
        */

        Nset gpsAlrm = Nset.readJSON(Generator.getNikitaParameterValue("INIT-GPS-TRACK", "{}"));
        if (gpsAlrm.getData("TIMER").toInteger()>=10) {
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), gpsAlrm.getData("TIMER").toInteger()*1000, getPendingIntentGPS(context, gpsAlrm));
		}else{
			cancelAlarmBroadcast( context, getPendingIntentGPS(context, gpsAlrm));
		}
        
        Nset gpsSend = Nset.readJSON(Generator.getNikitaParameterValue("INIT-GPS-SEND", "{}"));
        if (gpsSend.getData("TIMER").toInteger()>=10) {
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), gpsSend.getData("TIMER").toInteger()*1000, getPendingIntentGPSsend(context, gpsSend));
		}else{
			cancelAlarmBroadcast( context, getPendingIntentGPSsend(context, gpsSend));
		}
    	        
        Nset ordeDwn = Nset.readJSON(Generator.getNikitaParameterValue("INIT-ORDER-DOWNLOAD", "{}"));
        if (ordeDwn.getData("TIMER").toInteger()>=10) {
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), ordeDwn.getData("TIMER").toInteger()*1000, getPendingIntentOrder(context, ordeDwn));
		}else{
			cancelAlarmBroadcast( context, getPendingIntentOrder(context, ordeDwn));
		}
        
        Nset synActy = Nset.readJSON(Generator.getNikitaParameterValue("INIT-SYNC-ACTIVITY", "{}"));
        if (synActy.getData("TIMER").toInteger()>=10) {
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), synActy.getData("TIMER").toInteger()*1000, getPendingIntentPending(context, synActy));
		}else{
			cancelAlarmBroadcast( context, getPendingIntentPending(context, synActy));
		}
        
        Nset synBckgrd = Nset.readJSON(Generator.getNikitaParameterValue("INIT-SYNC-BACKGROUND", "{}"));
        if (synBckgrd.getData("TIMER").toInteger()>=10) {
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), synBckgrd.getData("TIMER").toInteger()*1000, getPendingIntentBackground(context, synBckgrd));
		}else{
			cancelAlarmBroadcast( context, getPendingIntentBackground(context, synBckgrd));
		}
        
        if (Generator.getNikitaParameters().containsKey("INIT-NIKITARZ")) {
        	int interval = 60*1000;
        	Nset n = Nset.readJSON(Generator.getNikitaParameters().getData("INIT-NIKITARZ-DATA").toString());
        	if (Generator.getNikitaParameters().containsKey("INIT-NIKITARZ-DATA")) {        		 
        		 if (n.containsKey("interval")) {
        			 interval = n.getData("interval").toInteger();
        		 }
			}
        	alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(), interval, getPendingIntentNikitaRz(context, n));
        }else{
        	cancelAlarmBroadcast( context, getPendingIntentNikitaRz(context, Nset.newObject()));
        }
        
        cancelAlarmBroadcast( context, getPendingIntentNikitaNR(context, Nset.newObject()));
        
        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,   PackageManager.COMPONENT_ENABLED_STATE_ENABLED,   PackageManager.DONT_KILL_APP);    
    }
    
    public static void restartAlarm(Context context) {
    	cancelAlarmBroadcast(context);
    	setAlarm(context);
    }
    
    public static void cancelAlarmBroadcast(Context context){
    	cancelAlarmBroadcast(context, getPendingIntentBackground(context, Nset.newObject()));
    	cancelAlarmBroadcast(context, getPendingIntentGPS(context, Nset.newObject()));
    	cancelAlarmBroadcast(context, getPendingIntentGPSsend(context, Nset.newObject()));
    	cancelAlarmBroadcast(context, getPendingIntentOrder(context, Nset.newObject()));
    	//cancelAlarmBroadcast(context, getPendingIntentPending(context, Nset.newObject()));   	
    	cancelAlarmBroadcast(context, getPendingIntentNikitaRz(context, Nset.newObject()));
    	
    	cancelAlarmBroadcast(context, getPendingIntentNikitaNR(context, Nset.newObject())); 
    }
    
    public static void cancelAlarmBroadcast(Context context, PendingIntent pIntent){
		 AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		 alarmMgr.cancel(pIntent);		 
         
    }
    
    private void androidComponentEnabledSetting(Context context, boolean enable){
    	ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,  (enable ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED :PackageManager.COMPONENT_ENABLED_STATE_DISABLED), PackageManager.DONT_KILL_APP);
    }
 
}
