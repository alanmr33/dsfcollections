package com.nikita.mobile.finalphase.connection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.mime.Header;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.ssl.EasySSLSocketFactory;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.HashtableMulti;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.util.Log;

public class NikitaInternet {
	private static void setHeader(HttpRequestBase requestBase){
		requestBase.addHeader("Nikita", "mobile");
		requestBase.addHeader("User-Agent", "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0; Nikita V3) Gecko/41.0 Firefox/41.0");
		requestBase.addHeader("Accept", "text/plain, */*; q=0.01");
		requestBase.addHeader("Accept-Language", "en-US,en;q=0.5");
	}
	public static Nikitaset getNikitaset(HttpResponse httpResponse){		 
		return new Nikitaset(getNset(httpResponse));
	}	
	public static Nset getNset(HttpResponse httpResponse){		 
		return Nset.readJSON(getString(httpResponse));
	}
	public static String getString(HttpResponse httpResponse){
		try {
			return getString(httpResponse.getEntity().getContent());
		}  catch (Exception e) { }
		return getTimeoutMessage().toJSON();
	}
	public static Nset getTimeoutMessage(){
		return Nset.readJSON("{'error':'Connection timeout', 'nfid':'NikitaInternet'}", true);
	}
	public static String getString(InputStream inputStream){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();	
	    try {
	    	int length = 0;byte[] buffer = new byte[1024];
			while ((length = inputStream.read(buffer)) != -1) {
			    baos.write(buffer, 0, length);
			}
		} catch (IOException e) { }		 
		  
	   Generator.out("DEBUG-HTTPr:",  new String(baos.toByteArray()));
		return new String(baos.toByteArray());
	}
	
	
	public static HttpResponse getHttp(String url, String...paramvalue)  {
		Generator.out("DEBUG-HTTPg", url);
		try{
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https",getSslSocketFactory(), 443));
			
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);	
	
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			DefaultHttpClient httpClient = new DefaultHttpClient(cm, params);
	
			if (paramvalue!=null) {
				StringBuffer stringBuffer = new StringBuffer();
				for (int i = 0; i < paramvalue.length; i++) {
					if (paramvalue[i].contains("=")) {
						int split = paramvalue[i].indexOf("=");String sdata = Utility.urlEncode(paramvalue[i].substring(split+1));
						stringBuffer.append(paramvalue[i].substring(0, split)).append("=").append(sdata).append("&");
					}				
				}		
				url =  url+(url.contains("?")?"&":"?")+stringBuffer.toString(); 
			}			 
			Log.i("=GET=", url);
		    HttpGet httpget = new HttpGet(url);
		    setHeader(httpget);
		    httpget.setParams(params);
		    HttpResponse response;
		    response = httpClient.execute(httpget);
			Log.i("=GET=", "RESPONSE : "+response.getStatusLine().getStatusCode());
		    
		    return response;
	    
		}catch (Exception e) {
	    	return null;
		}	 
	} 
	public static HttpResponse postHttp(String url, String...paramvalue)  {
		Generator.out("DEBUG-HTTPp", url);
		
		if (paramvalue!=null) {
			Hashtable<String, String> arg = new Hashtable<String, String>();
			for (int i = 0; i < paramvalue.length; i++) {
				if (paramvalue[i].contains("=")) {
					int split = paramvalue[i].indexOf("=");
					arg.put(paramvalue[i].substring(0, split), paramvalue[i].substring(split+1));
				}				
			}
			return postHttp(url, arg);
		}
		return postHttp(url);
	}
	public static HttpResponse postHttp(String url, Hashtable<String, String> arg) {
		return postHttp(url, null,  arg);
	}
	public static HttpResponse postHttp(String url, Hashtable<String, String> header, Hashtable<String, String> arg) {
		try {
			 
			HttpClient client = new DefaultHttpClient();
			
			if (url.startsWith("http://")||url.startsWith("https://")) {
			}else{
				String s =  AppNikita.getInstance().getBaseUrl();
				if (s.endsWith("/")) {
					url=s+url;
				}else{
					url=s+"/"+url;
				}
			}
			
			Generator.out("DEBUG-HTTPp", url);
			//13kcrazy
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			client = new DefaultHttpClient(cm, params);
			//13kcrazy
			
			HttpPost post = new HttpPost(url);	
			 
			Nset nheader = new Nset(header) ;
			String[] keyheader =  nheader.getObjectKeys();			
			for (int i = 0; i < keyheader.length; i++) {
				post.addHeader(keyheader[i], nheader.getData(keyheader[i]).toString() );
			}	
			setHeader(post);
			
			Nset nset = new Nset(arg) ;
			String[] keys =  nset.getObjectKeys();
			
			ArrayList<NameValuePair> postparam = new ArrayList<NameValuePair>();
			Log.i("=POST=", url);
			for (int i = 0; i < keys.length; i++) {
				postparam.add(new BasicNameValuePair(keys[i], nset.getData(keys[i]).toString() ));
				Log.i("=POST=", keys[i]+" ==> "+ nset.getData(keys[i]).toString());
			}	
			
			post.setEntity(new UrlEncodedFormEntity(postparam) );  
			HttpResponse response = client.execute(post);
			Log.i("=POST=", "RESPONSE : "+response.getStatusLine().getStatusCode());
		 
			return response;
			    
		}catch (Exception e) {
	    	return null;
		}	
	}
	public static HttpResponse getHttp(String url, Hashtable<String, String> arg) {
        StringBuilder sb = new StringBuilder(url.contains("?")?"":"?");         
        Nset nset = new Nset(arg) ;
        String[] keys =  nset.getObjectKeys();
        for (int i = 0; i < keys.length; i++) {
            sb.append("&");
            sb.append(keys[i]);
            sb.append("=");
            sb.append(URLEncoder.encode( nset.getData(keys[i]).toString() )  );
        }
        return getHttp(url+sb.toString(), new String[0]);
	}
	public static HttpResponse postHttpData(String url, String contentType, String data) {
		try {
			Log.i("=POST=", url);
			Log.i("=POST=", contentType);
			Log.i("=POST=", data);
			HttpClient client = new DefaultHttpClient();
			
			if (url.startsWith("http://")||url.startsWith("https://")) {
			}else{
				String s = AppNikita.getInstance().getBaseUrl();
				if (s.endsWith("/")) {
					url=s+url;
				}else{
					url=s+"/"+url;
				}
			}
			
			Generator.out("DEBUG-HTTPd", url);
			//13kcrazy
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			client = new DefaultHttpClient(cm, params);
			//13kcrazy
			
			HttpPost post = new HttpPost(url);	
			setHeader(post);
			BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
			basicHttpEntity.setContentType(contentType);
			basicHttpEntity.setContent(new ByteArrayInputStream(data.getBytes()));
			
			post.setEntity(basicHttpEntity);
			HttpResponse response = client.execute(post);
			Log.i("=POST=", "RESPONSE : "+response.getStatusLine().getStatusCode());
		 
			return response;
			    
		}catch (Exception e) {
	    	return null;
		}	
	}	
	public static HttpResponse multipartHttp(String url, Hashtable<String, String> arg, String imagename, InputStream file) {
		HashtableMulti<String, String, InputStream> inps = new HashtableMulti<String, String, InputStream>  ();
		inps.put("image", imagename, file);		
		return multipartHttp(url, arg, inps);
	}
	
	public static HttpResponse multipartHttp(String url, Hashtable<String, String> arg, HashtableMulti<String, String, InputStream> file) {
		Generator.out("DEBUG-HTTPm", url);
		try {
			HttpClient client = new DefaultHttpClient();
			
			//13kcrazy
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			client = new DefaultHttpClient(cm, params);
			//13kcrazy
			
			HttpPost post = new HttpPost(url);	
			setHeader(post);
			 
			Log.i("=POST=",url); 
			//multipart/form-data
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			Nset nset = new Nset(arg) ;
			String[] keys =  nset.getObjectKeys();
			for (int i = 0; i < keys.length; i++) {
				reqEntity.addPart(keys[i], new StringBody( nset.getData(keys[i]).toString() ) );
				Log.i("=POST=",keys[i]+" ==> "+nset.getData(keys[i]).toString());
			}				
			
			
			nset = new Nset(file) ;
			keys =  nset.getObjectKeys();
			for (int i = 0; i < keys.length; i++) {
				reqEntity.addPart(keys[i], new InputStreamBody( file.getData(keys[i]),  file.get(keys[i])) );
				Log.i("=POST=",keys[i]+" ==> "+nset.getData(keys[i]).toString());
			}	
			//reqEntity.addPart("image", new InputStreamBody(file, imagename) );
			 
			 
			
			post.setEntity(reqEntity);
			
			 

			HttpResponse response = client.execute(post);
			Log.i("=POST=", "RESPONSE : "+response.getStatusLine().getStatusCode());
			return response;
			    
		}catch (Exception e) {
	    	return null;
		}	
	}
	 
	
	public static  SSLSocketFactory getSslSocketFactory() {
		SSLSocketFactory sf = null; 
		try {
			 // Get an instance of the Bouncy Castle KeyStore format
          KeyStore trusted = KeyStore.getInstance("BKS");//"BKS
          // Get the raw resource, which contains the keystore with
          // your trusted certificates (root and any intermediate certs)
          InputStream in = Utility.getAppContext().getResources().openRawResource(R.raw.dipo);
          //InputStream in = Utility.getAppContext().getAssets().open("bcafx.bks");
          try {
              // Initialize the keystore with the provided trusted certificates
              // Also provide the password of the keystore
            //  trusted.load(in, "bcaf12345".toCharArray());
        	  trusted.load(in, "dipo.2016".toCharArray());
        	  
          } finally {
              in.close();
          }
          // Pass the keystore to the SSLSocketFactory. The factory is responsible
          // for the verification of the server certificate.
           sf = new SSLSocketFactory(trusted);
          // Hostname verification from certificate
          // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
          sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
		return sf;
	}
	
	
}
