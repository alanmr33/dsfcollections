package com.nikita.mobile.finalphase.connection;

import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.stream.Stream;
import com.nikita.mobile.finalphase.utility.Utility;

public class Syncronizer {
	public static String getHttp(String...url){
		 
		return Utility.getHttpConnection(Utility.getURLenc(url));
	}
	public static Recordset getHttpStream(String...url){
		return Stream.downStream(Syncronizer.getHttp(url));
	}
	
	public static void saveToDB(String table, String...url){
		saveToDB(table, getHttpStream(url));
	}
	
	public static void saveToDB(String table, Recordset rst){
		 Connection.DBdelete(table);
		 Connection.DBdrop(table);
		 
		 createToDB(table, rst);
		 insertToDB(table, rst);
	}
	public static void insertToDB(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 String[] field = new String[rst.getCols()];
			 for (int j = 0; j < rst.getCols(); j++) {
				 field[j] = rst.getHeader(j)+"="+rst.getText(i, j).trim();//matikan spasy
			 }
			 Connection.DBinsert(table, field);
		 }
	}
	public static void createToDB(String table, Recordset rst){
		 String[] field = new String[rst.getCols()];
		 for (int j = 0; j < rst.getCols(); j++) {
			 if ( rst.getHeader(j).equals("sequence_id")) {
				 field[j] = "-"+rst.getHeader(j);
			 }else{
				 field[j] = rst.getHeader(j);
			 }			 
		 }
		 Connection.DBcreate(table,field);
	}
	public static void deleteToDB(String table, Recordset rst){
		 for (int i = 0; i < rst.getRows(); i++) {
			 Connection.DBdelete(table, rst.getHeader(0)+"=?", new String[]{rst.getText(i, 0)});
		 }
	}
}
