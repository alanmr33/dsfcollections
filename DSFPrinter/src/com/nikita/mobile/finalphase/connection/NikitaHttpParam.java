package com.nikita.mobile.finalphase.connection;

import java.net.URLEncoder;
import java.util.Hashtable;

import org.apache.http.message.BasicNameValuePair;

import com.rkrzmail.nikita.data.Nset;

 
 
public class NikitaHttpParam {
	private String urls = "";
	private Hashtable<String, String> args = new Hashtable<String, String>() ;
	
	
	public static NikitaHttpParam create(String url, String key, String value){
		NikitaHttpParam nikitaHttpParam=new NikitaHttpParam();
		nikitaHttpParam.add(url).add(key, value);
		return nikitaHttpParam;
	}
	public static NikitaHttpParam create(){
		 return new NikitaHttpParam();
	}
	public static NikitaHttpParam create(Hashtable<String, String> arg){
		NikitaHttpParam nikitaHttpParam=new NikitaHttpParam(arg);
		return nikitaHttpParam;
	}
	
	public NikitaHttpParam(){
		
	}	
	public NikitaHttpParam(Hashtable<String, String> arg){
		args=arg;
	}
	public NikitaHttpParam add(String url){
		this.urls=url;
		return this;
	}
	public NikitaHttpParam add(String key, String value){
		args.put(key, value) ;
		return this;
	}
	public Hashtable<String, String> get(){
		return args;
	}
	public String getURL(){
		return urls;
	}
	public String toString() {
		StringBuffer sbBuffer = new StringBuffer(urls);
 
		Nset  nset = new Nset(args) ;
		String[] keys =  nset.getObjectKeys();
		for (int i = 0; i < keys.length; i++) {
			sbBuffer.append(i!=0?"&":"?").append(keys[i]).append("=").append( URLEncoder.encode( nset.getData( keys[i]).toString() ) );
		}	
		return sbBuffer.toString();
	}
}
