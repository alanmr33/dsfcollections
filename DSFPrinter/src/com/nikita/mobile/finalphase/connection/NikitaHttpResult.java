package com.nikita.mobile.finalphase.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.nikita.mobile.finalphase.database.Recordset;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
 
public class NikitaHttpResult {
	private Hashtable<String, Object> args = new Hashtable<String, Object>() ;
	private MultipartEntity multipartEntity = new MultipartEntity();
	
	public static NikitaHttpResult create(){
		 return new NikitaHttpResult();
	}
	
	public NikitaHttpResult(){
		
	}	
	
	public NikitaHttpResult add(String name, Recordset valueRes){
		try {
			multipartEntity.addPart(name, new StringBody(valueRes.toString())); 
		} catch (Exception e) { }
		return this;
	}

	public NikitaHttpResult add(String name, Nikitaset   valueRes){
		try {
			multipartEntity.addPart(name,  new StringBody(new Nset(valueRes).toJSON()) ); 
		} catch (Exception e) { } 
		return this;
	}
	
	public NikitaHttpResult add(String name, String valueRes){
		try {
			multipartEntity.addPart(name, new StringBody(valueRes)); 
		} catch (Exception e) { } 
		return this;
	}
	
	public NikitaHttpResult add(String name, InputStream valueRes){
		try {
			multipartEntity.addPart(name,  new InputStreamBody(valueRes, "") ); 
		} catch (Exception e) { }  
		return this;
	}
	public NikitaHttpResult add(String name, ContentBody valueRes){
		try {
			multipartEntity.addPart(name,  new ContentBody() {				
				@Override
				public String getTransferEncoding() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getSubType() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getMimeType() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getMediaType() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public long getContentLength() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public String getCharset() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public void writeTo(OutputStream arg0) throws IOException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public String getFilename() {
					// TODO Auto-generated method stub
					return null;
				}
			} ); 
		} catch (Exception e) { }  
		return this;
	}
	public String toString() {
		StringBuffer sbBuffer = new StringBuffer();
 
 
		return sbBuffer.toString();
	}
}
