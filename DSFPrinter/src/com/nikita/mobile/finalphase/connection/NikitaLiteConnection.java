 
package com.nikita.mobile.finalphase.connection;
/**
 * created by 13k.mail@gmail.com
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.generator.NCore;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringEscapeUtils;
 

/**
 * created by 13k.mail@gmail.com
 */
 
public class NikitaLiteConnection extends NikitaConnection{
    private String urlConnection = "";
    private String usrConnection = "";
    private String pwdConnection = "";
    private SQLiteDatabase mConn ;
    
    public NikitaConnection openConnection(String cname, String url, String user, String pass){
        pwdConnection=pass;usrConnection=user;urlConnection=url;
		try {
			if (urlConnection.startsWith("/")) {
				
			}else if (urlConnection.trim().equals("")) {	
				urlConnection=com.nikita.mobile.finalphase.utility.Utility.getDefaultPath("data.db");
			}else{
				urlConnection=com.nikita.mobile.finalphase.utility.Utility.getDefaultPath(url);
			}			
			mConn=SQLiteDatabase.openDatabase(urlConnection, null, SQLiteDatabase.OPEN_READWRITE|SQLiteDatabase.CREATE_IF_NECESSARY);	
		} catch (Exception e) { 
			e.printStackTrace();
		}			
		Connection.setSQLiteDatabase(mConn);
        return this;
    }
    public void closeConnection(){
        try {
        	mConn.close();
        } catch (Exception e) {}
    }
    
    public boolean isClosed(){
        try {
            return !mConn.isOpen();
        } catch (Exception e) {}
        return true;
    }
    
    public Nikitaset QueryPage(int page, int rowperpage, String sql, String... param) {
    	return runQuery(page, rowperpage, sql, param);
    } 
    public Nikitaset QueryPage(String sql, Nset array, int page, int rowperpage) {
    	String[] param = new String[array.getArraySize()];
     	for (int i = 0; i < array.getArraySize(); i++) {
    		if (array.getData(i).isNsetObject()) {
    			param[i]=array.getData(i).getData("val").toString();
    		}else if (array.getData(i).isNsetArray()) {
    			param[i]=array.getData(i).getData(0).toString();
    		}else{
    			param[i]=array.getData(i).toString();
    		}			
		}    	
    	return runQuery(page, rowperpage, sql, param);
    }    
    private Nikitaset runQuery(int page, int rowperpage, String s, String... param) {

        Vector<String> header = new Vector<String>();
		Vector<Vector<String>> data = new Vector<Vector<String>>();
	    Object info = null;
	    
	 
	    if (param!=null && s.contains("?") && param.length>=1) {
	    	StringBuffer sb = new StringBuffer();
	        int index = s.indexOf("?");int i=0;
			while (index >= 0) {
				String v =s.substring(0, index);
				if (v.endsWith("@")) {
					sb.append(v.substring(0,v.length()-1));
					s = s.substring(index + 1);
					index = s.indexOf("?");			
					
					sb.append(param[i]);i++;
				}else{
					sb.append(v);
					s = s.substring(index + 1);
					index = s.indexOf("?");			
					
					sb.append("'").append(StringEscapeUtils.escapeSql(param[i])).append("'");i++;
				}
			}
			sb.append(s);
			s=sb.toString();
		}
	    
		try {
			Cursor curr =  mConn.rawQuery(s, null);
			
			for (int i = 0; i < curr.getColumnCount(); i++) {
				header.addElement(curr.getColumnName(i));
			}
			if (rowperpage<=0  ) {
				while (curr.moveToNext()) {
					Vector<String> field = new Vector<String>();
					for (int i = 0; i < curr.getColumnCount(); i++) {
						field.addElement(curr.getString(i));
					}
					data.addElement(field);
				}
				
			}else{
				 try {
	                    int rows = 0; 
	                    rows = curr.getCount();
                        page=page<=0?0:page;                    
                        int pmax = rows/rowperpage+ (rows%rowperpage<=0?0:1);
                        page=page>=pmax?pmax:page;

                        for (int row = rowperpage*(page-1); row < Math.min(rowperpage*(page-1)+rowperpage, rows); row++) {
                        	curr.moveToPosition(row);
                            Vector<String> field = new Vector<String>();
                            for (int i = 0; i < curr.getColumnCount(); i++) {
                                field.addElement(curr.getString(i));
                            }
                            data.addElement(field);
                        }                 
	                    
	                    info = Nset.newObject().setData("nfid", "Nset").setData("mode", "paging").setData("rows", rows).setData("row", rowperpage).setData("page", page);
	                    
				 } catch (Exception e) { return  new Nikitaset( e.getMessage() ); }  				
			}			
			try {
				//add info type n len fields
	            if (info instanceof Nset) {                
	            }else{
	                info=Nset.newObject();
	            }
	            Nset metatype = Nset.newArray();
	            Nset metasize = Nset.newArray();
	            for (int i = 0; i < curr.getColumnCount(); i++) {                
	                metatype.addData(getTypeName(curr.getType(i))) ;
	                metasize.addData(0) ;                
	            }
	            ((Nset)info).setData("metadata", Nset.newObject().setData("type", metatype).setData("size", metasize));
			} catch (Exception e) { }
			((Nset)info).setData("core", getDatabaseCore());
			
			curr.close();
			return  new Nikitaset(header, data, "" , info);
		} catch (Exception e) { 
			return  new Nikitaset( e.getMessage() );
		}
    }
    
    private String getTypeName(int itype){
    	switch (itype) {
		case Cursor.FIELD_TYPE_NULL:
			return "STRING";
		case Cursor.FIELD_TYPE_BLOB:
			return "BLOB";
		case Cursor.FIELD_TYPE_FLOAT:
			return "FLOAT";
		case Cursor.FIELD_TYPE_INTEGER:
			return "INTEGER";
		case Cursor.FIELD_TYPE_STRING:
			return "STRING";
		default:
			return "STRING";
		}    	 
    }
    
    @Override
    public int getDatabaseCore() {
    	return NikitaConnection.CORE_SQLITE;
    }
    
    
    
    
}
