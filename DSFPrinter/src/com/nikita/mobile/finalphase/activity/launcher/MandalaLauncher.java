package com.nikita.mobile.finalphase.activity.launcher;

import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.activity.SettingActivity;
import com.nikita.mobile.finalphase.activity.SplashScreen;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.service.NikitaService;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MandalaLauncher extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.launchermandala);
		
		Utility.setAppContext(getApplicationContext());
 
		
		AppNikita.getInstance().setVirtual("@+SETTING+LAUNCHERICONID", R.drawable.mandala_launcher+""); 
		
		
		Utility.setAppContext(getApplicationContext());
		AppNikita.getInstance().setVirtual("@+SETTING+LAUNCHERICONID", R.drawable.mandala_launcher+"");
		Generator.initializeNikita(getApplicationContext());

		if (Generator.getNikitaParameters().getData("INIT-SPLASH").toInteger()>=1) {
			new Handler().postDelayed(new Runnable() {
				public void run() {
					Intent intent = new Intent(MandalaLauncher.this, LoginActivity.class);	
					intent.putExtra("iconid", R.drawable.mandala_launcher);
					startActivity(intent);
					finish();
				}
			}, Generator.getNikitaParameters().getData("INIT-SPLASH").toInteger() * 1000);			
		}else {
			Intent intent = new Intent(MandalaLauncher.this, LoginActivity.class);	
			intent.putExtra("iconid", R.drawable.mandala_launcher);
			startActivity(intent);
			finish();
		}
		
	}

}
