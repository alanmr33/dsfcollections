package com.nikita.mobile.finalphase.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.View;

import com.nikita.mobile.finalphase.connection.Syncronizer;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class StarterActivity extends AndroidActivity {
	 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		if (getIntent()!=null && getIntent().getStringExtra("nikitaformname")!=null) {	 
			String formname =  getIntent().getStringExtra("nikitaformname") ;			
			Generator.startNikitaForm(this, formname);
		}
		
		finish();
	}
	
 
}
