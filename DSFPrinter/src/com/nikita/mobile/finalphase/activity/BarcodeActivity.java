package com.nikita.mobile.finalphase.activity;



import google.PlanarYUVLuminanceSource;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.Result;
import com.google.zxing.client.android.PreferencesActivity;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Reader;
import com.google.zxing.oned.Code39Reader;
import com.google.zxing.oned.Code93Reader;
import com.google.zxing.oned.EAN13Reader;
import com.google.zxing.oned.EAN8Reader;
import com.google.zxing.oned.ITFReader;
import com.google.zxing.oned.MultiFormatOneDReader;
import com.google.zxing.oned.UPCAReader;
import com.google.zxing.oned.UPCEReader;
import com.google.zxing.pdf417.PDF417Reader;
import com.google.zxing.qrcode.QRCodeReader;
import com.nikita.mobile.finalphase.R;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class BarcodeActivity extends Activity  implements  ShutterCallback, PictureCallback, AutoFocusCallback, PreviewCallback{
    private PreviewDisplay mPreview;
    private  Camera mCamera;

    // The first rear facing camera
    private  Handler handler;
    
    private static final float BEEP_VOLUME = 0.10f;
    private static final long VIBRATE_DURATION = 200L;
    private boolean playBeep;
    private boolean vibrate;
    private MediaPlayer mediaPlayer;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.capture);
		
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		if (getIntent()!=null) {
			 
		}
		
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        playBeep = prefs.getBoolean(PreferencesActivity.KEY_PLAY_BEEP, true);
        if (playBeep) {
	          // See if sound settings overrides this
	          AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
	          if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
	        	  playBeep = false;
	          }
        }
        vibrate = prefs.getBoolean(PreferencesActivity.KEY_VIBRATE, false);
        initBeepSound();
		
        mPreview = new PreviewDisplay(this);
        addView(mPreview, new LayoutParams(getWidth(), getHeight()) ); 
        
        
        CameraRect cameraRect = new CameraRect(this);
        addView(cameraRect, new FrameLayout.LayoutParams(getWidthScanArea(), getHeightScanArea(), Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL) ); 
 
        handler = new Handler();        
       
        
        handler.postDelayed(new Runnable() {
			public void run() {
				if (mCamera!=null) {
					setZoom(mCamera);
					requestAutoFocus();		
				}						
			}
		}, 1000);
        
        
        
        IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.rkrmail.mobile.barcode");	 
		registerReceiver(_broadcastReceiver, intentFilter);
	}
	protected void onResume() {
        super.onResume();
        mCamera = Camera.open();
        mPreview.setCamera(mCamera);
    }

    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
        
    }
    private boolean is2Donly = false;
    public void addView(View child, android.view.ViewGroup.LayoutParams params){
		((ViewGroup)findViewById(R.id.barcodeContainer)).addView(child, params);
	}
    public int getWidth() {
		return getResources().getDisplayMetrics().widthPixels;
	}
	public int getHeight() {
		return getResources().getDisplayMetrics().heightPixels;
	}
	public int getWidthScanArea() {
		int sa = getWidth()*2/3;
		if (is2Donly) {
			return Math.min(sa, getHeight()*2/3) ;
		}
		return sa;
	}
	public int getHeightScanArea() {
		int sa = getHeight()*2/3;
		if (is2Donly) {
			return Math.min(sa, getWidth()*2/3) ;
		}
		return sa;
	}
	private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
          // The volume on STREAM_SYSTEM is not adjustable, and users found it too loud,
          // so we now play on the music stream.
          setVolumeControlStream(AudioManager.STREAM_MUSIC);
          mediaPlayer = new MediaPlayer();
          mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
          mediaPlayer.setOnCompletionListener(beepListener);

          AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
          try {
            mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(),
                file.getLength());
            file.close();
            mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
            mediaPlayer.prepare();
          } catch (IOException e) {
            mediaPlayer = null;
          }
        }
      }

      private void playBeepSoundAndVibrate() {
	        if (playBeep && mediaPlayer != null) {
	        	mediaPlayer.start();
	        }
	        if (vibrate) {
		          Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		          vibrator.vibrate(VIBRATE_DURATION);
	        }
      }
      private final OnCompletionListener beepListener = new OnCompletionListener() {
    	    public void onCompletion(MediaPlayer mediaPlayer) {
    	      mediaPlayer.seekTo(0);
    	    }
    	  };

	
    	  private void setFlash(Camera camera) {
  			Camera.Parameters parameters = camera.getParameters();
  			parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
  		    camera.setParameters(parameters);
  		}
  		private void setFlashOff(Camera camera) {
  			Camera.Parameters parameters = camera.getParameters();
  			parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
  		    camera.setParameters(parameters);
  		}
  		private void setZoom(Camera camera) {
  			final int TEN_DESIRED_ZOOM = 27;
  			final int DESIRED_SHARPNESS = 30;
  			
  			Camera.Parameters parameters = camera.getParameters();
  			
  		    String zoomSupportedString = parameters.get("zoom-supported");
  		    if (zoomSupportedString != null && !Boolean.parseBoolean(zoomSupportedString)) {
  		      return;
  		    }

  		    int tenDesiredZoom = TEN_DESIRED_ZOOM;

  		    String maxZoomString = parameters.get("max-zoom");
  		    if (maxZoomString != null) {
  		      try {
  			        int tenMaxZoom = (int) (10.0 * Double.parseDouble(maxZoomString));
  			        if (tenDesiredZoom > tenMaxZoom) {
  			          tenDesiredZoom = tenMaxZoom;
  			        }
  		      	} catch (NumberFormatException nfe) {}
  		    }

  		    String takingPictureZoomMaxString = parameters.get("taking-picture-zoom-max");
  		    if (takingPictureZoomMaxString != null) {
  		      try {
  			        int tenMaxZoom = Integer.parseInt(takingPictureZoomMaxString);
  			        if (tenDesiredZoom > tenMaxZoom) {
  			          tenDesiredZoom = tenMaxZoom;
  			        }
  		        } catch (NumberFormatException nfe) {}
  		    }

  		    String motZoomValuesString = parameters.get("mot-zoom-values");
  		    if (motZoomValuesString != null) {
  		      tenDesiredZoom = findBestMotZoomValue(motZoomValuesString, tenDesiredZoom);
  		    }

  		    String motZoomStepString = parameters.get("mot-zoom-step");
  		    if (motZoomStepString != null) {
  		      try {
  		        double motZoomStep = Double.parseDouble(motZoomStepString.trim());
  		        int tenZoomStep = (int) (10.0 * motZoomStep);
  		        if (tenZoomStep > 1) {
  		          tenDesiredZoom -= tenDesiredZoom % tenZoomStep;
  		        }
  		      } catch (NumberFormatException nfe) {
  		        // continue
  		      }
  		    }

  		    // Set zoom. This helps encourage the user to pull back.
  		    // Some devices like the Behold have a zoom parameter
  		    if (maxZoomString != null || motZoomValuesString != null) {
  		      parameters.set("zoom", String.valueOf(tenDesiredZoom / 10.0));
  		    }

  		    // Most devices, like the Hero, appear to expose this zoom parameter.
  		    // It takes on values like "27" which appears to mean 2.7x zoom
  		    if (takingPictureZoomMaxString != null) {
  		      parameters.set("taking-picture-zoom", tenDesiredZoom);
  		    }
  		    
  		    camera.setParameters(parameters);
  		 }
    	  
  		 private static int findBestMotZoomValue(CharSequence stringValues, int tenDesiredZoom) {
			 	final Pattern COMMA_PATTERN = Pattern.compile(",");
			    int tenBestValue = 0;
			    for (String stringValue : COMMA_PATTERN.split(stringValues)) {
			      stringValue = stringValue.trim();
			      double value;
			      try {
			        value = Double.parseDouble(stringValue);
			      } catch (NumberFormatException nfe) {
			        return tenDesiredZoom;
			      }
			      int tenValue = (int) (10.0 * value);
			      if (Math.abs(tenDesiredZoom - value) < Math.abs(tenDesiredZoom - tenBestValue)) {
			        tenBestValue = tenValue;
			      }
			    }
			    return tenBestValue;
		}
	public void takePictureFrame(){
		Log.i("Nikita barcode", "takePictureFrame");
		mCamera.setOneShotPreviewCallback(this);
	}

    public void requestAutoFocus(){
    	Log.i("Nikita barcode", "requestAutoFocus");
    	mCamera.autoFocus(this);
    }
	
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		Log.i("Nikita barcode", "onPreviewFrame");
		PlanarYUVLuminanceSource source=new PlanarYUVLuminanceSource(data, camera.getParameters().getPreviewSize().width, camera.getParameters().getPreviewSize().height, camera.getParameters().getPreviewSize().width*1/6,  camera.getParameters().getPreviewSize().height*1/6, camera.getParameters().getPreviewSize().width*2/3, camera.getParameters().getPreviewSize().height*2/3);
		Result result= null;
		try {
			result= new QRCodeReader().decode(new BinaryBitmap(new HybridBinarizer(source)));
			result.getText();
			Log.i("Nikita barcode", result.getText());
		} catch (Exception e) {}
		try {
			 String s = new Code39Reader().decode(new BinaryBitmap(new HybridBinarizer(source))).getText();
			 Log.i("Nikita barcode", s);

		} catch (Exception e) {}
		try {
			Hashtable hashtable = new Hashtable ();
			
			hashtable.put(BarcodeFormat.CODE_128, BarcodeFormat.CODE_128);
			hashtable.put(BarcodeFormat.CODE_39, BarcodeFormat.CODE_39);
			hashtable.put(BarcodeFormat.CODE_93, BarcodeFormat.CODE_93);
			hashtable.put(BarcodeFormat.EAN_13, BarcodeFormat.EAN_13);
			hashtable.put(BarcodeFormat.EAN_8, BarcodeFormat.EAN_8);
			hashtable.put(BarcodeFormat.UPC_A, BarcodeFormat.UPC_A);
			hashtable.put(BarcodeFormat.UPC_E, BarcodeFormat.UPC_E);
			hashtable.put(BarcodeFormat.UPC_EAN_EXTENSION, BarcodeFormat.UPC_EAN_EXTENSION);
			result = new MultiFormatOneDReader(hashtable).decode(new BinaryBitmap(new HybridBinarizer(source)));
			 Log.i("Nikita barcode", result.getText() );		 
		} catch (Exception e) {
			Log.i("Nikita barcode", "onPreviewFrame");
			requestAutoFocus();	
			return;
		}
		
		if (getIntent()!=null && getIntent().getStringExtra("autosccan")!=null && getIntent().getStringExtra("autosccan").equals("true")) {
			//none
			
		}else if (result!=null){
			Intent intent = new Intent();
			intent.putExtra("BARCODE", result.getText());
			intent.putExtra("TEXT", result.getText());
			intent.putExtra("FORMAT", result.getBarcodeFormat().toString());
			setResult(RESULT_OK, intent);
			finish();
		}else{
			requestAutoFocus();	
		}
	}
	
	private BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			//finish
			if (intent.getStringExtra("finish")!=null && intent.getStringExtra("finish").equalsIgnoreCase("true")) {
				finish();
			}	
			//nextscan
			if (intent.getStringExtra("nextscan")!=null && intent.getStringExtra("nextscan").equalsIgnoreCase("true")) {
				requestAutoFocus();
			}			
		}
	};	
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_broadcastReceiver);
	}
	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		Log.i("Nikita barcode", "onAutoFocus");
		takePictureFrame();
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		 
		
	}

	@Override
	public void onShutter() {
		mCamera.startPreview();
	}

 


	class PreviewDisplay extends ViewGroup implements SurfaceHolder.Callback{
	    private final String TAG = "Preview";
	    private BarcodeActivity parent;
	    SurfaceView mSurfaceView;
	    SurfaceHolder mHolder;
	    Size mPreviewSize;
	    List<Size> mSupportedPreviewSizes;
	    Camera mCamera;

	    PreviewDisplay(BarcodeActivity context) {
	        super(context);
	        
	        parent = context;
	        mSurfaceView = new SurfaceView(context);
	        addView(mSurfaceView);

	        // Install a SurfaceHolder.Callback so we get notified when the
	        // underlying surface is created and destroyed.
	        mHolder = mSurfaceView.getHolder();
	        mHolder.addCallback(this);
	        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	        
	         
	       
	    }
	    
	    public void setCamera(Camera camera) {
	        mCamera = camera;
	        if (mCamera != null) {
	            mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
	            requestLayout();
	        }
	    }

	    public void switchCamera(Camera camera) {
	       setCamera(camera);
	       try {
	           camera.setPreviewDisplay(mHolder);
	       } catch (IOException exception) {
	           Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
	       }
	       Camera.Parameters parameters = camera.getParameters();
	       parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
	       requestLayout();

	       camera.setParameters(parameters);
	    }

	    
	    @Override
	    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	        // We purposely disregard child measurements because act as a
	        // wrapper to a SurfaceView that centers the camera preview instead
	        // of stretching it.
	        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
	        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
	        setMeasuredDimension(width, height);

	        if (mSupportedPreviewSizes != null) {
	            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
	        }
	         
	    }

	    @Override
	    protected void onLayout(boolean changed, int l, int t, int r, int b) {
	        if (changed && getChildCount() > 0) {
	            final View child = getChildAt(0);

	            final int width = r - l;
	            final int height = b - t;

	            int previewWidth = width;
	            int previewHeight = height;
	            if (mPreviewSize != null) {
	                previewWidth = mPreviewSize.width;
	                previewHeight = mPreviewSize.height;
	            }

	            // Center the child SurfaceView within the parent.
	            if (width * previewHeight > height * previewWidth) {
	                final int scaledChildWidth = previewWidth * height / previewHeight;
	                child.layout((width - scaledChildWidth) / 2, 0,
	                        (width + scaledChildWidth) / 2, height);
	            } else {
	                final int scaledChildHeight = previewHeight * width / previewWidth;
	                child.layout(0, (height - scaledChildHeight) / 2,
	                        width, (height + scaledChildHeight) / 2);
	            }
	        }
	    }

	    public void surfaceCreated(SurfaceHolder holder) {
	        // The Surface has been created, acquire the camera and tell it where
	        // to draw.
	        try {
	            if (mCamera != null) {
	                mCamera.setPreviewDisplay(holder);
	                
	            }
	        } catch (IOException exception) {
	            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
	        }
	    }

	    public void surfaceDestroyed(SurfaceHolder holder) {
	        // Surface will be destroyed when we return, so stop the preview.
	        if (mCamera != null) {
	            mCamera.stopPreview();
	        }
	    }


	    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
	        final double ASPECT_TOLERANCE = 0.1;
	        double targetRatio = (double) w / h;
	        if (sizes == null) return null;

	        Size optimalSize = null;
	        double minDiff = Double.MAX_VALUE;

	        int targetHeight = h;

	        // Try to find an size match aspect ratio and size
	        for (Size size : sizes) {
	            double ratio = (double) size.width / size.height;
	            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
	            if (Math.abs(size.height - targetHeight) < minDiff) {
	                optimalSize = size;
	                minDiff = Math.abs(size.height - targetHeight);
	            }
	        }

	        // Cannot find the one match the aspect ratio, ignore the requirement
	        if (optimalSize == null) {
	            minDiff = Double.MAX_VALUE;
	            for (Size size : sizes) {
	                if (Math.abs(size.height - targetHeight) < minDiff) {
	                    optimalSize = size;
	                    minDiff = Math.abs(size.height - targetHeight);
	                }
	            }
	        }
	        return optimalSize;
	    }

	    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
	        // Now that the size is known, set up the camera parameters and begin
	        // the preview.
	        Camera.Parameters parameters = mCamera.getParameters();
	        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
	        requestLayout();

	        mCamera.setParameters(parameters);
	        mCamera.startPreview();
	        
	    }   
	}
	
	class CameraRect extends View{
		public CameraRect(Context context) {
			super(context);
		}
		protected void onDraw(Canvas canvasObject) {
	        super.onDraw(canvasObject);
	        int x = 1;
	        int y = 1;
	        int width = getWidth()-2;
	        int height = getHeight()-2;
	        
	        
	        if (true) {
	        	Paint thePaint = new Paint();   
	 	        thePaint.setColor(Color.RED);
	 	        thePaint.setStyle(Style.STROKE);
	 	        thePaint.setAlpha(127);
	 	        thePaint.setStrokeWidth(1);
	 	        RectF rectnagle1 = new RectF(x,getHeight()/2-1,x+width,getHeight()/2+1);
	 	        canvasObject.drawRect(rectnagle1, thePaint); 
			}
	       	         
	        Paint  thePaint = new Paint();   
	        thePaint.setColor(Color.BLUE);
	        thePaint.setStyle(Style.STROKE);
	        thePaint.setStrokeWidth(2);
	        RectF rectnagle1 = new RectF(x,y,x+width,y+height);
	        canvasObject.drawRect(rectnagle1, thePaint); 	        
   	    }
	}
}
