package com.nikita.mobile.finalphase.generator.ui.layout;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class HorizontalLayout  extends ComponentGroup{

	public HorizontalLayout(NikitaControler form) {
		super(form);
	}


	private LinearLayout view;
	 
	
	public View onCreate(NikitaControler form) {
		
		view=new LinearLayout(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		((LinearLayout)view).setOrientation(LinearLayout.HORIZONTAL);
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		
		LinearLayout container = view;
		if (getStyle()!=null && getStyle().getViewStyle().contains("n-scroll:true")) {
			HorizontalScrollView hScrollView = new HorizontalScrollView(form.getActivity());
			hScrollView.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			
			container = new LinearLayout(form.getActivity());
			container.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			((LinearLayout)container).setOrientation(LinearLayout.HORIZONTAL);
			
			hScrollView.addView(container, Utility.getMarginLayoutParams(hScrollView) );
			view.addView(hScrollView, Utility.getMarginLayoutParams(hScrollView) );
		}
		int divider = 0;
		if (getStyle()!=null && getStyle().getViewStyle().contains("n-divider:")) {
			divider =  getStyle().getInternalObject().getData("style").getData("n-divider").toInteger();
			Log.i("Div", divider+":"+getStyle().getViewStyle());
			Log.i("Div", divider+":"  );
		}
		
		
		for (int i = 0; i < getComponentCount(); i++) {
			if (getComponent(i).getLabel().equals("")) {
				getComponent(i).setLabel(".");
			}
			
			View child = getComponent(i).startCreateUI(getNikitaComponent());
			MarginLayoutParams marginLayoutParams= Utility.getMarginLayoutParams(child);
			if (divider>=1) {
				int w =getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels;
				marginLayoutParams.width = w /divider;
			}			
			 
			
			container.addView(child, marginLayoutParams );		
		}
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
	}
	
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
	}

}
