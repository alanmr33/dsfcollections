package com.nikita.mobile.finalphase.generator.ui;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class WebViewUI extends Component{

	static final int STARTDATE = 1;
	
	public WebViewUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	private WebView web;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compweb);
		web = (WebView)view.findViewById(R.id.webView);
		web.getSettings().setJavaScriptEnabled(true);
		web.loadUrl(super.getText());
		web.setTag(super.getText());
		
		if (super.getLabel().equalsIgnoreCase("")) {
			((TextView)view.findViewById(R.id.lblLabel)).setVisibility(View.GONE);
		}
		
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		if (web != null) {
			web.loadUrl(text);
		}
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);	
		
		View  v = view.findViewById(R.id.webView);
		if (v instanceof WebView) {
			((WebView)v).getTag();
		}
		
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (web != null) {
			StringBuffer sbuBuffer = new StringBuffer("");			
			if (!((String)web.getTag()).equalsIgnoreCase("")) {
				sbuBuffer.append((String)web.getTag());
			}			
			return sbuBuffer.toString();
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	

	
}
