package com.nikita.mobile.finalphase.generator.ui;

import java.util.Locale;

import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class TextareaUI extends Component{
	public TextareaUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.comptextarea);

		view.findViewById(R.id.imgText).setVisibility(View.GONE);
		setText(super.getText());	
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
	 
		view.findViewById(R.id.txtText).setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				NCore.actionActivityTimeout();
				return false;//not consume
			}
		});
		((EditText)view.findViewById(R.id.txtText)).setHint(getHint());
		
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	




	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
					String val =  getStyle().getInternalStyle().getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
					if (val.equals("true")) {
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittexterror);
					}else{
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittext);
					}	
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText) {
			((EditText)v).setEnabled(getEnable());	
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof TextView) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
	}
	
	public void onChangeViewUi() {	 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	public void setText(final String text) {
		super.setText(text);	
 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					View v = view.findViewById(R.id.txtText);
					if (v instanceof EditText) {			
						if ( !text.equals( ((EditText)v).getText().toString() )) {
							InputFilter[] f = ((EditText)v).getFilters();
							((EditText)v).setFilters(new InputFilter[0]);
								((EditText)v).setText( text );
							((EditText)v).setFilters(f);
						}				
						
						((EditText)v).setEnabled(getEnable());	
					}
				}
			});
		} 
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText && getStyle()!=null) {
			validateGravity(((EditText)v), getStyle().getInternalObject().getData("style"));
		}
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.txtText);
			if (v instanceof EditText) {
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
}
