package com.nikita.mobile.finalphase.generator.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class TimeUI extends Component{
	int hour, minute, second;


	static final int STARTDATE = 1;
	
	public TimeUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compdate);



		view.findViewById(R.id.imgDate).setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				onDialog(TYPE_TEXTBOX).show();
			}
		});
		
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.edtDate);
		if (v instanceof EditText) {
			((EditText)v).setText(getText());
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.edtDate);
			if (v instanceof EditText) {
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	
	private TimePickerDialog.OnTimeSetListener mStartTime = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view1, int hourOfDay, int minuteOfHour) {
			hour = hourOfDay;
			minute = minuteOfHour;
			String stime = LPad("" + hour, "0", 2) + ":" + LPad("" + minute, "0", 2);
//			edtDate.setText(stime);
			View v = view.findViewById(R.id.edtDate);
			if (v instanceof EditText) {
				((EditText)v).setText(stime);
			}
		}
	};
	
	private Dialog onDialog(int id){
		switch (id) {
		case STARTDATE:
			return new TimePickerDialog(getNikitaComponent().getActivity(), mStartTime, hour, minute, true);
		}
		return null;
	}
	
	private static String LPad(String schar, String spad, int len) {
		String sret = schar;
		for (int i = sret.length(); i < len; i++) {
			sret = spad + sret;
		}
		return new String(sret);
	}
	
}
