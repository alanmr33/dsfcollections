package com.nikita.mobile.finalphase.generator.ui.layout;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;

import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class DivLayout extends ComponentGroup{

	public DivLayout(NikitaControler form) {
		super(form);
		
	}
	private LinearLayout view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=new LinearLayout(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		((LinearLayout)view).setOrientation(LinearLayout.VERTICAL);
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		for (int i = 0; i < getComponentCount(); i++) {
			View child = getComponent(i).startCreateUI(getNikitaComponent());
			view.addView(child, Utility.getMarginLayoutParams(child) );		
		}
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
	}
	
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
	}

}
