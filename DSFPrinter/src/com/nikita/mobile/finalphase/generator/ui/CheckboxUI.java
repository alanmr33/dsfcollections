package com.nikita.mobile.finalphase.generator.ui;

import java.util.Locale;
import java.util.Vector;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class CheckboxUI extends Component{
	private Vector<CheckBox> vCheckBoxs = new Vector<CheckBox>();
	private LinearLayout container ;
	public CheckboxUI(NikitaControler form) {
		super(form);
		 
	}

	private View view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compcheckbox);
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		int cols=getCols();int col = 0;
		
		container = new LinearLayout(form.getActivity());
		container.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		((LinearLayout)view.findViewById(R.id.frmContainer)).addView(container);
		container.setOrientation(LinearLayout.VERTICAL);
		
		vCheckBoxs.removeAllElements();
		
		LinearLayout containercols =null ;
		if (cols>=2) {
			containercols = new LinearLayout(form.getActivity());
			containercols.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			containercols.setOrientation(LinearLayout.HORIZONTAL);
			container.addView(containercols);
		}
		
        for (int i = 0; i < getData().getArraySize(); i++) {
             
        	CheckBox checkBox = new CheckBox(form.getActivity());
        	
        	vCheckBoxs.add(checkBox);
        	
            if(getData().getData(i).getData("id").toString().length()>=1){
            	checkBox.setTag(getData().getData(i).getData("id").toString());
            	checkBox.setText(getData().getData(i).getData("text").toString());
            }else if(getData().getData(i).getArraySize()>=2){
                checkBox.setTag(getData().getData(i).getData(0).toString());
            	checkBox.setText(getData().getData(i).getData(1).toString());
            }else if(getData().getData(i).getArraySize()>=1){
            	checkBox.setTag(getData().getData(i).getData(0).toString());
             	checkBox.setText(getData().getData(i).getData(0).toString());
            }else{
            	checkBox.setTag(getData().getData(i).toString());
            	checkBox.setText(getData().getData(i).toString());
            }  
            if (super.getText().contains("\""+(String)checkBox.getTag()+"\"")) {
            	checkBox.setChecked(true);
			}
            
            checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							CheckboxUI.this.runRoute();						
						}
					});		
				}
			});
            
            if (cols>=2) {
            	containercols.addView(checkBox);
            	if ( cols >=1 && ((i+1) % cols) == 0) {
            		containercols = new LinearLayout(form.getActivity());
        			containercols.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        			containercols.setOrientation(LinearLayout.HORIZONTAL);
        			container.addView(containercols);
                }else{
                    col++;
                }
            }else{
            	container.addView(checkBox);
            }
            
        }
        mobileLabelOrientation(view.findViewById(R.id.incLabels));
        
        refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	private void fillDataOnUI(){
		if (view!=null) {
			 
		}	
	}
	private int getCols(){
        String s = getViewStyle();
        if (s.contains("n-cols:")) {
            s=s.substring(s.indexOf("n-cols:")+7)+";";
            return Utility.getInt(s.substring(0,s.indexOf(";")));
        }
        return 0;
    }
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText) {
			((EditText)v).setText(getText());
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
		
		for (int i = 0; i < vCheckBoxs.size(); i++) {
			if (vCheckBoxs.elementAt(i)!=null) {
				vCheckBoxs.elementAt(i).setEnabled(isEnable());
			}
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
		Nset style = Nset.newObject();
		if (getStyle()!=null) {
			style = getStyle().getInternalObject().getData("style");
		}
		String val =  style.getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
		if (container!=null && val.equals("true")) {
			container.setBackgroundResource(R.drawable.nfedittexterror);
		}else if (container!=null ){			
			container.setBackgroundResource(0);
		}	
	}
	
	public String getText() {
		if (view!=null) {
			Nset n = Nset.newArray();
			for (int i = 0; i < vCheckBoxs.size(); i++) {
				if (vCheckBoxs.elementAt(i).isChecked()) {
					n.addData((String)vCheckBoxs.elementAt(i).getTag());
				}
			}
			return n.toJSON();
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
	}
}
