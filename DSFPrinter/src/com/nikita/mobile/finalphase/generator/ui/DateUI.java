package com.nikita.mobile.finalphase.generator.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class DateUI extends Component{
	int hour, minute, mYear, mMonth, mDay, second;
	private String[] arrMonth = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
	


	 
	
	public DateUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compdate);


		Calendar c = Calendar.getInstance();
		//mYear = c.get(Calendar.YEAR);
 		//mMonth = c.get(Calendar.MONTH);
 		//mDay = c.get(Calendar.DAY_OF_MONTH);

		view.findViewById(R.id.imgDate).setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (isEnable()) {
					showDialogDate().show();
				}
				
			}
		});		 
		view.findViewById(R.id.edtDate).setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (isEnable()) {
					showDialogDate().show();
				}
			}
		});	

		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.edtDate);
		if (v instanceof EditText) {
			((EditText)v).setText(super.getText());
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
		
	
		setTextDate(super.getText());
		
		v = view.findViewById(R.id.edtDate);
		if (v != null ) {			 
			v.setEnabled(isEnable());
		}
		
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
		String val =  getStyle().getInternalStyle().getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
		if (val.equals("true")) {
			view.findViewById(R.id.edtDate).setBackgroundResource(R.drawable.nfedittexterror);
		}else{
			view.findViewById(R.id.edtDate).setBackgroundResource(R.drawable.nfedittext);
		}
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.edtDate);
			if (v instanceof EditText) {
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
	}
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view1, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			String sdate = LPad(mDay + "", "0", 2) + "/" + arrMonth[mMonth] + "/" + mYear ;
			
			setTextDate(sdate);
		}
	};
	
	private void prepareTextDate(){
		String sdate = getText();
			try {
			Calendar c = Calendar.getInstance();
			if (sdate.trim().equals("")) {
				sdate = new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
			}
				
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			c.setTime(simpleDateFormat.parse(sdate));
			
			mYear = c.get(Calendar.YEAR);
	 		mMonth = c.get(Calendar.MONTH);
	 		mDay = c.get(Calendar.DAY_OF_MONTH);
		} catch (Exception e) { }
	}
	private void setTextDate(String sdate){
		super.setText(sdate); 
		 
		
		View v = view.findViewById(R.id.edtDate);
		if (v instanceof EditText) {
			((EditText)v).setText(sdate);
			
			getNikitaComponent().runOnActionThread(new Runnable() {
				public void run() {
					DateUI.this.runRoute();							 					
				}
			});	
		}
	}
	private static String LPad(String schar, String spad, int len) {
		String sret = schar;
		for (int i = sret.length(); i < len; i++) {
			sret = spad + sret;
		}
		return new String(sret);
	}
	
	@Override
	public Style getStyle() {
		if (super.getStyle()!=null) {			
			return super.getStyle();
		} 
		super.setStyle(Style.createStyle());		
		return super.getStyle();
	}
	private Dialog showDialogDate(){ 
		prepareTextDate();
		DatePickerDialog datePickerDialog = new DatePickerDialog(getNikitaComponent().getActivity(),  date, mYear, mMonth, mDay);
		try {
			if (getStyle().getInternalStyle().containsKey("n-mindate")) {
				datePickerDialog.getDatePicker().setMinDate(Utility.getDate(getStyle().getInternalStyle().getData("n-mindate").toString().trim()));
			}
			if (getStyle().getInternalStyle().containsKey("n-maxdate")) {
				datePickerDialog.getDatePicker().setMaxDate(Utility.getDate(getStyle().getInternalStyle().getData("n-maxdate").toString().trim()));
			} 
		} catch (Exception e) { }
		
		return datePickerDialog;
	}
	
	
}
