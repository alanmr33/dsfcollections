package com.nikita.mobile.finalphase.generator.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class ListViewUI extends TableGridUI{

	static final int STARTDATE = 1;
	
	public ListViewUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	private LinearLayout listview;
	private ArrayAdapter adapter;
	private Vector<String> d  = new Vector<String>();
 
	public View onCreateList(NikitaControler form) {		
		view=Utility.getInflater(form.getActivity(), R.layout.complist);		
		adapter = new ArrayAdapter(form.getActivity(), 0){			 
			public int getCount() {				
				return getData().getArraySize();
			}
			public View getView(int position, View convertView, ViewGroup parent) {
				Button button = new Button(getNikitaComponent().getActivity());
				button.setText("getView"+position);
				return button;
			}
		};		 
		/*
		adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                removeAndRedrawViews();

            }
        });
		*/
		((ListView)view.findViewById(R.id.lstView)).setAdapter(adapter);
		setListViewHeightBasedOnChildren(((ListView)view.findViewById(R.id.lstView)));
		fillGridOnUI();
		
		
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	public View onCreate(NikitaControler form) {		
		view=new LinearLayout(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		((LinearLayout)view).setOrientation(LinearLayout.VERTICAL);		
		  
		listview=(LinearLayout)view;
		fillGridOnUI();
				
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		   ListAdapter listAdapter = listView.getAdapter();
		    if(listAdapter == null) return;
		    if(listAdapter.getCount() <= 1) return;

		    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
		    int totalHeight = 0;
		    View view = null;
		    for(int i = 0; i < listAdapter.getCount(); i++) {
		        view = listAdapter.getView(i, view, listView);
		        //view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
		        view.measure(0, 0);
		        totalHeight += view.getMeasuredHeight();
		    }
		    ViewGroup.LayoutParams params = listView.getLayoutParams();
		    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		    listView.setLayoutParams(params);
		    listView.requestLayout();
	}
	public void fillGridOnUI(){
		try {
			//adapter.notifyDataSetChanged();
		} catch (Exception e) { }
		
		listview.removeAllViews();
	 
		
		for (int row = 0; row < getData().getArraySize(); row++) {		
			Component vv ;
            if (adapterListener!=null) {
                vv =adapterListener.getViewItem(row, 0, this, getData());
            }else{
            	vv = new LabelUI(getNikitaComponent());
            	vv.setText(getData().getData(row).toString());
            }
			
            /*
            FrameLayout frameLayout = new FrameLayout(getNForm().getActivity());		 
			frameLayout.addView(vv.getView(), new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
			MarginLayoutParams mParams = (MarginLayoutParams)view.getLayoutParams();
			mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
			*/
            if (vv.getView()!=null) {
            	View v = vv.getView();
            	if (v.getParent() instanceof ViewGroup) {
					( (ViewGroup)v.getParent()).removeView(v);
				}else if (v.getParent() instanceof FrameLayout) {
					( (FrameLayout)v.getParent()).removeView(v);
				}else if (v.getParent() instanceof LinearLayout) {
					( (LinearLayout)v.getParent()).removeView(v);	
				}
            	listview.addView(v  );
			}
			 	
							
		}

	}
	
	public void setData(Nset data) {
		super.setData(data);	
		if (view!=null) {			 
			runOnUI(new Runnable() {
				public void run() {					 
					fillGridOnUI();
					refreshViewOnUI();
				}
			});
		} 
	}
	
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.lstView);
		if (v instanceof ListView) {
			 
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.lstView);
			 
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	
	
	public class ModelData{
		public String id;
		public String name;
		public String describe;
 
		public ModelData(String id, String name, String describe){
			this.id=id;
			this.name=name;
			this.describe=describe;
		}
		
	} 
	private class ItemAdapter extends ArrayAdapter<Vector>{
		private Vector data;
		private int layout;
		private Context contex;
 		
		public ItemAdapter(Context context, int layoutinflater, Vector dat) {
			super(context, layoutinflater, dat);
			data = dat;
			layout = layoutinflater;
			contex = context;
 		}
		 
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) contex .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//View v = inflater.inflate(layout, parent, false);
					 
			
			Button button = new Button(getNikitaComponent().getActivity());
			button.setText("getView");
			return button;
		}

	}
	 
    public int getShowPerPage() {
        return 1000000000; 
    }

    
    public int getSeletedRow() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }
	    
	
}
