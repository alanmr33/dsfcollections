package com.nikita.mobile.finalphase.generator.ui;

import java.io.File;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.database.SingleRecordset;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.stream.NfString;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

 
public class LabelUI extends Component{

	public LabelUI(NikitaControler form) {
		super(form);
		// TODO Auto-generated constructor stub
	}

	private TextView view;
	 
	@Override
	public View onCreate(NikitaControler nikitaComponent) {
		view = new TextView(nikitaComponent.getActivity());
		//view = Utility.getInflater(nikitaComponent.getActivity(), R.layout.complbl);
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		int i = 3;//Utility.dpToPx(nikitaComponent.getActivity(), 3);
		view.setPadding(i,i,i,i);
		
		setText(super.getText());
		
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	public void setStyle(Style style) {		 
		super.setStyle(style);
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					//refreshStyleOnUI();
					validateGravity(view, getStyle().getInternalStyle());
					validateFontStyle(view,  getStyle().getInternalStyle() );
				}
			});
		} 
	}
	public Style getStyle() {
		if (super.getStyle()!=null) {
			return super.getStyle();
		} else{
			super.setStyle(Style.createStyle());
		}		
		return super.getStyle();
	}
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}
	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		
		view.setText(super.getText());
		
		Spannable a  =new SpannableString("");
		CharSequence charSequence = "";
		
		validateGravity(view, getStyle().getInternalStyle());
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
		if (view instanceof TextView && getStyle()!=null) {
			validateGravity(view, getStyle().getInternalStyle());
		}
	}
	
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

}
