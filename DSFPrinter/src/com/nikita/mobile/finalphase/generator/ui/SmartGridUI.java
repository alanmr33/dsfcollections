package com.nikita.mobile.finalphase.generator.ui;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;

public class SmartGridUI extends TableGridUI{

	public SmartGridUI(NikitaControler form) {
		super(form);
	}

	private SmartGridDetailViewListener smartGridDetailViewListener;
    public interface SmartGridDetailViewListener{
        public NikitaForm onDetailView(SmartGridUI parent, int row, Object data);
    }
    public void setSmartGridDetailViewListener(SmartGridDetailViewListener listener){
        this.smartGridDetailViewListener=listener;
    }
    
    private SmartGridItemViewListener smartGridItemViewListener;
    public interface SmartGridItemViewListener{
        public Component[] onItemView(SmartGridUI parent, int row, Object data);
    }
    public void setSmartGridItemViewListener(SmartGridItemViewListener listener){
        this.smartGridItemViewListener=listener;
    }
    
    
    
    
    
    
    private String curradetail = "";
    public void setCurrentDetail(String curradetail){
        this.curradetail=curradetail;
    }
    public String getCurrentDetail(){
        return this.curradetail;
    }
    private String detailview;
    public void setDefaultDetailView(String detailview){
        this.detailview=detailview;
    }
    public String getDefaultDetailView(){
        return this.detailview;
    }
}
