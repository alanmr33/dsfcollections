package com.nikita.mobile.finalphase.generator.ui;

import java.util.Locale;
import java.util.Vector;

import android.text.Editable;
import android.text.Editable.Factory;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class TextAutoCompleteUI extends Component{
	public TextAutoCompleteUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
 
    
  
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.comptextautocomplete);
		
		view.findViewById(R.id.imgText).setVisibility(View.GONE);		 
		view.findViewById(R.id.imgText).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRouteAction("finder");							
					}
				});			
			}
		});
		
		((EditText)view.findViewById(R.id.txtText)).setOnEditorActionListener(new EditText.OnEditorActionListener() {
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_GO) {
		        	getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							runRouteAction("enter");							
						}
					});	 
		            return true;
		        }
		        return false;
		    }

			 
		});
		
		((AutoCompleteTextView)view.findViewById(R.id.txtText)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
		    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		    	//arg0.getItemAtPosition(arg2).toString()
		    	final String s = arg0.getItemAtPosition(arg2).toString();
		    	getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						getNikitaControler().setVirtual("@+AUTOTEXT", s);
						runRouteAction("click");							
					}
				});	
		    }
		});
		
		/*
		if (getStyle()!=null & getStyle().getInternalObject().getData("style").getData("n-inputtype").toString().toLowerCase().equals("number")) {
			
		}else{
			
		}
		*/
		if (isPassword()) {
			((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			((EditText)view.findViewById(R.id.txtText)).setTransformationMethod(PasswordTransformationMethod.getInstance());
		}else{
			
		}
		
		setText(super.getText());
		
				
		if (getStyle()!=null) {
			if (getStyle().getViewStyle().contains("n-finder:true")) {
				bfinder=true;
			}
		}
		if (bfinder) {
			view.findViewById(R.id.imgText).setVisibility(View.VISIBLE);
		}
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		view.findViewById(R.id.txtText).setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				NCore.actionActivityTimeout();
				return false;//not consume
			}
		});
		populatData();
		return view;
	}
	 
	 private boolean isPassword( ){
	      if (getStyle()!=null) {
	         if (getStyle().getInternalObject().getData("style").getData("n-password").toString().toLowerCase().equals("true")) {
	             return true;
	         }
	     }
	     return false;
	}
	 
	  
	public View getView() {		 
		return view;
	}
	private boolean bfinder=false;
	public void showFinder(boolean b){
		bfinder=b;
	}
	 
 
	public Style getStyle() {
		if (super.getStyle()!=null) {			
			return super.getStyle();
		} 
		super.setStyle(Style.createStyle());		
		return super.getStyle();
	}
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					//refreshStyleOnUI();
					String val =  getStyle().getInternalStyle().getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
					if (val.equals("true")) {
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittexterror);
					}else{
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittext);
					}
					
					if (getStyle().getInternalObject().getData("style").getData("n-lock").toString().toLowerCase().equals("true")) {
						
		            }
					
					 
					
				}
			});
		} 
	}
	@Override
	public void onChangeViewUi() {	 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText) {
			InputFilter[] f = ((EditText)v).getFilters();
			((EditText)v).setFilters(new InputFilter[0]);
				((EditText)v).setText( super.getText() );//?
			((EditText)v).setFilters(f);
			((EditText)v).setEnabled(getEnable());	
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
		
		
	}
	private boolean isEditable(){
        if (getStyle()!=null) {
            if (getStyle().getInternalObject().getData("style").getData("n-lock").toString().toLowerCase().equals("true")) {
                return false;
            }
        }
        return isEnable();
    }
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText && getStyle()!=null) {
			validateGravity(((EditText)v), getStyle().getInternalObject().getData("style"));
		}
		
		Nset style = Nset.newObject();
		if (getStyle()!=null) {
			style = getStyle().getInternalObject().getData("style");
		}
		
		String val =  style.getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
		if (val.equals("true")) {
			view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittexterror);
		}else{
			view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittext);
		}	
			
		if (isPassword()) {
			((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			((EditText)view.findViewById(R.id.txtText)).setTransformationMethod(PasswordTransformationMethod.getInstance());
		}else{
			if (getStyle()!=null) {				
				  
				val =  style.getData("n-input").toString().toLowerCase();
				if (val.length()>=1) {
					if (val.contains("number")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_NUMBER);
					}	else if (val.contains("email")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
					}	else if (val.contains("ucase")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
					}	else if (val.contains("phone")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_PHONE);
					}
				}
			}			
		}
		
		Vector<InputFilter> filters = new Vector<InputFilter>();
		if (!isEditable()) {
			filters.addElement(  readOnlyFilter()  ); 
		}
		if (getStyle()!=null) {						
			 			
			val =  style.getData("n-maxlength").toString().toLowerCase();
			if (val.length()>=1) {
				filters.addElement(  new InputFilter.LengthFilter(style.getData("n-maxlength").toInteger())  ); 
			}
		}
		
		
		if (filters.size()>=1) {
			InputFilter[] f = new InputFilter[filters.size()];
			for (int i = 0; i < f.length; i++) {
				f[i]=filters.elementAt(i);
			}
			((EditText)view.findViewById(R.id.txtText)).setFilters(f);
		}
		
		
		
	}
	
	private InputFilter readOnlyFilter(){
		return new InputFilter() {
		    public CharSequence filter(CharSequence source, int start, int end,Spanned dest, int dstart, int dend) {
				
		    	return source.length() < 1 ? dest.subSequence(dstart, dend) : "";
			}
		 };
	}
	private void populatData(){
		
		if (getData()!=null && view != null && (view.findViewById(R.id.txtText) instanceof AutoCompleteTextView)) {
			View v =  view.findViewById(R.id.txtText);
			AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)v;
			
			String[] data =new String[getData().getSize()];			 
			for (int i = 0; i < getData().getArraySize(); i++) {
				if(getData().getData(i).getData("id").toString().length()>=1){
		        	//checkBox.setTag(getData().getData(i).getData("id").toString());
					//code.add(getData().getData(i).getData("id").toString());
		        	//list.add(getData().getData(i).getData("text").toString());
		        	data[i]=getData().getData(i).getData("text").toString();
		        }else if(getData().getData(i).getArraySize()>=2){
		            //checkBox.setTag(getData().getData(i).getData(0).toString());
		        	//code.add(getData().getData(i).getData(0).toString());
		        	//list.add(getData().getData(i).getData(1).toString());
		        	data[i]= getData().getData(i).getData(1).toString();
		        }else if(getData().getData(i).getArraySize()>=1){
		        	// checkBox.setTag(getData().getData(i).getData(0).toString());
		        	//code.add(getData().getData(i).getData(0).toString());
		        	//list.add(getData().getData(i).getData(0).toString());
		        	data[i] = getData().getData(i).getData(0).toString();
		        }else{
		        	//checkBox.setTag(getData().getData(i).toString());
		        	//code.add(getData().getData(i).toString());
		        	//list.add(getData().getData(i).toString());
		        	data[i] = getData().getData(i).toString();
		        }  
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getNikitaControler().getActivity(),  android.R.layout.simple_dropdown_item_1line, data);
			autoCompleteTextView.setAdapter(adapter);
		}
	}
 	public void setData(Nset data) {
		super.setData(data);
		runOnUI(new Runnable() {
			public void run() {
				populatData();				
			}
		});
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.txtText);
			if (v instanceof EditText) {
				super.setText(((EditText)v).getText().toString()); //?
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			
			super.setText(getText()); 
		}	
		
		//view=null;//reset
	}
	
}
