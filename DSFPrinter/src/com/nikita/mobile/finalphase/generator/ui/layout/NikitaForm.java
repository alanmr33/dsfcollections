package com.nikita.mobile.finalphase.generator.ui.layout;

import java.util.Vector;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;

public class NikitaForm extends VerticalLayout{

	public NikitaForm(NikitaControler form) {
		super(form);
	}	
	 
	public Vector<Component> populateAllComponents(){
		Vector<Component> components = new Vector<Component>() ;
		populateComponents(this, components);		
		return components;
	}
	
	private  void populateComponents(ComponentGroup component, Vector<Component> components){
		for (int i = 0; i < component.getComponentCount(); i++) {
			if (component.getComponent(i) instanceof ComponentGroup) {
				if (components.contains((ComponentGroup)component.getComponent(i))) {					
				}else{
					components.add(component.getComponent(i));	
					populateComponents((ComponentGroup)component.getComponent(i), components);
				}				
			}else{
				components.add(component.getComponent(i));
			}
		}
	}


}
