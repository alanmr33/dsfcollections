package com.nikita.mobile.finalphase.generator.ui.layout;

import java.util.Locale;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
 

import android.view.ViewGroup.MarginLayoutParams;

import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class FrameLayout  extends ComponentGroup{

	public FrameLayout(NikitaControler form) {
		super(form);
	}


	private android.widget.FrameLayout view;
	 
	
	
	public View onCreate(NikitaControler form) {
		view=new android.widget.FrameLayout(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		
		for (int i = 0; i < getComponentCount(); i++) {
			View child = getComponent(i).onCreate(getNikitaComponent());
			
			android.widget.FrameLayout.LayoutParams layoutParams = new android.widget.FrameLayout.LayoutParams(Utility.getMarginLayoutParams(child));
			
			Nset style = Nset.newObject();	
			if (getComponent(i).getStyle()!=null) {
				style = getComponent(i).getStyle().getInternalObject().getData("style");	
			}
			String  val =   style.getData("n-layout-gravity").toString();
			if (val.length()>=1) {
				val=val.trim().toLowerCase(Locale.ENGLISH);
				if (val.equals("center")||val.equals("center center")) {					
					layoutParams.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
				}else if (val.equals("bottom")||val.equals("bottom center")||val.equals("center bottom")) {
					layoutParams.gravity = Gravity.CENTER | Gravity.BOTTOM;
				}else if (val.equals("left")||val.equals("left center")||val.equals("center left")) {
					layoutParams.gravity = Gravity.CENTER | Gravity.LEFT;
				}else if (val.equals("right")||val.equals("right center")||val.equals("center right")) {
					layoutParams.gravity = Gravity.CENTER | Gravity.RIGHT;
				}else if (val.equals("top")||val.equals("top center")||val.equals("center top")) {
					layoutParams.gravity = Gravity.CENTER | Gravity.TOP;
				 
					
				}else if (val.equals("bottom left")||val.equals("left bottom")) {
					layoutParams.gravity =  Gravity.LEFT | Gravity.BOTTOM;
				}else if (val.equals("bottom right")||val.equals("right bottom")) {
					layoutParams.gravity =  Gravity.RIGHT | Gravity.BOTTOM;	
				}else if (val.equals("top left")||val.equals("left top")) {
					layoutParams.gravity =  Gravity.LEFT | Gravity.TOP;
				}else if (val.equals("top right")||val.equals("right top")) {
					layoutParams.gravity =  Gravity.RIGHT | Gravity.TOP;	
	 
						
					
				}else  {
					layoutParams.gravity = Utility.getInt(val);
				}
				
				
				
			}			
			view.addView(child, layoutParams);
		}
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
	}
	
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
	}

}
