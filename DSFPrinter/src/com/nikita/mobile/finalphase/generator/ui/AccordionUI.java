package com.nikita.mobile.finalphase.generator.ui;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.generator.ui.layout.VerticalLayout;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class AccordionUI extends ComponentGroup{

	public AccordionUI(NikitaControler form) {
		super(form);
	}
	private View view;
	 
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compaccordion);
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		//view.findViewById(R.id.lnraccordion).getLayoutParams().width = view.getContext().getResources().getDisplayMetrics().widthPixels; 
	
		 
		LinearLayout cnt = (LinearLayout) view.findViewById(R.id.lnraccordion);
		for (int i = 0; i < getComponentCount(); i++) {		
			
			Button btn = new Button(form.getActivity());
			btn.setText(getComponent(i).getText());
			cnt.addView(btn,  new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT) );
			
			btn.setTag(i+"");
			btn.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) { 
					try {
						if (getComponent(Utility.getInt( (String)v.getTag() )  ).getView().getVisibility()==View.GONE) {
							getComponent(Utility.getInt( (String)v.getTag() )  ).getView().setVisibility(View.VISIBLE);
						}else{
							getComponent(Utility.getInt( (String)v.getTag() )  ).getView().setVisibility(View.GONE);
							
						}						
					} catch (Exception e) { }
				}
			});
			
			LinearLayout v=new LinearLayout(form.getActivity());
			
			View child = getComponent(i).onCreate(form);	
			child.setVisibility(View.GONE);			 
			v.addView(child, Utility.getMarginLayoutParams(child) );	
			int pad = convertPixel("7dp");
			v.setPadding(pad,pad,pad,pad);
			
			cnt.addView(v, Utility.getMarginLayoutParams(v) );
		}
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		 
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		
		View v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
	}
	
}
