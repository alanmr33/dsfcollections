package com.nikita.mobile.finalphase.generator.ui;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.AdapterView.OnItemSelectedListener;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.Global;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.MultiSelectionSpinner;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class CombolistUI extends Component{

	 
	
	public CombolistUI(NikitaControler form) {
		super(form);
	}
	

	private View view;
	private MultiSelectionSpinner spin;
	private TextView lblText;
	private Vector<String> list = new Vector<String>();
	private Vector<String> code = new Vector<String>();
 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compcombolist);
		spin = (MultiSelectionSpinner) view.findViewById(R.id.combobox);
		lblText = (TextView) view.findViewById(R.id.lblLabel);

		populatData();
		
		brunFirst = false;
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		fillDataOnUI();
		return view;
	}
	 
	public void setData(Nset data) { 
		super.setData(data);
		if (spin!=null && spin.getAdapter() instanceof ArrayAdapter) {
			populatData();
			runOnUI(new Runnable() {
				public void run() {
				 	((ArrayAdapter)spin.getAdapter() ).notifyDataSetChanged();	
				}
			});
		}
	} 
	private void populatData(){
		code.removeAllElements();
		list.removeAllElements();
		//if (Global.getText("generator.ui.dropdown.default").length()>=1) {
			//list.add(0,Global.getText("generator.ui.dropdown.default"));
		//}
		for (int i = 0; i < getData().getArraySize(); i++) {
			if(getData().getData(i).getData("id").toString().length()>=1){
	        	//checkBox.setTag(getData().getData(i).getData("id").toString());
				code.add(getData().getData(i).getData("id").toString());
	        	list.add(getData().getData(i).getData("text").toString());
	        }else if(getData().getData(i).getArraySize()>=2){
	            //checkBox.setTag(getData().getData(i).getData(0).toString());
	        	code.add(getData().getData(i).getData(0).toString());
	        	list.add(getData().getData(i).getData(1).toString());
	        }else if(getData().getData(i).getArraySize()>=1){
	        	// checkBox.setTag(getData().getData(i).getData(0).toString());
	        	code.add(getData().getData(i).getData(0).toString());
	        	list.add(getData().getData(i).getData(0).toString());
	        }else{
	        	//checkBox.setTag(getData().getData(i).toString());
	        	code.add(getData().getData(i).toString());
	        	list.add(getData().getData(i).toString());
	        }  
		}
		
	}
	private void fillDataOnUI(){
		if (view!=null) {			 
			ArrayAdapter<String> adapt = new ArrayAdapter(getNikitaComponent().getActivity(), android.R.layout.simple_list_item_multiple_choice, list);
			//spin.setAdapter(adapt);		
			
			spin.setItems(list);
			setText(super.getText());
			spin.setSelectionString();
			
			spin.setOnItemSelectedListener(new OnItemSelectedListener() {
				public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
					if (brunFirst) {
						getNikitaComponent().runOnUiThread(new Runnable() {
							public void run() {
								CombolistUI.this.runRoute();							 					
							}
						});	
					}
					brunFirst=true;
				}

				public void onNothingSelected(AdapterView<?> arg0) { }
			});
		}	
	} 
	private boolean brunFirst = false;
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	 
	public void setText(String text) {
		if (spin != null && spin.getSelection()!=null) {		
			Nset n = Nset.readJSON(text);
			for (int i = 0; i < spin.getSelection().length; i++) {
				for (int j = 0; j < n.getArraySize(); j++) {
					if (n.getData(j).toString().equals(code.get(i))) {
						spin.setSelection(i, true);
						break;
					}
				}				
			}
		}
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.combobox);
		if (v instanceof Spinner) {
			((Spinner)v).getSelectedItem();
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (spin != null && spin.getSelection()!=null) {
			Nset n = Nset.newArray();
			for (int i = 0; i < spin.getSelection().length; i++) {
				if (spin.getSelection()[i]) {
					n.addData(code.get(i));
				}
			}
			return n.toJSON();
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	private class ItemAdapter extends ArrayAdapter{
		public ItemAdapter(Context context, int textViewResourceId, List objects) {
			super(context, textViewResourceId, objects);
			 
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return super.getView(position, convertView, parent);
		}
	}
	

}
