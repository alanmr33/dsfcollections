package com.nikita.mobile.finalphase.generator.expression;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.nikita.mobile.finalphase.generator.action.EvaluateAction;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class EvaluateExpression implements IExpression {
	@Override
	public boolean OnExpression(Component comp, Nset currdata) {
		Nset data =currdata.getData("args");   
        String code = data.getData("evaluate").toString().trim();  
  
        if (code.contains("==")) {
            String[] comparator = AUtility.split(code, "==");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) == new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains("!=")) {
            String[] comparator = AUtility.split(code, "!=");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) != new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains(">=")) {
            String[] comparator = AUtility.split(code, ">=");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) >= new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains("<=")) {
            String[] comparator = AUtility.split(code, "<=");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) <= new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains(">")) {
            String[] comparator = AUtility.split(code, ">");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) > new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains("<")) {
            String[] comparator = AUtility.split(code, "<");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) < new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }else if (code.contains("=")) {
            String[] comparator = AUtility.split(code, "=");
            if ( new EvaluateAction().evaluate(comparator[0], data, comp.getNikitaComponent()) == new EvaluateAction().evaluate(comparator[1], data, comp.getNikitaComponent()) ) {
                return true;
            }  
        }
		return false;
	}
}
