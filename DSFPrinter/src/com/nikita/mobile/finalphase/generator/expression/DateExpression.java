package com.nikita.mobile.finalphase.generator.expression;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class DateExpression implements IExpression {
	@Override
	public boolean OnExpression(Component comp, Nset currdata) {
		Nset data =currdata.getData("args");        
        
        String code = currdata.getData("code").toString().trim();
        String param1 = comp.getNikitaComponent().getVirtualString(data.get("param1").toString());
        String param2 = comp.getNikitaComponent().getVirtualString(data.get("param2").toString());

        int date1, date2;
        
        if (code.equalsIgnoreCase("date>")) {
            date1 = convert(param1);//converDateToLong(param1);
            date2 = convert(param2);//converDateToLong(param2);
            
            if (date1 > date2) {
                return true;
            }
            
        }else if (code.equalsIgnoreCase("date>=")) {
            date1 = convert(param1);//converDateToLong(param1);
            date2 = convert(param2);//converDateToLong(param1);
            
            if (date1 >= date2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("date<")) {
            date1 = convert(param1);//converDateToLong(param1);
            date2 = convert(param2);//converDateToLong(param1);
            
            if (date1 < date2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("date<=")) {
            date1 = convert(param1);//converDateToLong(param1);
            date2 = convert(param2);//converDateToLong(param1);
            
            if (date1 <= date2) {
                return true;
            }
        }
		return false;
	}
	public static int convert(String date){
        if (date.contains("-")) {
            date = date.replace("-", "");
        }else if (date.contains("/")) {
            date = date.replace("/", "");
        }
        
        return AUtility.getInt(date);
    }
    
    public static long converDateToLong(String date) {

		SimpleDateFormat df = null;

		if (date.length() == 10) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy");
			}
		} else if (date.length() == 16) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			}
		} else if (date.length() == 19) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			}
		} else {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy");
			}
		}

		try {
			Date dt = df.parse(date);
			long hasil = dt.getTime();
			return hasil;
		} catch (Exception e) {
		}

		return 0;
	}
}
