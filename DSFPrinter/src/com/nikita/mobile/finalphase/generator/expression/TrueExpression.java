package com.nikita.mobile.finalphase.generator.expression;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.nikita.mobile.finalphase.generator.NCore;
import com.rkrzmail.nikita.data.Nset;

public class TrueExpression implements IExpression{
	public boolean OnExpression(Component comp, Nset data) {		 
		return true;
	}
}
