package com.nikita.mobile.finalphase.generator.expression;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.rkrzmail.nikita.data.Nset;

public class StringComparationExpression implements IExpression {
	@Override
	public boolean OnExpression(Component comp, Nset currdata) {
		Nset data =currdata.getData("args");
        
        String code = currdata.getData("code").toString().trim();
        if (code.equalsIgnoreCase("equal")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (val1.equals(val2)) {
                return true;
            }
        }else if (code.equalsIgnoreCase("notequal")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (!val1.equals(val2)) {
                return true;
            }
        }else if (code.equalsIgnoreCase("startwith")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (val1.startsWith(val2)) {
                return true;
            }
        }else if (code.equalsIgnoreCase("endwith")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (val1.endsWith(val2)) {
                return true;
            }
        }else if (code.equalsIgnoreCase("contain")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (val1.contains(val2)) {
                return true;
            }
        }else if (code.equalsIgnoreCase("indexof")) {
            
            String key1 = data.getData("param1").toString().trim();
            String val1 = comp.getNikitaComponent().getVirtualString(key1);

            String key2 = data.getData("param2").toString().trim();
            String val2 = comp.getNikitaComponent().getVirtualString(key2);

            if (val1.indexOf(val2) != -1) {
                return true;
            }
        }
        
        
		return false;
	}
}
