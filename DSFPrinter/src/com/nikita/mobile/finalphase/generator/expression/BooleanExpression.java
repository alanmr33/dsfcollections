package com.nikita.mobile.finalphase.generator.expression;

import java.util.Vector;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class BooleanExpression implements IExpression {
	@Override
	public boolean OnExpression(Component comp, Nset currdata) {
		Nset data =currdata.getData("args");
        
        String param = currdata.getData("code").toString();
        
        if (param.equalsIgnoreCase("iftrue")) {
            String p = comp.getNikitaComponent().getVirtualString(data.get("param").toString());            
            
            if ( p.equals("1")||p.toLowerCase().equals("true")  ) {
                return true;
            }
        }else if (param.equalsIgnoreCase("and")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            if ( val1 && val2) {
                return true;
            }
        }else if (param.equalsIgnoreCase("or")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            if ( val1 || val2) {
                return true;
            }
        }else if (param.equalsIgnoreCase("not")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            if ( val1 != val2) {
                return true;
            }
        }else if (param.equalsIgnoreCase("2and")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            String key3 = data.getData("param3").toString();
            boolean val3 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param3").toString()));
            
            String key4 = data.getData("param4").toString();
            boolean val4 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param4").toString()));
            
            if ( (val1 == val2) && (val3 == val4)) {
                return true;
            }
        }else if (param.equalsIgnoreCase("xor")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            String key3 = data.getData("param3").toString();
            boolean val3 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param3").toString()));
            
            String key4 = data.getData("param4").toString();
            boolean val4 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param4").toString()));
            
            if ( (val1 == val2) || (val3 == val4)) {
                return true;
            }
        }else if (param.equalsIgnoreCase("3and")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            String key3 = data.getData("param3").toString();
            boolean val3 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param3").toString()));
            
            String key4 = data.getData("param4").toString();
            boolean val4 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param4").toString()));
            
            String key5 = data.getData("param5").toString();
            boolean val5 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param5").toString()));
            
            String key6 = data.getData("param6").toString();
            boolean val6 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param6").toString()));
            
            if ( (val1 == val2) && (val3 == val4) && (val5 == val6)) {
                return true;
            }
        }else if (param.equalsIgnoreCase("3or")) {
            String key1 = data.getData("param1").toString();
            boolean val1 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param1").toString()));
            
            String key2 = data.getData("param2").toString();
            boolean val2 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param2").toString()));
            
            String key3 = data.getData("param3").toString();
            boolean val3 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param3").toString()));
            
            String key4 = data.getData("param4").toString();
            boolean val4 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param4").toString()));
            
            String key5 = data.getData("param5").toString();
            boolean val5 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param5").toString()));
            
            String key6 = data.getData("param6").toString();
            boolean val6 = Boolean.valueOf(comp.getNikitaComponent().getVirtualString(data.get("param6").toString()));
            
            if ( (val1 || val2) && (val3 || val4) && (val5 || val6)) {
                return true;
            }
        }else if (param.equalsIgnoreCase("first")) {
            if (comp.getNikitaComponent().getLoopCount()>=1) {
                return false;
            }
            return true;
        }else if (param.equalsIgnoreCase("iserror")) {
            String param1 = currdata.getData("args").getData("param1").toString();  
        
            Object obj = comp.getNikitaComponent().getVirtual(param1);
            if (obj instanceof Nikitaset) {
               
                return ((Nikitaset)obj).getError().length()>=1;
            }else if (obj instanceof Nset) {
                
            }
        }else if (param.equalsIgnoreCase("else")) {
            try {
                return !((Boolean)comp.getNikitaComponent().getVirtual("@EXPESSION"));
            } catch (Exception e) { }
            
        }else if (param.equalsIgnoreCase("isnikitasetrows")) {
            String param1 = currdata.getData("args").getData("param1").toString();  
        
            Object obj = comp.getNikitaComponent().getVirtual(param1);
            if (obj instanceof Nikitaset) {
                return ((Nikitaset)obj).getRows()>=1; 
            }
        }else if (param.equalsIgnoreCase("varlen")) {
            String param1 = currdata.getData("args").getData("param1").toString();  
            int l = AUtility.getInt( comp.getNikitaComponent().getVirtualString(currdata.getData("args").get("param2").toString())  );  
            
            Object obj = comp.getNikitaComponent().getVirtual(param1);
            if (obj instanceof Nikitaset) {
                int i = ((Nikitaset)obj).getRows();
                return i < l && i> 0; 
            }else if (obj instanceof Nset) {
                int i = ((Nset)obj).getArraySize();
                if (i!=0) {
                    return i < l && i> 0; 
                }
                i = ((Nset)obj).getObjectKeys().length;
                return i < l && i> 0; 
            }else{
            }
        }else if (param.equalsIgnoreCase("filter")) {
            return comp.getNikitaComponent().getParameter("action").startsWith("page");
        }else if (param.equalsIgnoreCase("iif")) {
            String param1 = comp.getNikitaComponent().getVirtualString(data.getData("param1").toString());
            String parama = comp.getNikitaComponent().getVirtualString(data.getData("parama").toString());
            String param2 = comp.getNikitaComponent().getVirtualString(data.getData("param2").toString());
            String paramb = comp.getNikitaComponent().getVirtualString(data.getData("paramb").toString());
            String param3 = comp.getNikitaComponent().getVirtualString(data.getData("param3").toString());
            String paramc = comp.getNikitaComponent().getVirtualString(data.getData("paramc").toString());
            String param4 = comp.getNikitaComponent().getVirtualString(data.getData("param4").toString());
            String paramd = comp.getNikitaComponent().getVirtualString(data.getData("paramd").toString());
            String param5 = comp.getNikitaComponent().getVirtualString(data.getData("param5").toString());
            String param6 = comp.getNikitaComponent().getVirtualString(data.getData("param6").toString());
             
            boolean b = iif(iif(param1, parama, param2), paramb, iif(param3, paramc, param4));
            return iif(b, paramd, iif(param5, "=", param6));
          
        }else if (param.equalsIgnoreCase("expression")) {
        	
        } 
		return false;
	}
	private boolean iif(boolean b1, String exp, boolean b2){
        if (exp.equals("OR")) {
            return b1 || b2;
        }else if (exp.equals("AND")) {
            return b1 &&  b2;
        }else{
            return b1;
        }
    }
    private boolean iif(String s1, String exp ,String s2 ){
       
        if (exp.equals("<>")) {
            return !(s1.equals(s2)); 
        }else if (exp.equals("=")) {
            return s1.equals(s2); 
        }else if (exp.equals(">=")) { 
            return AUtility.getLong(s1) >= AUtility.getLong(s2) ;
        }else if (exp.equals(">")) {   
            return AUtility.getLong(s1) > AUtility.getLong(s2) ;
        }else if (exp.equals("<=")) {
            return AUtility.getLong(s1) <= AUtility.getLong(s2) ;
        }else if (exp.equals("<")) {
            return AUtility.getLong(s1) < AUtility.getLong(s2) ;
        }else if (exp.equals("equalignorecase")) {    
            return s1.equalsIgnoreCase(s2); 
        }else if (exp.equals("containignorecase")) {
            return s1.toLowerCase().contains(s2.toLowerCase()); 
        }else if (exp.equals("contain")) {
            return s1.contains(s2); 
        }else if (exp.equals("startwithcignorecase")) {
            return s1.toLowerCase().startsWith(s2.toLowerCase()); 
        }else if (exp.equals("startwith")) {
            return s1.startsWith(s2); 
        }else if (exp.equals("endwithignorecase")) {   
            return s1.toLowerCase().endsWith(s2.toLowerCase()); 
        }else if (exp.equals("endwith")) {       
            return s1.endsWith(s2); 
        }else{
            return s1.equals(s2);//default equal
        }
    }
    
    private String correctionformat(String ex){
    	ex=Utility.replace(ex, "(", " ( ");
    	ex=Utility.replace(ex, ")", " ) ");
    	ex=Utility.replace(ex, "  ", " ");
    	ex=Utility.replace(ex, "  ", " ");
    	ex=Utility.replace(ex, "  ", " ");
    	
    	StringBuffer stringBuffer = new StringBuffer();
    	Vector<String> sx = Utility.splitVector(ex, " ");
    	for (int i = 0; i < sx.size(); i++) {
			
		}
    	
    	return ex;
    }
}
