package com.nikita.mobile.finalphase.generator.action;

import java.util.HashMap;
import java.util.Map;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;

public class EvaluateAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
		Nset data =currdata.getData("args");   
        String code = data.getData("evaluate").toString().trim();
   
        
        comp.getNikitaComponent().setVirtual(data.getData("result").toString(), Double.toString(evaluate(code, data, comp.getNikitaComponent())));
        return true;
	}

    public double evaluate (String code, Nset data, NikitaControler nForm ){
        double result = 0;
          
        HashMap map = new HashMap();
        map.put("A", AUtility.getDouble(nForm.getVirtualString(data.get("parama").toString())));
        map.put("B", AUtility.getDouble(nForm.getVirtualString(data.get("paramb").toString())));
        map.put("C", AUtility.getDouble(nForm.getVirtualString(data.get("paramc").toString())));
        map.put("D", AUtility.getDouble(nForm.getVirtualString(data.get("paramd").toString())));
        map.put("E", AUtility.getDouble(nForm.getVirtualString(data.get("parame").toString())));
        map.put("F", AUtility.getDouble(nForm.getVirtualString(data.get("paramf").toString())));
        map.put("G", AUtility.getDouble(nForm.getVirtualString(data.get("paramg").toString())));
        map.put("H", AUtility.getDouble(nForm.getVirtualString(data.get("paramh").toString())));
        
      
        try {
            Calculable cc =  new ExpressionBuilder(code).withVariables(map).build();
            result = cc.calculate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return result;
    }
}
