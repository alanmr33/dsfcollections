package com.nikita.mobile.finalphase.generator.action;

import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class VariableAction implements IAction {
	@Override
	public boolean OnAction(Component comp, Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        
        if (code.equals("new")) {
            Nset n = Nset.newNull(); 
            String var = currdata.getData("args").getData("param1").toString();  
            
            if (var.length()>=1) {
                n=Nset.newArray();
            }    
             
            if (currdata.getData("args").getData("param2").toString().length()>=1) {
            	var = currdata.getData("args").getData("param2").toString();  
                n=Nset.newObject();
            } 
           
            String varn = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()).trim();  
            
    
            if (AUtility.isNumeric(varn)||varn.equals("")) {
                add(n, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param4").toString()));  
            }else{
                set(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param4").toString()));  
            }                
          
            
            comp.getNikitaComponent().setVirtual(var, n);
        }else if (code.equals("get")) {
            String 
            varn = currdata.getData("args").getData("param2").toString();  
            Nset n = new Nset(comp.getNikitaComponent().getVirtual(varn));
            
            String m= currdata.getData("args").getData("param1").toString();
            if (m.equals("getbyname")) {
                varn =  comp.getNikitaComponent().getVirtualString (currdata.getData("args").getData("param3").toString()); 
                
                comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param4").toString(), n.getData( varn ));
                comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param5").toString(), n.getData( varn ).toString() );
                if (currdata.getData("args").getData("param6").toString().length()>=1) {
                	comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param6").toString(), n.getData( varn ).toJSON() );
                }  
                
            }else if (m.equals("getbyindex")) {                
                varn = comp.getNikitaComponent().getVirtualString (currdata.getData("args").getData("param3").toString() );  
                
                comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param4").toString(), n.getData( AUtility.getInt(varn) ));
                comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param5").toString(), n.getData( AUtility.getInt(varn) ).toString() );
                if (currdata.getData("args").getData("param6").toString().length()>=1) {
                	comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param6").toString(), n.getData( AUtility.getInt(varn) ).toJSON() );
                }                
                
            }else if (m.equals("getkeys")) {
                varn = currdata.getData("args").getData("param2").toString();  
                String[] keys = n.getObjectKeys();
                Nset r = Nset.newArray();
                for (int i = 0; i < keys.length; i++) {
                    r.addData(keys[i]);
                }
                comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("param4").toString(), r );
            }
            
        }else if (code.equals("set")) {
            

            String 
            varn = currdata.getData("args").getData("param1").toString(); 
            
            Nset n = new Nset(comp.getNikitaComponent().getVirtual(varn));
   
            
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString())).length()>=1)  {
                    set(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString()));   
                }
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString())).length()>=1)  
                    set(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param5").toString()));   
                
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param6").toString())).length()>=1)  
                    set(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param7").toString()));   
                
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param8").toString())).length()>=1)  
                    set(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param9").toString()));   
            
        }else if (code.equals("replace")) {    
            String 
            varn = currdata.getData("args").getData("param1").toString(); 
            
            Nset n = new Nset(comp.getNikitaComponent().getVirtual(varn));
   
            
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString())).length()>=1)  {
                    replace(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString()));   
                }
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString())).length()>=1)  
                    replace(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param5").toString()));   
                
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param6").toString())).length()>=1)  
                    replace(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param7").toString()));   
                
                if ((varn=comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param8").toString())).length()>=1)  
                    replace(n, varn, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param9").toString()));   
        }else if (code.equals("add")) {
            String 
            varn = currdata.getData("args").getData("param1").toString();  
            Nset n = new Nset(comp.getNikitaComponent().getVirtual(varn));

            if ((varn=currdata.getData("args").getData("param2").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));            
            if ((varn=currdata.getData("args").getData("param3").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));            
            if ((varn=currdata.getData("args").getData("param4").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));            
            if ((varn=currdata.getData("args").getData("param5").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));
            if ((varn=currdata.getData("args").getData("param6").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));
            if ((varn=currdata.getData("args").getData("param7").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));
            if ((varn=currdata.getData("args").getData("param8").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));            
            if ((varn=currdata.getData("args").getData("param9").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));
            if ((varn=currdata.getData("args").getData("param10").toString()).length()>=1)  
                add(n, comp.getNikitaComponent().getVirtual(varn));
            
           
        }
        return true;
	}
	private void replace(Nset n, String  keyindex, Object obj){
        int index = Utility.getInt(keyindex);
        if (index==0 && !keyindex.equals("0")  ) {
            index=-1;            
        }
                
        if (index>=0 && index < n.getArraySize()) {
            try {
                ((Vector) n.getInternalObject()).set(index, obj);
            } catch (Exception e) {}
        }else{
            add(n, obj);
        }
    }
	private void add(Nset n, Object obj){
        if (obj instanceof Nset) {
            n.addData( (Nset)obj );
        }else if (obj instanceof String) {
            n.addData( (String)obj );
        }else if (obj instanceof Hashtable) {
            n.addData( (Hashtable)obj );
        }else if (obj instanceof Vector) {
            n.addData( (Vector)obj );
        }else if (obj instanceof Integer) {  
            n.addData( (Integer)obj );
        }else if (obj instanceof Double) {   
            n.addData( (Double)obj );
        }else if (obj instanceof Boolean) {
            n.addData( (Boolean)obj );
        }else   if (obj !=null ){
            n.addData( (String)obj );
        }else{
            n.addData( Nset.newNull() );
        }
    }
    private void set(Nset n,String key, Object obj){
        if (obj instanceof Nset) {
            n.setData(key, (Nset)obj );
        }else if (obj instanceof String) {
            n.setData(key,(String)obj );
        }else if (obj instanceof Hashtable) {
            n.setData(key, (Hashtable)obj );
        }else if (obj instanceof Vector) {
            n.setData(key, (Vector)obj );
        }else if (obj instanceof Integer) {  
            n.setData(key, (Integer)obj );
        }else if (obj instanceof Double) {   
            n.setData(key, (Double)obj );
        }else if (obj instanceof Boolean) {
            n.setData(key,(Boolean)obj );
        }else if (obj !=null ) {
            n.setData(key, (String)obj );
        }else{
            n.setData(key, Nset.newNull());
        }
    }
}
