package com.nikita.mobile.finalphase.generator.action;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class CurrencyFormatAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
        
		Nset data =currdata.getData("args");        
        String param = comp.getNikitaComponent().getVirtualString(data.get("param").toString());
        String separator = comp.getNikitaComponent().getVirtualString(data.get("separator").toString());
        String decimal = comp.getNikitaComponent().getVirtualString(data.get("decimal").toString());
        StringBuffer sb = new StringBuffer();
        String format = null;
        if (separator.equalsIgnoreCase("IDR")) {
            format = AUtility.formatCurrency(param).replace(",", ".");
        }else{            
            format = AUtility.formatCurrency(param);
        }
        if (decimal.equalsIgnoreCase("None")) {
        }else{            
            if (separator.equalsIgnoreCase("IDR")) {
                format = sb.append(format).append(",00").toString();
            }else{            
                format = sb.append(format).append(".00").toString();
            }
        }
        
        comp.getNikitaComponent().setVirtual(data.get("result").toString(), format);
        
		return true;
	}
	
	public static String FormatCurrency(String curr, double val){
        DecimalFormat format = null;
        String strValue = null;

        format = (DecimalFormat)NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols symbol = format.getDecimalFormatSymbols();
        symbol.setGroupingSeparator('.');
        format.setDecimalFormatSymbols(symbol);

        strValue = format.format(val); // $200.000.00
        
        return strValue;
    }
}
