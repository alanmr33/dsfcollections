package com.nikita.mobile.finalphase.generator.action;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Hashtable;

import org.apache.http.HttpResponse;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.rkrzmail.nikita.data.Nset;

public class HttpConnectionAction implements IAction {
	@Override
	public boolean OnAction(Component comp, Nset data) {
	 
        String code = comp.getNikitaComponent().getVirtualString(data.getData("code").toString());  
        String param1 =comp.getNikitaComponent().getVirtualString( data.getData("args").getData("param1").toString());//getpost  
        String param2 = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param2").toString());//url
        Object param3 = comp.getNikitaComponent().getVirtual(data.getData("args").getData("param3").toString());//nset param
        String param4 = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param5").toString());//string param
        
        String result = "";
        
        if (code.equals("http")) {
            if (param1.equals("POST")) {
                Hashtable hashtable = new Hashtable();
                if (param3 instanceof Nset) {
                    if (((Nset)param3).getInternalObject() instanceof Hashtable) {
                        hashtable = (Hashtable)((Nset)param3).getInternalObject();
                    }
                }else if (param3 instanceof Hashtable) {
                    hashtable = (Hashtable)param3;
                }                    
                HttpResponse httpResponse = NikitaInternet.postHttp(param2+param4, hashtable);

                comp.getNikitaComponent().setVirtual(data.getData("args").getData("param8").toString(), httpResponse.getStatusLine().getStatusCode());
                InputStream is; StringBuffer sb = new StringBuffer();
                try {
                    is = httpResponse.getEntity().getContent();                    
                    byte[] buffer = new byte[1024];int length;
                    while ((length = is.read(buffer)) > 0) {
                        sb.append(new String(buffer, 0, length));
                    }
                } catch (Exception ex) {  }                
                comp.getNikitaComponent().setVirtual(data.getData("args").getData("param9").toString(), sb.toString());
            }else{//get
                Hashtable hashtable = new Hashtable();
                if (param3 instanceof Nset) {
                    if (((Nset)param3).getInternalObject() instanceof Hashtable) {
                        hashtable = (Hashtable)((Nset)param3).getInternalObject();
                    }
                }else if (param3 instanceof Hashtable) {
                    hashtable = (Hashtable)param3;
                }  
                Nset p = new Nset(hashtable);
                String[] keys = p.getObjectKeys();
                StringBuffer stringBuffer = new StringBuffer();
                for (int i = 0; i < keys.length; i++) {
                    stringBuffer.append("&");
                    stringBuffer.append(keys[i]);
                    stringBuffer.append("=");
                    stringBuffer.append(URLEncoder.encode(  p.getData(keys[i]).toString()  ));
                }
                 
                        
                HttpResponse httpResponse = NikitaInternet.getHttp(param2+(param2.contains("?")?"":"?")+stringBuffer.toString()+param4);
                comp.getNikitaComponent().setVirtual(data.getData("args").getData("param8").toString(), httpResponse.getStatusLine().getStatusCode());
                InputStream is; StringBuffer sb = new StringBuffer();
                try {
                    is = httpResponse.getEntity().getContent();                    
                    byte[] buffer = new byte[1024];int length;
                    while ((length = is.read(buffer)) > 0) {
                        sb.append(new String(buffer, 0, length));
                    }
                } catch (Exception ex) {  }                
                comp.getNikitaComponent().setVirtual(data.getData("args").getData("param9").toString(), sb.toString());
            }
        }else if (code.equals("multipart")) {
            Hashtable hashtable = new Hashtable();
            if (param3 instanceof Nset) {
                if (((Nset)param3).getInternalObject() instanceof Hashtable) {
                    hashtable = (Hashtable)((Nset)param3).getInternalObject();
                }
            }else if (param3 instanceof Hashtable) {
                hashtable = (Hashtable)param3;
            }             
                
            HttpResponse httpResponse = NikitaInternet.multipartHttp(param1+param2, hashtable, null, null);
            comp.getNikitaComponent().setVirtual(data.getData("args").getData("param8").toString(), httpResponse.getStatusLine().getStatusCode());
            InputStream is; StringBuffer sb = new StringBuffer();
                try {
                    is = httpResponse.getEntity().getContent();                    
                    byte[] buffer = new byte[1024];int length;
                    while ((length = is.read(buffer)) > 0) {
                        sb.append(new String(buffer, 0, length));
                    }
                } catch (Exception ex) {  }                
                comp.getNikitaComponent().setVirtual(data.getData("args").getData("param9").toString(), sb.toString());
        }else{
            comp.getNikitaComponent().setVirtual(data.getData("args").getData("param8").toString(), "");
            comp.getNikitaComponent().setVirtual(data.getData("args").getData("param9").toString(), "");
        }              
        return true;
    }
}
