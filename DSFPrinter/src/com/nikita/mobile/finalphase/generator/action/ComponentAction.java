package com.nikita.mobile.finalphase.generator.action;

import java.util.Vector;

import android.util.Log;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.NikitaEngine;
import com.nikita.mobile.finalphase.generator.ui.SmartGridUI;
import com.nikita.mobile.finalphase.generator.ui.TableGridUI;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class ComponentAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());           
        String compz = currdata.getData("args").getData("param1").toString();
        
        
        if (code.equals("setvisibleall")||code.equals("setenableall")||code.equals("settextall")) {
        	setComponent(comp, code, comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()) ,  currdata, comp.getNikitaComponent());
               
        	return true;
        }
        
        if (comp.getNikitaComponent().isVirtual(compz)) {
        	String data = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()); 
            if (code.equals("settext")) {
            	comp.getNikitaComponent().setVirtual(compz, data);
            }if (code.equals("setdata")) {
            	comp.getNikitaComponent().setVirtual(compz, comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString()) );
            }            
        }else if (code.equals("component")) {
           
            Component component  = comp.getNikitaComponent().getComponent(currdata.getData("args").getData("param2").toString());
            setComponent(component, comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()) ,(currdata.getData("args").getData("param3").toString()), currdata, comp.getNikitaComponent());
                
        }else if (comp.getNikitaComponent().isComponent(compz)){
            Component component  = comp.getNikitaComponent().getComponent(currdata.getData("args").getData("param1").toString());
            setComponent(component, code, (currdata.getData("args").getData("param2").toString() ), currdata, comp.getNikitaComponent());
  
        }
		return true;
	}
	private void setComponent(Component component, String code, String data, Nset currdata, final NikitaControler nf){
		 
        if (code.equals("settext")) {
            component.setText(nf.getVirtualString(data));
            
            nf.refreshComponent(component);
        }else  if (code.equals("settextall")) {
        	data = nf.getVirtualString(data);
        	Vector<Component> components = nf.getContent().populateAllComponents();
            for (int i = 0; i < components.size(); i++) {
                component  =  components.elementAt(i);
                component.setText(nf.getVirtualString(data));
                
                nf.refreshComponent(component);
            }  
        }else  if (code.equals("setvisibleall")) {
        	data = nf.getVirtualString(data);
        	Vector<Component> components = nf.getContent().populateAllComponents();
            for (int i = 0; i < components.size(); i++) {
                component  =  components.elementAt(i);
                component.setVisible(data.equalsIgnoreCase("true")||data.equals("1")?true:false);
                
                nf.refreshComponent(component);
            }  
        
        }else  if (code.equals("setvisible")) {
            data = nf.getVirtualString(data);
            component.setVisible(data.equalsIgnoreCase("true")||data.equals("1")?true:false);
            
            nf.refreshComponent(component);
        }else  if (code.equals("setenableall")) {
        	data = nf.getVirtualString(data);
        	Vector<Component> components = nf.getContent().populateAllComponents();
            for (int i = 0; i < components.size(); i++) {
                component  =  components.elementAt(i);
                component.setEnable(data.equalsIgnoreCase("true")||data.equals("1")?true:false);
                
                nf.refreshComponent(component);
            }  
        }else if (code.equals("setenable")) {
            data = nf.getVirtualString(data);
            component.setEnable(data.equalsIgnoreCase("true")||data.equals("1")?true:false);
            
            nf.refreshComponent(component);
        }else if (code.equals("settag")) {
            Object obj = nf.getVirtual(data);
            if (obj instanceof Nikitaset) {
                component.setTag(((Nikitaset)obj).toNset().toJSON());  
            }else if (obj instanceof Nset) {
                component.setTag(((Nset)obj).toJSON());   
            }else  if (obj != null) {
                component.setTag( nf.getVirtualString(data));
            }else{
                component.setTag("");
            }     
            
            nf.refreshComponent(component);
        }else if (code.equals("setmandatary")) { 
        	data = nf.getVirtualString(data);
            component.setMandatory(data.equalsIgnoreCase("true")||data.equals("1")?true:false);
        }else if (code.equals("resultgrid")) {
        	if (component instanceof SmartGridUI) {  
        		SmartGridUI grid = ((SmartGridUI)component);
        		/*
                Nset smartarg = Nset.readJSON(   response.getVirtualString(currdata.getData("args").getData("param9").toString())  );  
                                 
                grid.setCurrentAction(smartarg.getData("action").toString());             
                if (smartarg.getData("showdetailview").toString().equals("false")) {
                    grid.setCurrentDetail("");//hide
                }
                */
            }
        }else if (code.equals("smartgrid")) {	
        	Object  vData = nf.getVirtual (currdata.getData("args").getData("param2").toString());
            Nset smartarg = Nset.readJSON(   nf.getVirtualString(currdata.getData("args").getData("param9").toString())  );  
            
            if (vData instanceof Nikitaset && component instanceof SmartGridUI) {
                ((SmartGridUI)component).setData((Nikitaset)vData);
            }else if (vData instanceof Nset) {
                component.setData((Nset)vData);
            }else{
                 
            }
            if (component instanceof SmartGridUI) {     
            	SmartGridUI grid = ((SmartGridUI)component);
            	/*
                grid.showRowNumber(smartarg.getData("showrownum").toString().equals("true"));
                grid.showAction(smartarg.getData("showaction").toString().equals("true"), smartarg.getData("action").toString());
                grid.showRowCheckbox(smartarg.getData("multiselect").toString().equals("true"), smartarg.getData("multiselectfield").toString());
                grid.sortableGrid(smartarg.getData("sortable").toString().equals("true"));
                
                grid.showRowMaster(!smartarg.getData("showrowmaster").toString().equals("false"));
                
                grid.setDefaultHeader(smartarg.getData("header").toString());
                grid.setDefaultDataView(smartarg.getData("data").toString());
                grid.setDefaultDetailView(smartarg.getData("detail").toString());
                grid.setHideHeaderEmpty(true);
                */
                
                final Component comp = nf.getComponent(smartarg.getData("looper").toString());
               
                if (!comp.getId().equals("")) {
                     
                    grid.setSmartGridItemViewListener(new SmartGridUI.SmartGridItemViewListener() {
                        public Component[] onItemView(SmartGridUI parent, int row, Object data) {
                            nf.setVirtualRegistered("@+GRIDROW", row);
                            nf.setVirtualRegistered("@+GRIDCOUNT", row);
                            nf.setVirtualRegistered("@+GRIDDATA", data);
                            if (data instanceof Nikitaset) {
                            	nf.setVirtualRegistered("@+GRIDDATANSET", ((Nikitaset)data).toNset() );
                            }else if (data instanceof Nset) {                             
                            	nf.setVirtualRegistered("@+GRIDDATANSET", (Nset)data);
                            }else{
                            	nf.setVirtualRegistered("@+GRIDDATANSET", Nset.newObject());
                            }
                                    
                            
                             
                            
                            //NikitaServlet.execLogicComponent(response.getContent(), request, response, response.getNikitaLogic(), comp.getId());
                            Component[] components= new Component[0];
                            
                             
                            
               
                            return components;
                        }
                    });
                }    
                if (!comp.getId().equals("")||! grid.getDefaultDetailView().equals("")) {    
                    grid.setSmartGridDetailViewListener(new SmartGridUI.SmartGridDetailViewListener() {
                        public NikitaForm onDetailView(SmartGridUI parent, int row, Object data) {                           
                            //String comp = parent.getCurrentDetail()!=null?(parent.getCurrentDetail().equals("")?parent.getDefaultDetailView():parent.getCurrentDetail()):parent.getDefaultDetailView();
                            String comp =  parent.getCurrentDetail()!=null?parent.getCurrentDetail():"";
                            if (comp.equals("")) {
                                return null; //hide
                            }
                            
                             
                                                        
                            
                            nf.setVirtualRegistered("@+GRIDROW", row);
                            nf.setVirtualRegistered("@+GRIDCOUNT", row);
                            nf.setVirtualRegistered("@+GRIDDATA", data);
                            if (data instanceof Nikitaset) {
                            	nf.setVirtualRegistered("@+GRIDDATANSET", ((Nikitaset)data).toNset() );
                            }else if (data instanceof Nset) {                             
                            	nf.setVirtualRegistered("@+GRIDDATANSET", (Nset)data);
                            }else{
                            	nf.setVirtualRegistered("@+GRIDDATANSET", Nset.newObject());
                            }
                            
                          
                            Nset n = Nset.newObject();
                            if (comp.startsWith("{")) {
                                n = Nset.readJSON(comp);
                                comp=n.getData("formid").toString();
                                if (!n.getData("formname").toString().equals("")) {
                                    comp=n.getData("formname").toString();
                                }


                                String[] keys = n.getData("args").getObjectKeys();               
                                for (int i = 0; i < keys.length; i++) {
                                    String vdata = n.getData("args").getData(keys[i]).toString();
                                    if (keys[i].startsWith("[")&& keys[i].endsWith("]")) {
                                        //Difinition Result                     
                                        nf.updateParameter(keys[i].substring(1, keys[i].length()-1), vdata );   
                                    }else{

                                    	nf.updateParameter(keys[i], nf.getVirtualString(  vdata ) );   
                                    }                                                 
                                }
                            } else if (comp.equals("")) {
                                return null;
                            }
                         
                            
                            if (nf.getVirtualString("@+NIKITAGENERATORSMARTGRIDMASTERDETAIL").equals(comp) && (nf.getVirtual("@+NIKITAGENERATORSMARTGRIDMASTERDETAILENGINE") instanceof NikitaEngine) ) {
                            	//nf.runServletGen( comp , nq, nr , nf.getNikitaLogic(), (NikitaEngine)response.getVirtual("@+NIKITAGENERATORSMARTGRIDMASTERDETAILENGINE") );
                            }else{
                            	//nf.runServletGen( comp , nq, nr , response.getNikitaLogic());
                                nf.setVirtualRegistered("@+NIKITAGENERATORSMARTGRIDMASTERDETAIL", comp);
                                nf.setVirtualRegistered("@+NIKITAGENERATORSMARTGRIDMASTERDETAILENGINE", nf.getNikitaEngine());
                            }
                            
                             /*
                            nf.getContent().setInstanceId( parent.getJsId()+ "-" + row );
                            nf.getContent().setFormCaller( parent.getFormJsId() );//20160219 sebelumnya salah
                            nf.getContent().setInitLoad(nf.getInitLoadAndClear());//addd for init
                            
                            parent.setCurrentDetail("");//reset
                            if (nf.getVirtualString("@SMARTGRIDDETAILVIEWHIDE").equals("true")) {
                               return null; //hide
                            }
                            */
                            return nf.getContent();
                        }
                    });
                }
            }
            nf.refreshComponent(component);            
        }else if (code.equals("setdata")) {
        	if (component instanceof TableGridUI) {
                if (nf.getVirtual(data) instanceof Nikitaset) {
                    ((TableGridUI)component).setData( (Nikitaset)nf.getVirtual(data)  );
                    
                }else{
                    component.setData(new Nset(nf.getVirtual(data)));
                }
                //header
                if (currdata.getData("args").getData("param3").toString().trim().length()>=1) {         
                    Nset  n  = new Nset(nf.getVirtual(currdata.getData("args").getData("param3").toString()));
                    if (nf.getVirtual(currdata.getData("args").getData("param3").toString()) instanceof  String) {
                        String s = nf.getVirtualString(currdata.getData("args").getData("param3").toString());
                        if((s.startsWith("[")|| s.startsWith("{") ) && !(s.startsWith("[*")|| s.startsWith("[#")|| s.startsWith("[v"))){
                            n = Nset.readJSON(s);
                        } else if(s.contains("|")){
                            n = Nset.readsplitString(s,"|");
                        } else if(s.contains(",")){
                            n = Nset.readsplitString(s,",");
                        }
                    }
                    ((TableGridUI)component).setDataHeader(n);
                     
                    
                 
                    if ( n.getData(0).toString().startsWith("[#]") ) {
                        ((TableGridUI)component).showRowIndex(true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }else if ( n.getData(0).toString().startsWith("[*]") ) {
                        ((TableGridUI)component).showRowIndex(true,true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }else if ( n.getData(0).toString().startsWith("[v]") ) {
                        ((TableGridUI)component).showRowIndex(false,true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }
                    
                    for (int i = 0; i < n.getArraySize(); i++) {
                       ((TableGridUI)component).setColHide(i, n.getData(i).toString().equals(""));
                        if (n.getData(i).toString().contains("[*]")) {
                            ((TableGridUI)component).setRowCounter(true);                       
                        }else if (n.getData(i).toString().equals("[v]")) {
                            ((TableGridUI)component).setColType(i, TableGridUI.TYPE_CHECKBOK);                        
                        }else if (n.getData(i).toString().startsWith("[")&&n.getData(i).toString().endsWith("]") ) {
                            //((Tablegrid)component).setColType(i, Tablegrid.TYPE_CHECKBOK);
                        }
                    }
                    
                }               
                
            }else{                
                if (nf.getVirtual(data) instanceof Nikitaset) {
                    component.setData(new Nset( ((Nikitaset)nf.getVirtual(data)).getDataAllVector() ));
                }else{
                    component.setData(new Nset(nf.getVirtual(data)));
                }            
                
            }
            
            nf.refreshComponent(component);
            
        }else if (code.equals("gettext")) {    
            nf.setVirtual(data, component.getText());
        } else if (code.equals("gettag")) {      
          
            nf.setVirtual(data, component.getTag());    
        } else if (code.equals("getenable")) {            
            nf.setVirtual(data, component.isEnable()?"1":"0");
        } else if (code.equals("getvisible")) {            
            nf.setVirtual(data, component.isVisible()?"1":"0");    
        } else if (code.equals("getmandatory")) {            
            nf.setVirtual(data, component.isVisible()?"1":"0");        
        }  
	}
}
