package com.nikita.mobile.finalphase.generator.action;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class JsonAction implements IAction {
	@Override
	public boolean OnAction(Component comp, Nset currdata) {
		String data = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
        
        
        if (data.equals("read")) {
            String json = comp.getNikitaComponent().getVirtualString(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();
            
            comp.getNikitaComponent().setVirtual(compz, Nset.readJSON(json));
        }else if (data.equals("nikitaset")) {
            
            String json = comp.getNikitaComponent().getVirtualString(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();
            
            Nset n = Nset.readJSON(json);
            if (Nikitaset.isNikitaset(n)) {
            	comp.getNikitaComponent().setVirtual(compz, new Nikitaset(n));
            }else{
            	comp.getNikitaComponent().setVirtual(compz, n);
            } 
        }else if (data.equals("write")){

            Object json = comp.getNikitaComponent().getVirtual(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();   
             
            if (json instanceof Nikitaset) {
            	comp.getNikitaComponent().setVirtual(compz, ((Nikitaset)json).toNset().toJSON() ); 
            }else{
            	comp.getNikitaComponent().setVirtual(compz, new Nset(json).toJSON() ); 
            }

        }else if (data.equals("split")){
             String json = comp.getNikitaComponent().getVirtualString(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();
            
            comp.getNikitaComponent().setVirtual(compz, Nset.readsplitString(json));
        }else if (data.equals("comma")){
             String json = comp.getNikitaComponent().getVirtualString(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();
            
            comp.getNikitaComponent().setVirtual(compz, Nset.readsplitString(json, ","));
        }else if (data.equals("semicolon")){
             String json = comp.getNikitaComponent().getVirtualString(  currdata.getData("args").getData("param2").toString()  );
            String compz = currdata.getData("args").getData("param3").toString();
            
            comp.getNikitaComponent().setVirtual(compz, Nset.readsplitString(json, ";"));
        }
        return true;
	}
}
