package com.nikita.mobile.finalphase.generator.action;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.rkrzmail.nikita.data.Nset;

public class ShowDialogAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
        comp.runOnUI(new Runnable() {
			public void run() {
				String title = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("title").toString());
		        String message = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("message").toString());
		        String button1 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("button1").toString());
		        String button2 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("button2").toString());
		        String button3 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("button3").toString());
		        String reqcode = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("reqcode1").toString());
		        
		        if (reqcode.equals("") && currdata.getData("args").getData("data").toString().equals("")) {
		        	comp.getNikitaComponent().showDialog(title, message, button1, button2, button3, reqcode , Nset.newObject());
		        }else{
		            comp.getNikitaComponent().showDialog(title, message, button1, button2, button3, reqcode , new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("data").toString())));
		        } 
			}
		});
		return true;
	}
}
