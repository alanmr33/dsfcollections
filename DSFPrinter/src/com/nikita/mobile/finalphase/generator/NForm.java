package com.nikita.mobile.finalphase.generator;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Random;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.activity.OrderCurrentActivity;
import com.nikita.mobile.finalphase.activity.SettingActivity;
import com.nikita.mobile.finalphase.activity.SplashScreen;
import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.component.ComponentManager;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.generator.action.StringAction;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.Base64Coder;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NForm {
//	private Thread currProcess;
//	private Handler currhandler;
//	public void setWorkerThread(Thread work){
//		this.currProcess=work;
//	}	
//	public Thread getWorkerThread(){
//		if (currProcess==null) {
//			currProcess=new Thread("nikita"){
//				public void run() {
//					try {
//						Looper.prepare();
//						currhandler = new Handler();					
//						Looper.loop();
//					} catch (Exception e) { }
//				}
//				
//			};
//		}
//		currProcess.start();
//		return currProcess;
//	}
//	
//	public void runOnWorker(Runnable run){
//		if (currhandler!=null) {
//			currhandler.post(run);
//		}
//	}
//	
//	public void closeNikitaForm(){
//		currProcess.interrupt();
//		currProcess=null;
//		
//		
//	}
//	
//	
//	
//	
//	
//	
//	
//	public void refreshComponent(Component component){ 
//		/*
//		if (component.getView()!=null) {
//			runOnUI(new Runnable() {
//				private Component component;
//				public Runnable get(Component component){
//					this.component=component;
//					return this;
//				}
//				public void run() {
//					if (component.getView()!=null) {
//						component.getView().invalidate();
//					}
//				}
//			}.get(component));
//		}		
//		*/
//	}
//	public void refreshComponent(String s){
//		refreshComponent( getComponent(s) );
//	}
//	
//	
//	
//	public static void startForm(final String fname, final Activity activity){
//		NForm.runOnNewThread(new Runnable() {
//			public void run() {
//				NForm form = NCore.getInstanceForm(fname, "");				
//				form.showForm(activity);
//			}
//		});
//	}
// 
//	public static void startFormWithBusy(final String fname, final Activity activity){
//		
//		NForm form = NCore.getInstanceForm("","");
//		form.busy=true;
//		form.showForm(activity);
//		 
//		NForm.runOnNewThread(new Runnable() {
//			private NForm form;
//			public void run() {
//				form.initialize (fname);	
//				if (form.getActivity()!=null && !form.getActivity().isFinishing()) {
//					form.getActivity().runOnUiThread(new Runnable() {
//						public void run() {
//							if (!form.isCreated()) {
//								form.onCreate(null);
//							}						
//							form.showBusy(false);					
//						}
//					});
//				}else{
//					
//				}
//			}
//			public Runnable get(NForm form){
//				this.form=form;
//				return this;
//			}
//		}.get(form));		
//	}
//	
//	public NFormActivity getActivityNF(){
//		return (NFormActivity)getActivity();
//	}
//	public Activity getActivity(){
//		if (fListener!=null) {
//			return fListener.getActivity();
//		}
//		/*
//		if (NCore.getCurrentActivity()!=null) {
//			String own = NCore.getCurrentActivity().getOwnerForm();
//		}		
//		*/ 
//		return NCore.getCurrentActivity();
//	}
//	
//	public interface NFormActivityListener {
//		public NFormActivity getActivity();
//	}
//	private NFormActivityListener fListener;
//	public void setActivityListener(NFormActivityListener formActivityListener){
//		fListener=formActivityListener;
//	}
//	
//	public void showForm(NForm caller){
//		this.caller=caller;
//		
//		showForm();
//	}
//	
//	public void showForm(){		 
//		NFormActivity currActivity  = NCore.getCurrentActivity();		
//		showForm(currActivity);		
//		
//		if (currActivity instanceof NFormActivity) {
//			currActivity.onSaveInstanceState(null);
//			
//		}		
//		currActivity.closeActivity();
//	}
//	public void showForm(Activity caller){
//		NCore.setCurrentForm(this);
//		lNikitaGeneratorNfid=System.currentTimeMillis();
//			
//		
//		Intent intent = new Intent(caller, NFormActivity.class);	
//		intent.putExtra(NFormActivity.NIKITA_GENERATOR_NAME, getInstanceName());
//		intent.putExtra(NFormActivity.NIKITA_GENERATOR_NFID, lNikitaGeneratorNfid);
//	 	caller.startActivityForResult(intent, 1);	 	
//	}
//	
//	public void showFormFinder(){		 
//		NFormActivity currActivity  = NCore.getCurrentActivity();		
//		showFormFinder(currActivity);			
//		//currActivity.closeActivity();//*1234**********************
//	}
//	public void showFormFinder(NForm caller){
//		this.caller=caller;
//		
//		showFormFinder();
//	}
//	public void showFormFinder(Activity caller){
//		NCore.setCurrentForm(this);
//		lNikitaGeneratorNfid=System.currentTimeMillis();
//		
//		Intent intent = new Intent(caller, NFormFinder.class);	
//		intent.putExtra(NFormActivity.NIKITA_GENERATOR_NAME, getInstanceName());
//		intent.putExtra(NFormActivity.NIKITA_GENERATOR_NFID, lNikitaGeneratorNfid);
//	 	caller.startActivity(intent);	 	
//	}
//	private long lNikitaGeneratorNfid = 0;
//	public long getNikitaGeneratorNfid(){
//		return lNikitaGeneratorNfid;
//	}
//	public void closeForm(){
//		closeForm(true);
//	}
//	public void closeForm(boolean showcaller){
//		NCore.out("OUT", "closeForm"+showcaller);
//		//close form n back to caller	
//		//NFormActivity currActivity = NCore.getCurrentActivity();
//		NCore.removeForm(getName(), getInstance());
//		
//		NFormActivity currActivity =(NFormActivity) getActivity();
//		
//		if (currActivity!=null) {			
//			if (caller!=null) {	
//				if (currActivity instanceof NFormFinder) {
//					try {
//						//caller.getActivity().closeActivity();//*1234**********************
//						
//						currActivity.closeActivity();
//					} catch (Exception e) {}
//					//caller.showForm();
//				}else{
//					currActivity.closeActivity();//
//					if (showcaller) {
//						caller.showForm();
//					}					
//				}
//			}else if (getCallerName().length()>=1) {
//				//NForm form = NCore.getInstanceForm(getCallerName(), getCallerInstance());	 
//				//form.showForm();
//				
//				currActivity.closeActivity();
//			}else{
//				currActivity.closeActivity();
//			}			
//		}
//			
//	}
//	
//	 
//	
//	private String callername = "";
//	private String callerinstance = "";
//	
//	
//	private String getCallerName(){
//		return callername;
//	}
//	private String getCallerInstance(){
//		return callerinstance;
//	}	
//
//	public void saveFileComponent() {
//		if (getContent()!=null) {	
//			Vector<Component> all = getContent().populateAllComponents();
//			for (int i = 0; i < all.size(); i++) {
//				if (!all.elementAt(i).getName().equals("")) {					 
//					if (all.elementAt(i).isFileComponent()) {
//						all.elementAt(i).saveFileComponent();
//					}				 
//				}			
//			}
//		}
//	}
//	
//	public Nset getMobileActivityStream(){
//		//{"compname":["text","visible[0,1]","enable[0,1]","[file,]"]}
//		Nset nset = Nset.newObject();	
//		
//		if (getContent()!=null) {	
//			Vector<Component> all = getContent().populateAllComponents();
//			for (int i = 0; i < all.size(); i++) {
//				if (!all.elementAt(i).getName().equals("")) {
//					Nset compset = Nset.newArray();
//					compset.addData(all.elementAt(i).getText());
//					compset.addData(all.elementAt(i).isVisible()?"1":"0");
//					compset.addData(all.elementAt(i).isEnable() ?"1":"0");
//					if (all.elementAt(i).isFileComponent()) {
//						compset.addData("file");//array[3]
//					}
//					nset.setData(all.elementAt(i).getName(), compset);
//				}			
//			}
//		}
//		nset.addData(Nset.newObject().setData(Utility.MD5("init").toLowerCase(Locale.ENGLISH), Nset.newObject().setData("caller", getCallerName()).setData("callerinstance", getCallerInstance()) ));
//		return nset;//{components}
//	}
//	
//	
//	public String getInstance(){
//		return instance;
//	}
//	private String instance ="";
//	public String getInstanceName(){
//		return getName()+"-"+getInstance();
//	}
//	public void setInstance(String instance){
//		 this.instance=instance;
//	}
//	private String fname ="";
//	
//	public void setArgument(Nset param){
//		 this.param=param;
//	}
//	
//	public NForm(String fname){
//		this.fname=fname;
//		if (!fname.equals("")) {
//			initialize (fname);
//		}		
//	}
//	
//	public NForm(String fname, Nset param){
//		this.fname=fname;
//		this.param=param;
//		
//		if (!fname.equals("")) {
//			initialize (fname);
//		}	
//	}
//	
//	public String getName(){
//		return fname;
//	}
//	
//	private boolean bload = false;
//	private boolean binit = false;
//	public boolean isInitialized(){
//		return binit;
//	}
//	public NForm(){}	
//	private boolean bfirst = false;
//	public boolean isCreated(){
//		return bfirst;
//	}
//	
//	private Vector<Component> navLoad  = new Vector<Component>();
//	private Vector<Component> navBack  = new Vector<Component>();
//	private Vector<Component> navResult  = new Vector<Component>();
//    
//	public void callTask(String fname, Nset param){
//		NikitaConnection nc = NCore.getConnection(NikitaConnection.LOGIC);
//		 
//		boolean isID = Utility.isNumeric(fname);
//	    Nikitaset logicform = nc.QueryPage(1, 1, "SELECT formid,formname,formtitle,formtype,formstyle FROM web_form WHERE "+(isID?"formid":"formname")+" = ?",fname);
//	    
//	    Nikitaset nikitaset = nc.Query("SELECT * FROM web_component WHERE formid = ? ORDER BY compindex ASC;", logicform.getText(0, 0) );
//        
//        if (param!=null) {
//        	Nset n = param;
//            String[] key = n.getObjectKeys();
//            for (int i = 0; i < key.length; i++) {
//                 setVirtual("@"+key[i], n.getData(key[i]).toString());
//            }
//            
//		}
//        
//        Component component = ComponentManager.createComponent(NForm.this, nikitaset, 0);  
//        component.runRoute();        
//	}
//	
//	private void initialize(String fname){
//		binit=true;
//		NikitaConnection nc = NCore.getConnection(NikitaConnection.LOGIC);
//		 
//		boolean isID = Utility.isNumeric(fname);
//	    Nikitaset logicform = nc.QueryPage(1, 1, "SELECT formid,formname,formtitle,formtype,formstyle FROM web_form WHERE "+(isID?"formid":"formname")+" = ?",fname);
//	    //nikitaLogic.setForm(nikitaset.getText(0, 0), nikitaset.getText(0, 1), nikitaset.getText(0, 2), nikitaset.getText(0, 3), nikitaset.getText(0, 4));
// 		
//	    //Nikitaset nikitaset = nc.Query("SELECT * FROM web_component WHERE formid = ? ORDER BY compindex ASC;", logicform.getText(0, 0) );
//	    Nikitaset nikitaset = nc.Query("SELECT comp.*,MAX(logic.compid) AS countid, COUNT(*) AS count FROM web_component comp LEFT JOIN web_route logic ON (comp.compid=logic.compid) WHERE comp.formid = ?  GROUP BY comp.compid ORDER BY comp.compindex ASC ;", logicform.getText(0, 0) );
//	   
//	    
//	    NikitaForm nf = new NikitaForm(NForm.this);
//        nf.setText(logicform.getText(0, "formtitle"));  
//        
//        //mobile only
//        nf.setId(logicform.getText(0, "formid"));
//        nf.setName(logicform.getText(0, "formname"));
//        nf.setType(logicform.getText(0, "formtype"));
//        
//        
//        if (logicform.getText(0, "formstyle").contains(":")||logicform.getText(0, "formstyle").contains("=")) {
//            Style style = Style.createStyle(logicform.getText(0, "formstyle")) ;              
//            nf.setStyle(style);
//        }     
//        
//        Hashtable<String, Component> hashtable = new Hashtable<String, Component>();
//        Vector<Component> components = new Vector<Component>(); 
//        for (int i = 0; i < nikitaset.getRows(); i++) {
//
//            Component comp = ComponentManager.createComponent(NForm.this, nikitaset, i);       
//            components.addElement(comp);
//            hashtable.put(comp.getName(), comp);
//            //if (nc.QueryPage(1, 1, "SELECT routeindex from web_route WHERE compid = ? ;", comp.getId()).getRows()>=1 ) {
//                
//            //}
//            
//            //fill default
//            if (nikitaset.getText(i, "compdefault").startsWith("@")||nikitaset.getText(i, "compdefault").startsWith("&")||nikitaset.getText(i, "compdefault").startsWith("$")) {
//                comp.setText(getParameter(nikitaset.getText(i, "compdefault").substring(1)));
//            }  
//            if (nikitaset.getText(i, "comptext").startsWith("@")||nikitaset.getText(i, "comptext").startsWith("&")||nikitaset.getText(i, "comptext").startsWith("$")) {
//                comp.setText(getParameter(nikitaset.getText(i, "comptext").substring(1)));
//            } 
//            
//            //fill data
//            if (nikitaset.getText(i, "complist").startsWith("@")||nikitaset.getText(i, "complist").startsWith("&")||nikitaset.getText(i, "complist").startsWith("$")) {
//                comp.setData(Nset.readJSON(getParameter(nikitaset.getText(i, "complist").substring(1))));
//            } else if(nikitaset.getText(i, "complist").startsWith("[")|| nikitaset.getText(i, "complist").startsWith("{")){
//                comp.setData(Nset.readJSON(nikitaset.getText(i, "complist")));
//            } else if(nikitaset.getText(i, "complist").contains("|")){
//                comp.setData(Nset.readsplitString(nikitaset.getText(i, "complist"),"|"));
//            } else if(nikitaset.getText(i, "complist").contains(",")){
//                comp.setData(Nset.readsplitString(nikitaset.getText(i, "complist"),","));
//            }
//            
//            //fill style
//            if (nikitaset.getText(i, "compstyle").contains(":")||nikitaset.getText(i, "compstyle").contains("=")) {
//                Style style = Style.createStyle(nikitaset.getText(i, "compstyle")) ;    
//                comp.setStyle(style);
//            }
//            
//            if (nikitaset.getText(i, "comptype").equals("navload")) {
//                navLoad.addElement(comp);
//            }else if (nikitaset.getText(i, "comptype").equals("navback")) {
//                navBack.addElement(comp);
//            }else if (nikitaset.getText(i, "comptype").equals("navresult")) {   
//                navResult.addElement(comp);
//            }
//            
//        }  
//        
//        //flat to tree[1]
//        for (int i = 0; i < components.size(); i++) {
//            String parent =components.elementAt(i).getParentName().trim();
//            parent=parent.startsWith("$") ? parent.substring(1):parent;
//            
//            if (parent.equals(components.elementAt(i).getName())) {
//                 components.elementAt(i).clearParentName();
//            }else if (!parent.equals("")) {
//                if ( hashtable.get(parent) instanceof ComponentGroup) {
//                    ((ComponentGroup)hashtable.get(parent)).addComponent(components.elementAt(i));
//                }else{
//                    components.elementAt(i).clearParentName();
//                }
//            }else{
//                components.elementAt(i).clearParentName();
//            }
//        }            
//        //flat to tree[2]
//        for (int i = 0; i < components.size(); i++) {
//            if (components.elementAt(i).getParentName().equals("")) {
//                nf.addComponent(components.elementAt(i));
//            }                
//        }
//             
//        //add for mobile header
//        if (nf.getStyle()!=null) {
//        	String fheader =nf.getStyle().getInternalObject().getData("style").getData("n-header").toString();
//        	if (fheader.equals("default")) {
//        		
//        	}else if (fheader.length()>=1) {
//				NForm nheader = new NForm(fheader, Nset.newObject().setData("+FORMTITLE", logicform.getText(0, "formtitle")).setData("+FORMNAME", logicform.getText(0, "formname"))  );
//				nheader.setCaller(this);
//				nheader.setRequestCode("HEADER");
//				setHeader(nheader.getContent());			
//			}
//		}        
//       //add for mobile drawer
//        if (nf.getStyle()!=null) {
//        	String fheader =nf.getStyle().getInternalObject().getData("style").getData("n-drawer").toString();
//        	if (fheader.equals("default")) {
//        		
//        	}else if (fheader.length()>=1) {
//				NForm nheader = new NForm(fheader, Nset.newObject().setData("+FORMTITLE", logicform.getText(0, "formtitle")).setData("+FORMNAME", logicform.getText(0, "formname"))  );
//				nheader.setCaller(this);
//				nheader.setRequestCode("DRAWER");
//				setDrawer(nheader.getContent());			
//			}
//		}        
//        //set content after set header
//        setContent(nf);   
//        
//        
//        
//        //mobileActivity
//        String[]  keys = NCore.currMobileActivity.getData(getName()).getObjectKeys();
//        for (int i = 0; i < keys.length; i++) {
//			Component comp = getContent().findComponentbyName(keys[i]);
//			if (comp!=null) {
//				Nset n = NCore.currMobileActivity.getData(getName());
//				comp.setText(    n.getData(keys[i]).getData(0).toString() );//array 0 text
//				comp.setVisible( n.getData(keys[i]).getData(1).toString().equals("1") );//array 1 Visible
//				comp.setEnable(  n.getData(keys[i]).getData(2).toString().equals("1") );//array 2 Enable
//			}
//		}
//        //==================================//
//        
//        onActionLoad(); 
//	}
//	private String requestcode="";
//	private NForm caller;
//	public void setCaller(NForm caller){
//		this.caller=caller;
//	}
//	public void setRequestCode(String requestcode){
//		this.requestcode=requestcode;
//	}
//	public void setResult(Nset result){
//		setResult("", result);
//	}
//	
//	public void setResult(String rescode, Nset result){
//        if (caller!=null) {
//        	caller.onActionResult(requestcode, rescode, result);
//        	//caller.setBufferResult(requestcode, rescode, result);
//		}
//    }
//	
//	private void runBufferResult(){		
//		if (bufferResult.size()>=3) {
//			try {
//				String requestCode = (String)bufferResult.get("requestcode");
//				String resultCode = (String)bufferResult.get("resultcode");
//				Nset data = (Nset)bufferResult.get("result");
//				bufferResult.clear();
//				onActionResultNewThread(requestCode, resultCode, data);
//			} catch (Exception e) { }
//		}
//	}	
//	private Hashtable bufferResult = new Hashtable();
//	public void setBufferResult(String requestCode, String resultCode, Nset data) {
//		bufferResult.put("requestcode", requestCode);
//		bufferResult.put("resultcode", resultCode);
//		bufferResult.put("result", data);
//	}
//	
//	public void internalonCreate(Bundle savedInstanceState) {
//		onCreate(savedInstanceState);
//	}
//	protected void onCreate(Bundle savedInstanceState) {		
//		if (getContent()!=null) {
//			bfirst=true;
//			
//			if (getHeader() instanceof NikitaForm) {
//				View child = getHeader().onCreate(this);
//				getHeader().getNForm().setActivityListener(new NFormActivityListener() {
//					public NFormActivity getActivity() {						
//						return (NFormActivity) NForm.this.getActivity();
//					}
//				});				
//				MarginLayoutParams marginLayoutParams = Utility.getMarginLayoutParams(child);
//				((LinearLayout)((NFormActivity)getActivity()).getViewHeader()).addView(child, marginLayoutParams );		
//			}else  if (getContent().getStyle()!=null) {
//				if (getContent().getStyle().getInternalObject().getData("style").containsKey("n-header")) {
//					View child = Utility.getInflater(getActivity(), R.layout.header);
//					MarginLayoutParams marginLayoutParams = new MarginLayoutParams(MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.WRAP_CONTENT);
//					if (NCore.initParam.containsKey("INIT-HEADER-STYLE")) {						
//						if (NCore.initParam.getData("INIT-HEADER-STYLE").toString().contains("n-height")) {							
//							Style style = Style.createStyle(NCore.initParam.getData("INIT-HEADER-STYLE").toString());
//							marginLayoutParams.height = getContent().convertPixel(style.getInternalObject().getData("style").getData("n-height").toString());
//						}
//					}					
//					if (getContent().getStyle().getInternalObject().getData("style").containsKey("n-header-height")) {						
//						marginLayoutParams.height = getContent().convertPixel(getContent().getStyle().getInternalObject().getData("style").getData("n-header-height").toString());
//					}
//					((LinearLayout)getActivityNF().getViewHeader()).addView(child,  marginLayoutParams );		
//					
//					((TextView)child.findViewById(R.id.txtTitle)).setText(getContent().getText());
//					child.findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
//						public void onClick(View v) {
//							NForm.this.onActionResultNewThread("header", "right", Nset.newArray());		
//							
//						}
//					});		
//					
//					child.findViewById(R.id.imgLeft).setOnClickListener(new View.OnClickListener() {
//						public void onClick(View v) {
//							if (NForm.this.getDrawer() instanceof NikitaForm) {
//								NForm.this.getActivityNF().openListDrawer();
//							}else{
//								NForm.this.onActionResultNewThread("header", "left", Nset.newArray());
//							}						
//						}
//					});
//
//					if (NForm.this.getDrawer() instanceof NikitaForm || getContent().getStyle().getInternalObject().getData("style").getData("n-header-left").toString().equals("true")) {
//						child.findViewById(R.id.imgLeft).setVisibility(View.VISIBLE);
//					}else{
//						child.findViewById(R.id.imgLeft).setVisibility(View.GONE);
//					}
//					if (getContent().getStyle().getInternalObject().getData("style").getData("n-closable").toString().equals("false")) {
//						child.findViewById(R.id.imgRight).setVisibility(View.GONE);
//					}else{
//						child.findViewById(R.id.imgRight).setVisibility(View.VISIBLE);
//					}
//					
//				}	
//				
//				
//			}
//			
//			 
//		 		
//			
//			LinearLayout content = ((LinearLayout)getActivityNF().getViewContentScroll());
//			if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-scroll:false")) {
//				content = ((LinearLayout)getActivityNF().getViewContent());
//			}
//			if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-orientation:potrait")) {
//				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//			}else if (getContent().getStyle()!=null &&  getContent().getStyle().getViewStyle().contains("n-orientation:landscape")) {
//				getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//			}
//			if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-softinputmode:hidden")) {
//				getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//			}
//
//			View child = getContent().onCreate(this);
//			content.addView(child, Utility.getMarginLayoutParams(child, MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.MATCH_PARENT) );	
//			
//			
//			if (getDrawer() instanceof NikitaForm) {
//				getActivityNF().switchtodrawer();
//				 				
//				
//				child = getDrawer().onCreate(this);
//				getDrawer().getNForm().setActivityListener(new NFormActivityListener() {
//					public NFormActivity getActivity() {						
//						return NForm.this.getActivityNF();
//					}
//				});				
//				getActivityNF().setListDrawer(0, child);
//			}
//			
//			
//			runBufferResult();
//			
//			busy=false;//??
//		}	
//	}
//	
//	private NikitaForm content;
//	public void setContent(NikitaForm w){
//        content=w;
//    }
//	
//    public NikitaForm getContent(){
//        return content;
//    }
//    
//    private NikitaForm header;
//	public void setHeader(NikitaForm w){
//		header=w;
//    }
//	
//    public NikitaForm getHeader(){
//        return header;
//    }
//    
//    private NikitaForm drawer;
//   	public void setDrawer(NikitaForm w){
//   		drawer=w;
//       }
//   	
//       public NikitaForm getDrawer(){
//           return drawer;
//       }
//    
//    
//    public void runOnUI(Runnable run){
//		if (getActivity()!=null && !getActivity().isFinishing()) {
//			getActivity().runOnUiThread(run);
//		}
//	}
//	public static void runOnNewThread(Runnable run){
//		 new Thread(run).start();
//	}
//	public void runOnAsynTaskWithBusy(final Runnable runpro){
//		 
//		new Thread(new Runnable() {
//			public void run() {
//				showBusy(true);
//				runpro.run();;	
//				showBusy(false);
//			}
//		}).start();
//	}
//	
//	private void onActionLoad() {
//		//running on nonuithread
//		if (getContent()!=null) {
//			for (int i = 0; i < navLoad.size(); i++) {
//				navLoad.elementAt(i).runRoute(); 
//			}
//		}
//	} 
//	
//    protected void onSaveState(Bundle outState) {
//    	if (getContent()!=null) {
//    		for (int i = 0; i < getContent().getComponentCount(); i++) {
//        		getContent().getComponent(i).onSaveState();
//    		}
//		}    		 
//	}
//	 
//	protected void onRestoreState(Bundle savedInstanceState) {}
//	
//	public void onActionResult(String requestCode, String resultCode, Nset data) {
//		setVirtualRegistered("@+RESULT", data);
//		setVirtualRegistered("@+REQUESTCODE", requestCode);
//		setVirtualRegistered("@+RESPONSECODE", resultCode);
//		
//		//add for finder 13/02/2015
//        if (requestCode.equals("FINDER") && resultCode.equals("FINDER")) {
//           String[]  keys = data.getObjectKeys();
//           for (int i = 0; i < keys.length; i++) {
//                String key = keys[i];//key/component/variable
//                String res = data.getData(key).toString();//data
//                if (key.equals("")) {                                
//                }else if (key.startsWith("@")) {
//                    setVirtual(key, res);
//                }else{
//                    key = (key.startsWith("$")?"":"$")+key;
//                    Component c = getComponent(key);
//                    c.setText(res);
//                    if (key.contains(c.getName())) {
//                        refreshComponent(c);
//                    }
//                }        
//            }
//        } //end   
//		
//		if (getContent()!=null) {
//			for (int i = 0; i < navResult.size(); i++) {
//				navResult.elementAt(i).runRoute();
//			}
//		}
//	} 
//	public void onActionResultNewThread(final String requestCode, final String resultCode, final Nset data) {
//		runOnNewThread(new Runnable() {
//			public void run() {
//				onActionResult(requestCode, resultCode, data);
//			}	
//		});
//	}
//	private void runActionBack (){
//		NForm.runOnNewThread(new Runnable() {
//			public void run() {
//				for (int i = 0; i < navBack.size(); i++) {
//					navBack.elementAt(i).runRoute();
//				}							
//			}
//		});
//	}
//	protected boolean onActionBack() {
//		if (getContent()!=null) {
//			if (navBack.size()>=1) {
//				runActionBack();
//				return navBack.size()>=1;
//			}		
//		}
//		return false;
//	} 
//	public void showBusy(boolean show){
//		busy=show;
//		getActivityNF().showBusy(show);
//	}
//	public void showDialog(String title, String msg, String button1, String button2, String button3, String requestcode, Nset result){
//		dialogdata = Nset.newObject();
//		dialogdata.setData("title", title);
//		dialogdata.setData("msg", msg);
//		dialogdata.setData("button1", button1);
//		dialogdata.setData("button2", button2);
//		dialogdata.setData("button3", button3);
//		dialogdata.setData("requestcode", requestcode);
//		dialogdata.setData("result", result);
//		
//		((NFormActivity) getActivityNF()).showDialogBox();
//	}
//  
//    
//	private boolean busy = false;
//	public boolean isBusy() {
//		return busy;
//	} 
//	
//	private boolean dialog = false;
//	public boolean isDialog() {
//		return dialog;
//	} 
//	
//	private Nset dialogdata = null;
//	public Nset getDialogData() {
//		return dialogdata;
//	} 
//	public void clearDialogData() {
//		dialogdata = null;
//	} 
//	
//	
//	private Nset param ;
//	private Hashtable virtual = new Hashtable();
//	public String getParameter(String key){
//        if (param!=null) {
//        	return param.getData(key).toString();
//		}
//        return "";
//	}
//	public void setParameter(String key, String data){
//        if (param==null) {
//        	param=Nset.newObject();
//		}
//        param.setData(key, data);
//	}
//	
//	public boolean expression;
//	public void setInterupt(boolean b){
//	   setVirtualRegistered("@+INTERUPT", b?"true":"false");
//	}
//	public void setLogicClose(boolean b){
//		getVirtualString("@+INTERUPT").equals("true");
//	}
//	
//	public boolean isInterupted(){
//	   return getVirtualString("@+INTERUPT").equals("true"); 
//	}
//	private int irow = 0;
//    private int lcount = 0;
//    public void setCurrentRow(int i){
//        irow=i;
//    }
//    public void setLoopCount(int m){
//        lcount=m;
//    }    
//    public int getCurrentRow(){
//        return irow;
//    }
//    public int getLoopCount(){
//        return lcount;
//    }
//    public Component getComponent(String key){
//        if (key.contains("[")) {
//            key=key.substring(0,key.indexOf("["));
//        }
//        Component comp=null;        
//        if (getContent()==null) {
//        	comp=null;
//        }else if (key.startsWith("$#")) {
//            comp = getContent().findComponentbyId(key.substring(2));
//        }else if (key.startsWith("$")) {  
//            comp = getContent().findComponentbyName(key.substring(1));
//        }
//        //add for mobile header
//        if (getHeader()!=null && comp==null) {
//        	if (key.startsWith("$#")) {
//                comp = getHeader().findComponentbyId(key.substring(2));
//            }else if (key.startsWith("$")) {  
//                comp = getHeader().findComponentbyName(key.substring(1));
//            }
//        }
//        return comp!=null?comp:new Component(this);
//    }
//    public void setVirtualRegistered(String key, Object data){
//        if (key.startsWith("@+SESSION")) {
//        	   com.nikita.mobile.utility.Utility.getSetting(NCore.getCurrentActivity(), key, (String)data);      
//               return;
//        }else if (key.startsWith("@+COOKIE")) {
//        	 com.nikita.mobile.utility.Utility.getSetting(NCore.getCurrentActivity(), key, (String)data);      
//           return;
//        }else if (key.startsWith("@+CORE")) {
//        	 NCore.corevirtual.put(key, data);
//        	 return;
//        }else if (key.startsWith("@+")||key.startsWith("@-")) {
//        	virtual.put(key, data);  
//        }
//    }
//    public void setVirtual(String key, Object data){
//        if (key.equals("")) {
//        }else if (key.startsWith("@+SESSION")||key.startsWith("@+COOKIE")||key.startsWith("@+AUTHENTICATION")||key.startsWith("@+AUTHENTICATION")) {
//            //setVirtualRegistered(key, data);
//            com.nikita.mobile.utility.Utility.setSetting(NCore.getCurrentActivity(), key, (String)data); 
//            return;
//        }else if (key.startsWith("@+")||key.startsWith("@-")) {
//        	setVirtualRegistered(key, data);
//            return;
//        }else  if (key.startsWith("@")||key.startsWith("!")) {
//            virtual.put(key, data);  
//        }        
//    }  
//    
//	private Object getVirtualStream(String key){
//		String var = key;
//		if ( key.startsWith("!") ) {
//            if (!key.contains("[")) {
//                key=key+"[\"data\",0,\""+key.substring(1).trim()+"\"]";
//            }else if (key.contains("[")&& key.contains("[")) {
//                String index = key.substring(key.indexOf("[")+1);
//                key=key.substring(0,key.indexOf("["));
//                if (index.contains("]")) {
//                    index=index.substring(0,index.indexOf("]"));
//                }
//                key=key+"[\"data\","+index+",\""+key.substring(1).trim()+"\"]";
//            }
//        }
//		
//		if ( key.startsWith("@#") ) {
//            return key.substring(2);
//		}else if (key.startsWith("@+CORE")) {    
//			return NCore.corevirtual.get(key);
//		 }else if (key.startsWith("@+SETTING-")) {
//             return com.nikita.mobile.utility.Utility.getSetting(NCore.getCurrentActivity(), key.substring(10), "");   
//        }else if (key.startsWith("@+SESSION")) {
//              return com.nikita.mobile.utility.Utility.getSetting(NCore.getCurrentActivity(), key, "");           
//        }else if (key.startsWith("@+COOKIE")) {    
//        	  return com.nikita.mobile.utility.Utility.getSetting(NCore.getCurrentActivity(), key, "");       
//        }else if (key.startsWith("@+CHECKEDROWS")) {  
//            /*
//            key=key.substring(13);
//            if (key.startsWith(".")) {
//                key=key.substring(1);
//            }
//            if (key.startsWith("[")) {
//                key=key.substring(1);
//            }
//            if (key.endsWith("]")) {
//                key=key.substring(0, key.length()-1);
//            }
//            if (key.startsWith("$#")) {
//                Component cm = getContent().findComponentbyId(key.substring(2));
//                if (cm instanceof Tablegrid) {                    
//                    return ((Tablegrid)cm).getRowChecked();
//                }
//            }else if (key.startsWith("$")) {
//                Component cm = getContent().findComponentbyName(key.substring(1));
//                if (cm instanceof Tablegrid) {                    
//                    return ((Tablegrid)cm).getRowChecked();
//                }
//            }else if (key.length()>=2) {
//                Component cm = getContent().findComponentbyId(key);
//                if (cm instanceof Tablegrid) {                    
//                    return ((Tablegrid)cm).getRowChecked();
//                }    
//            }else if (key.equals("")) {
//                for (int i = 0; i < getContent().getComponentCount(); i++) {
//                    if (getContent().getComponent(i) instanceof Tablegrid) {                    
//                        return ((Tablegrid)getContent().getComponent(i)).getRowChecked();
//                    }
//                }     
//            }
//            */
//        }else if (key.equals("@+COREMYSQL")) {
//            return NikitaConnection.CORE_MYSQL;
//        }else if (key.equals("@+COREORACLE")) {
//            return NikitaConnection.CORE_ORACLE;
//        }else if (key.equals("@+CORESQLSERVER")) {
//            return NikitaConnection.CORE_SQLSERVER;
//        }else if (key.equals("@+CORESQLITE")||key.equals("@+CORESQLLITE")) {
//            return NikitaConnection.CORE_SQLITE; 
//        }else if (key.equals("@+BUTTON1")) {
//            return "button1";
//        }else if (key.equals("@+BUTTON2")) {
//            return "button2";
//        }else if (key.startsWith("@+BUTTONGRID")) {
//            try {
//                String sact = getParameter("action");
//                if (sact.startsWith("item-")) {
//                    sact=sact.substring(5);
//                    if (sact.contains("-")) {
//                        sact=sact.substring(sact.indexOf("-")+1);
//                    }else{
//                        sact="";
//                    }
//                }else{
//                    sact="";
//                }
//                return sact;    
//            } catch (Exception e) {}
//            return "" ;
//        }else if (key.equals("@+ENTER")) {
//            return "\r\n";
//        }else if (key.equals("@+SPACE")) {
//            return " ";
//        }else if (key.equals("@+SPACE32")) {
//            return "                                ";
//        }else if (key.equals("@+TAB")) {
//            return "\t";
//        }else if (key.equals("@+EMPTYSTRING")) {
//            return "";
//        }else if (key.equals("@+VERSION")) {
//            return "MOBILE 1.0.13 Beta";//MOBILE DEKSTOP
//        }else if (key.equals("@+FORMTITLE")) { 
//        	
//        	return virtual.get(key);
//        }else if (key.equals("@+FORMNAME")) {         
//        	return virtual.get(key);
//        }else if (key.equals("@+DEVICEOS")) {
//              
//            return "ANDROID";    
//        }else if (key.equals("@+DEVICENAME")) {  
//            
//            return "MOBILE-ANDROID" ;
//        }else if (key.equals("@+DEVICEINFO")) {
//        	return "MOBILE-ANDROID" ;
//        }else if (key.equals("@+NOW")) {
//            return Utility.Now();
//        }else if (key.equals("@+TIME")) {
//            return System.currentTimeMillis();    
//        }else if (key.equals("@+RANDOM")) {
//            StringBuffer sb = new StringBuffer();
//            Random randomGenerator = new Random();
//            for (int idx = 1; idx <= 16; ++idx){
//                sb.append( randomGenerator.nextInt(100)  );
//            }
//            return sb.toString();
//        }else if (key.equals("@+FILESEPARATOR")) {    
//            return  "/";
//        }else if (key.equals("@+UNIQUE")) {    
//            return  System.currentTimeMillis()+"";
//        }else if (key.startsWith("$")) {
//            if (getContent()!=null) {
//                Component comp ; String ky = "";
//                if (key.contains("[")&& key.contains("]")) {
//                    ky=key.substring( key.indexOf("["));
//                    key=key.substring(0,  key.indexOf("["));
//                    
//                    ky=Utility.replace(ky, "[", "");
//                    ky=Utility.replace(ky, "]", "");
//                    ky=Utility.replace(ky, "\"", "");
//                }
//                
//                ky=ky.trim();
//                key=key.trim();
//                        
//                if (key.startsWith("$#")) {
//                    comp = getContent().findComponentbyId(key.substring(2));
//                }else{   
//                    comp = getContent().findComponentbyName(key.substring(1));
//                }
//                
//                if (comp==null) {
//                    comp= new Component(this);
//                }
//                
//                if (ky.equals("tag")) {
//                    return  comp.getTag();
//                }else if (ky.equals("visible")) {
//                    return comp.isVisible()?"true":"false";
//                }else if (ky.equals("enable")) {
//                    return comp.isEnable()?"true":"false";   
//                }else if (ky.equals("id")) {
//                    return comp.getId();  
//                }else if (ky.equals("name")) {
//                    return comp.getName();  
//                }else if (ky.equals("data")) {
//                    return comp.getData()!=null?comp.getData().toJSON():"";  
//                }else if (ky.equals("style")) {
//                    return comp.getViewStyle();  
//                }else if (ky.equals("class")) {
//                    return comp.getViewClass();
//                }else if (ky.equals("attribut")) {
//                    return comp.getViewAttribut();
//                }
//                return comp.getText();
//            }
//        }else if (key.startsWith("@+")||key.startsWith("@-")) {
//        	 return virtual.get(key);
//        }else if (key.startsWith("@")||key.startsWith("!")) {
//            String stream ="";
//            if ( key.contains("[")) {                
//                stream=key.substring( key.indexOf("["));
//                key=key.substring(0,  key.indexOf("["));
//            } 
//             
//            key=key.trim();
//            stream=stream.trim();
//            Object obj = virtual.get(key);
//            if (key.equals("@")) {
//            	obj = "";
//			}            
//            if (obj==null) {
//                try {
//                    return getParameter(key.substring(1));    
//                } catch (Exception e) {} 
//            }  else if (obj instanceof  Nset && !stream.equals("")) {
//                stream=runArrayStream(stream);
//                return ((Nset)obj).get(stream);
//            }  else if (obj instanceof  Nikitaset && !stream.equals("")) {
//                stream=runArrayStream(stream);
//                return ((Nikitaset)obj).getStream(stream);
//            }  else if (obj instanceof  String && !stream.equals("")) {
//                stream=runArrayStream(stream);
//                return StringAction.getStringStream(((String)obj), stream);
//            }          
//            return obj;        
//        }else if (key.startsWith("&")) {
//            try {
//                return getParameter(key.substring(1));    
//            } catch (Exception e) {}
//        }else if (key.startsWith("!")||key.startsWith("'")) {
//            return key.substring(1);
//        }
//        return var;
//	}
//	private Nset fillStreamNset(Nset stream){
//        if (stream.isNsetArray()) {
//            Nset out = Nset.newArray();
//            for (int i = 0; i < stream.getArraySize(); i++) {
//                out.addData(  fillStreamNset(stream.getData(i))  );                
//            }
//            return out;
//        }else if (stream.isNsetObject()) {
//            Nset out = Nset.newObject();
//            String[] keys = stream.getObjectKeys();
//            for (int i = 0; i < keys.length; i++) {
//                out.setData(keys[i], fillStreamNset(stream.getData(keys[i])) );
//            }
//            return out;
//        }else{
//            if (stream.toString().startsWith("@")||stream.toString().startsWith("$")||stream.toString().startsWith("!")) {
//                Object obj = getVirtual(stream.toString());
//                if (obj instanceof Nset) {
//                    obj =  ((Nset)obj)  ;                        
//                }else if (obj instanceof String) {
//                    obj =  (String)obj  ;
//                }else if (obj instanceof Double) {
//                    obj =    (Double)obj  ;
//                }else if (obj instanceof Integer) {
//                    obj =   (Integer)obj  ;
//                }else if (obj instanceof Long) {
//                    obj =  (Long)obj  ;
//                }else if (obj instanceof Boolean) {
//                    obj =  (Boolean)obj  ;
//                }else if (obj instanceof Vector) {  
//                    obj = (((Vector)obj))   ;   
//                }else if (obj instanceof Hashtable) {  
//                    obj =  (((Hashtable)obj))  ;
//                }else{
//                    obj = "";
//                }       
//                return new Nset(obj);
//            }else {  
//                return stream;
//            }     
//        }        
//    }
//	private String runArrayStream(String stream){
//        if (stream.contains("@")||stream.contains("$")||stream.contains("!")) {
//            Nset n = Nset.readJSON(stream);
//            Nset out = Nset.newArray();
//            for (int i = 0; i < n.getArraySize(); i++) {
//                if (n.getData(i).toString().startsWith("@")||n.getData(i).toString().startsWith("$")||n.getData(i).toString().startsWith("!")) {
//                    Object obj = getVirtual(n.getData(i).toString());
//                    if (obj instanceof Nset) {
//                        out.addData(  (Nset)obj  );                        
//                    }else if (obj instanceof String) {
//                        out.addData(  (String)obj  );
//                    }else if (obj instanceof Double) {
//                        out.addData(  (Double)obj  );
//                    }else if (obj instanceof Integer) {
//                        out.addData(  (Integer)obj  );
//                    }else if (obj instanceof Long) {
//                        out.addData(  (Long)obj  );
//                    }else if (obj instanceof Boolean) {
//                        out.addData(  (Boolean)obj  );
//                    }else if (obj instanceof Vector) {  
//                        out.addData(  (Vector)obj  );   
//                    }else if (obj instanceof Hashtable) {  
//                        out.addData(  (Hashtable)obj  );
//                    }else if (obj == null) {  
//                        out.addData(  n.getData(i).toString()  );
//                    }else{
//                        out.addData(  obj.toString()  );
//                    }
//                }else {  
//                    out.addData(n.getData(i).toString());
//                }                
//            }      
//            return out.toJSON();
//        }
//        return stream;
//    }
//	public Object getVirtual(String key){
//		if ( (key.startsWith("@[") && key.endsWith("]") ) || (key.startsWith("@{") && key.endsWith("}"))) {
//			return Nset.readJSON(key.substring(1));
//		}
//		 
//		
//		String f = "";
//        if ( (key.startsWith("!") && key.contains("(") ) || (key.startsWith("@") && key.contains("(") ) || (key.startsWith("$") && key.contains("("))) {
//            f = key.substring(key.lastIndexOf("(")+1);//yang terakhir
//            if (key.contains(")")) {
//                f=f.substring(0,f.indexOf(")"));
//            }
//            key=key.substring(0,key.indexOf("("));
//        }   
//        f=f.trim();
//         
//        
//        Object reObject = getVirtualStream(key);
//        if (f.equals("integer")||f.equals("int")||f.equals("inc")||f.equals("dec")) {
//            int inc = f.equals("inc")?1:0;  inc = f.equals("dec")?-1:inc;
//            
//            if (reObject instanceof String) {               
//                return Utility.getInt((String)reObject);
//            }else if (reObject instanceof Integer) {
//                return (Integer)reObject;
//            }else if (reObject instanceof Long) {
//                return (Integer)reObject;
//            }else if (reObject instanceof Double) {
//                return Utility.getInt(  (Double)reObject +"");
//            }else if (reObject == null) {
//                return 0;
//            }
//            return Utility.getInt(reObject.toString());
//        }else if (f.equals("curr")||f.equals("fcurrview")) { 
//            if (reObject instanceof String) {
//                return Utility.formatCurrency((String)reObject);
//            }else if (reObject instanceof Integer) {
//                return Utility.formatCurrency(((Integer)reObject)+"");
//            }else if (reObject instanceof Long) {
//                return Utility.formatCurrency(((Integer)reObject)+"");
//            }else if (reObject instanceof Double) {
//                return Utility.formatCurrency(((Double)reObject)+"");
//            }else if (reObject == null) {
//                return 0;
//            }
//            return Utility.formatCurrency((reObject.toString())) ;
//        }else if (f.equals("num")) { 
//            if (reObject instanceof String) {
//                return Utility.getNumber((String)reObject);
//            }else if (reObject instanceof Integer) {
//                return Utility.getNumber(((Integer)reObject)+"");
//            }else if (reObject instanceof Long) {
//                return Utility.getNumber(((Integer)reObject)+"");
//            }else if (reObject instanceof Double) {
//                return Utility.getNumber(((Double)reObject)+"");
//            }else if (reObject == null) {
//                return 0;
//            }
//            return Utility.getNumber((reObject.toString())) ;
//        }else if (f.equals("name")) {
//            return key;
//        }else if (f.equals("exec")) {
//        	String key2 = getVirtualString(key);
//            if (key2.startsWith("@")||key2.startsWith("$")) {
//                return getVirtual(key2);
//            }
//            return "";
//        }else if (f.equals("long")) { 
//            if (reObject instanceof String) {
//                return Utility.getLong((String)reObject);
//            }else if (reObject instanceof Integer) {
//                return (Integer)reObject;
//            }else if (reObject instanceof Long) {
//                return (Long)reObject;
//            }else if (reObject instanceof Double) {
//                return Utility.getLong(  (Double)reObject +"");
//            }else if (reObject == null) {
//                return 0;
//            }
//            return Utility.getLong(reObject.toString());
//        }else if (f.equals("double")) { 
//            if (reObject instanceof String) {
//                return Utility.getDouble((String)reObject);
//            }else if (reObject instanceof Integer) {
//                return (Integer)reObject;
//            }else if (reObject instanceof Long) {
//                return (Integer)reObject;
//            }else if (reObject instanceof Double) {
//                return (Double)reObject;
//            }else if (reObject == null) {
//                return 0;
//            }
//            return Utility.getDouble(reObject.toString());
//        }else if (f.equals("json")) {
//            if (reObject instanceof String) {
//                return  Nset.newArray().addData((String)reObject).toJSON();
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset().toJSON() ;
//            }else if (reObject instanceof Nset) {
//                return  ((Nset)reObject).toJSON() ;
//            }
//        }else if (f.equals("csv")||f.equals("comma")) {
//            if (reObject instanceof String) {
//                return  Nset.readJSON((String)reObject).toCsv();
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset().toCsv() ;
//            }else if (reObject instanceof Nset) {
//                return  ((Nset)reObject).toCsv() ;
//            }  
//        }else if (f.equals("string")) {  
//            if (reObject instanceof String) {
//                return  (String)reObject;
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset().toString() ;
//            }else if (reObject instanceof Nset) {
//                return  ((Nset)reObject).toString() ;
//            }            
//        }else if (f.equals("trim")) {  
//            if (reObject instanceof String) {
//                return  ((String)reObject).trim();
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset().toString().trim() ;
//            }else if (reObject instanceof Nset) {
//                return  ((Nset)reObject).toString().trim() ;
//            }
//        }else if (f.startsWith("escape")||f.startsWith("unescape")||f.startsWith("encode")||f.startsWith("decode")) {  
//            String buffer = "";
//            if (reObject instanceof String) {
//                buffer =  StringEscapeUtils.escapeSql((String)reObject );
//            }else if (reObject instanceof Nikitaset) {
//                buffer =  StringEscapeUtils.escapeSql(((Nikitaset)reObject).toNset().toString()) ;
//            }else if (reObject instanceof Nset) {
//                buffer =  StringEscapeUtils.escapeSql(((Nset)reObject).toString()) ;
//            }   
//            if (f.equals("escapesql")) {
//               return  StringEscapeUtils.escapeSql(buffer) ; 
//            }else if (f.equals("escapehtml")) {
//               return  StringEscapeUtils.escapeHtml(buffer) ; 
//            }else if (f.equals("escapejs")) {
//               return  StringEscapeUtils.escapeJavaScript(buffer) ; 
//            }else if (f.equals("escapejava")) {
//               return  StringEscapeUtils.escapeCsv(buffer) ; 
//            }else if (f.equals("escapecsv")) {
//               return  StringEscapeUtils.escapeJava(buffer) ; 
//            //==============================================//
//            }else if (f.equals("unescapehtml")) {
//               return  StringEscapeUtils.unescapeHtml(buffer) ; 
//            }else if (f.equals("unescapejs")) {
//               return  StringEscapeUtils.unescapeJava(buffer) ; 
//            }else if (f.equals("unescapejava")) {
//               return  StringEscapeUtils.unescapeJavaScript(buffer) ; 
//            }else if (f.equals("unescapecsv")) {
//               return  StringEscapeUtils.unescapeCsv(buffer) ; 
//            //==============================================//
//            }else if (f.equals("encodeurl")) {
//               return URLEncoder.encode( buffer   )   ; 
//            }else if (f.equals("decodeurl")) {
//                return URLDecoder.decode(buffer   )   ; 
//            }else if (f.equals("encodebase64")) {                 
//               return  Base64Coder.encodeString(buffer)  ; 
//            }else if (f.equals("decodebase64")) {
//               return   Base64Coder.decodeString(buffer)  ; 
//            }
//        }else if (f.equals("md5")) {  
//            if (reObject instanceof String) {
//                return  Utility.MD5((String)reObject);
//            }else if (reObject instanceof Nikitaset) {
//                return  Utility.MD5(((Nikitaset)reObject).toNset().toString()) ;
//            }else if (reObject instanceof Nset) {
//                return  Utility.MD5(((Nset)reObject).toString()) ;
//            }
//        }else if (f.equals("nset")) {
//            if (reObject instanceof String) {
//                return  Nset.readJSON((String)reObject);
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset() ;
//            }else if (reObject instanceof Nset) {
//                return   ((Nset)reObject)  ;
//            }
//        }else if (f.equals("filltonset")) {            
//            if (reObject instanceof String) {
//                return   fillStreamNset(Nset.readJSON((String)reObject));
//            }else if (reObject instanceof Nikitaset) {
//                return   fillStreamNset(((Nikitaset)reObject).toNset()) ;
//            }else if (reObject instanceof Nset) {
//                return   fillStreamNset((Nset)reObject)  ;
//            }        
//        }else if (f.equals("nikitaset")) {
//            if (reObject instanceof String) {
//                Nset n = Nset.readJSON((String)reObject);
//                if (Nikitaset.isNikitaset(n)) {
//                    return new Nikitaset(n);
//                }else{
//                    return  n;
//                }
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).toNset() ;
//            }else if (reObject instanceof Nset) {
//                Nset n =  ((Nset)reObject) ;
//                if (Nikitaset.isNikitaset(n)) {
//                    return new Nikitaset(n);
//                }else{
//                    return  n;
//                }
//            }
//        }else if (f.equals("type")) {
//            if (reObject instanceof String) {
//                return  "string";
//            }else if (reObject instanceof Nikitaset) {
//                return  "nikitaset" ;
//            }else if (reObject instanceof Nset) {
//                return  "nset"  ;
//            }else if (reObject instanceof Integer) {
//                return  "integer"  ;
//            }else if (reObject instanceof Long) {
//                return  "long"  ;
//            }else if (reObject instanceof Double) {
//                return  "double"  ;     
//            }else {
//                return  ""  ;     
//            }
//        }else if (f.equals("newarray")) {
//            Nset n =Nset.newArray();
//            setVirtual(key, n);
//            return n;
//        }else if (f.equals("newobject")) { 
//            Nset n =Nset.newObject();
//            setVirtual(key, n);
//            return n;
//        }else if (f.equals("newstring")) { 
//            setVirtual(key, "");
//            return "";
//        }else if (f.equals("newint")||f.equals("newlong")||f.equals("newfloat")||f.equals("newdecimal")||f.equals("newdouble")) { 
//            setVirtual(key, 0);
//            return 0;
//        }else if (f.equals("fdate")||f.equals("fdatenumber")||f.equals("fdateint")||f.equals("fdatelong")) {
//            if (reObject instanceof String) {
//                return Utility.getDateTime((String)reObject);
//            }else if (reObject instanceof Long) {
//                return  (Long)reObject;
//            }
//        }else if (f.equals("fdateview")) {
//            if (reObject instanceof String) {
//                long l = Utility.getDate((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "dd/MM/yyyy");
//                }
//                //return DateFormatAction.FormatDate(-1, (String)reObject, "");
//            }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "dd/MM/yyyy");
//                }
//            }
//        }else if (f.equals("fdatetimeview")) { 
//            if (reObject instanceof String) {
//                long l = Utility.getDateTime((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "dd/MM/yyyy HH:mm:ss");
//                }
//            }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "dd/MM/yyyy HH:mm:ss");
//                }
//            }
//            //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
//        }else if (f.equals("fdatedb")||f.equals("todate")) {   
//            if (reObject instanceof String) {
//                long l = Utility.getDate((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd");
//                }
//            }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd");
//                }
//            }
//            //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
//        }else if (f.equals("fdatetimedb")||f.equals("todatetime")) {   
//            //yyyy-mm-dd hh:nn:ss
//            if (reObject instanceof String) {
//                long l = Utility.getDateTime((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd HH:mm:ss");
//                }
//             }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd HH:mm:ss");
//                }
//            }
//       //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
//        }else if (f.equals("fdatetmaxdb")||f.equals("todatetmax")) {   
//            //yyyy-mm-dd hh:nn:ss
//            if (reObject instanceof String) {
//                long l = Utility.getDateTime((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + " 23:59:59";
//                }
//             }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + " 23:59:59";
//                }
//            }
//        }else if (f.equals("fdatetnowdb")||f.equals("todatetnow")) {   
//            //yyyy-mm-dd hh:nn:ss
//            if (reObject instanceof String) {
//                long l = Utility.getDateTime((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + Utility.Now().substring(10);
//                }
//             }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + Utility.Now().substring(10);
//                }
//            }
//        }else if (f.equals("fdatetmmindb")||f.equals("todatetmin")) {   
//            //yyyy-mm-dd hh:nn:ss
//            if (reObject instanceof String) {
//                long l = Utility.getDateTime((String)reObject);
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + " 00:00:00";
//                }
//             }else if (reObject instanceof Long) {
//                long l = (Long)reObject;
//                if (l!=0) {
//                    return Utility.formatDate(l, "yyyy-MM-dd") + " 00:00:00";
//                }
//            }        
//        }else if (f.equals("length")||f.equals("rows")) {
//            if (reObject instanceof String) {
//                return  ((String)reObject).length();
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).getRows();
//            }else if (reObject instanceof Nset) {
//                return  ((Nset)reObject).getArraySize()>=0 ?((Nset)reObject).getArraySize():((Nset)reObject).getObjectKeys().length  ;
//            }else if (reObject instanceof Integer) {
//                return  (""+((Integer)reObject) ).length();
//            }else if (reObject instanceof Long) {
//                return  (""+((Long)reObject) ).length();
//            }else if (reObject instanceof Double) {
//                return  (""+((Double)reObject) ).length();    
//            }else {
//                return  "0"  ;     
//            }
//        }else if (f.equals("error")) {
//            if (reObject instanceof String) {
//                return  "";
//            }else if (reObject instanceof Nikitaset) {
//                return  ((Nikitaset)reObject).getError();
//            }else if (reObject instanceof Nset) {
//                return "";
//            }else{
//                return  ""  ;      
//            }
//        }else if (f.equals("header")||f.equals("headernset")) {
//            if (reObject instanceof Nikitaset) {
//                return  new Nset(((Nikitaset)reObject).getDataAllHeader());
//            }else{
//                return  Nset.newArray() ;      
//            }
//        }else if (f.equals("data")||f.equals("datanset")) {
//            if (reObject instanceof Nikitaset) {
//                return  new Nset(((Nikitaset)reObject).getDataAllVector());
//            }else{
//                return  Nset.newArray()  ;      
//            }
//        }
//        return reObject;
//	}
//	public String getVirtualString(String key){
//        Object obj = getVirtual(key);
//        return obj!=null?obj.toString():"";
//	}
//	public boolean isComponent(String key){
//	        return key.startsWith("$");
//    }
//    public boolean isVirtual(String key){
//        return key.startsWith("@");
//    }   
//	    
}
