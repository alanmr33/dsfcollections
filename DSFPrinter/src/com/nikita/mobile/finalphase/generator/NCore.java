package com.nikita.mobile.finalphase.generator;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.component.IExpression;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

import android.app.Activity;
import android.util.Log;

 

public class NCore {
	public static Vector<NForm> forms = new Vector<NForm>();
	public static NForm currform;
	public static Nset currMobileActivity = Nset.newObject();
	public static NFormActivity curractivity;
	private static Nset initParam = Nset.newObject();	 
	private static Hashtable corevirtual = new Hashtable();
	public static long lastActionActivity = 0;
	
	public static void actionActivityTimeout(){
		lastActionActivity=System.currentTimeMillis();
	}
	
	public static String getCoreVirtual(String key) {
		if (  corevirtual.get(key) instanceof String ) {
			return (String)corevirtual.get(key);
		}
		return "";
	}
	//mobile only
	public static void saveFileComponent() {
		for (int i = 0; i < forms.size(); i++) {
			//forms.elementAt(i).saveFileComponent();
		} 	
	}
	
	public static void removeAllForm() {
		forms.removeAllElements();
	}
	
	public static Nset getMobileActivityStream(String...fname){
		//{"fname":{components}}
		Nset nset = Nset.newObject();	
		if (fname!=null && fname.length>=1) {
			for (int t = 0; t < fname.length; t++) {
				String fn = fname[t];
				for (int i = 0; i < forms.size(); i++) {
					//if (forms.elementAt(i).getName().equals(fn)) {				
					//	nset.setData(forms.elementAt(i).getName(), forms.elementAt(i).getMobileActivityStream());
					//	break;
					//}
				} 
			}			
		}else{
			for (int i = 0; i < forms.size(); i++) {
				//if (!forms.elementAt(i).getName().equals("")) {
				//	nset.setData(forms.elementAt(i).getName(), forms.elementAt(i).getMobileActivityStream());
				//}				
			} 
		}
		return nset;
	}
	
	private static Nset mobileactivityactive = Nset.newObject();
	public static Nset getMobileActivityActive(){
		Nset nset = Nset.newObject();	
		String[]  key = mobileactivityactive.getObjectKeys();
		for (int i = 0; i < key.length; i++) {
			nset.setData(key[i], mobileactivityactive.getData(key[i]) );
		}
		
		for (int i = 0; i < forms.size(); i++) {
			//if (!forms.elementAt(i).getName().equals("")) {
			//	nset.setData(forms.elementAt(i).getName(), forms.elementAt(i).getMobileActivityStream());
			//}				
		} 
		//init
		
		return nset;
	}	
	public static void setMobileActivityActive(Nset mobile){
		mobileactivityactive=mobile;
		currMobileActivity=Nset.newObject();
		String[]  key = mobileactivityactive.getObjectKeys();
		for (int i = 0; i < key.length; i++) {
			currMobileActivity.setData(key[i], mobileactivityactive.getData(key[i]) );
		}	
	}
	
	
	public static Nset getMobileActivityStream(){
		return currMobileActivity;
	}
	public static void clearMobileActivityStream(String...fname){
		if (fname!=null && fname.length>=1) {
			for (int t = 0; t < fname.length; t++) {
				String fn = fname[t];
				currMobileActivity.setData(fn, Nset.newObject());
			}		
		}else{
			currMobileActivity = Nset.newObject();//clearall
		}
	}
	public static void setMobileActivityStream(String json){
		currMobileActivity = Nset.readJSON(json);
	}
	public static void setMobileActivityStream(NForm form){
	//	currMobileActivity.setData(form.getName(), form.getMobileActivityStream());
	}
	public static void saveMobileActivityStream(String...fname){
		if (fname!=null && fname.length>=1) {
			for (int t = 0; t < fname.length; t++) {
				String fn = fname[t];
				for (int i = 0; i < forms.size(); i++) {
					//if (forms.elementAt(i).getName().equals(fn)) {				
					//	currMobileActivity.setData(forms.elementAt(i).getName(), forms.elementAt(i).getMobileActivityStream());
					//	break;
					//}
				} 
			}		
		}else{
			currMobileActivity = Nset.newObject();//clearall
		}
	}
	
	
	/*
	public static NForm getInstanceForm(String fname, String instance) {
		return NCore.getInstanceForm(fname, instance, Nset.newObject());
	}
	
	public static NForm findInstanceForm(String instancename) {
		for (int i = 0; i < forms.size(); i++) {
			if (forms.elementAt(i).getInstanceName().equals(instancename)) {				
				return forms.elementAt(i);
			}
		} 
		return null;		
	}
	
	public static NForm getInstanceForm(String fname, String instance, Nset param) {
		for (int i = 0; i < forms.size(); i++) {
			if (forms.elementAt(i).getInstanceName().equals(fname +"-"+instance)) {				
				return forms.elementAt(i);
			}
		}
		NForm nForm = new NForm(fname, param);
		nForm.setInstance(instance);
		
		forms.addElement(nForm);
		return nForm;		
	}
	public static NForm getInstanceFormNF(String fname, String instance, Nset param) {
		for (int i = 0; i < forms.size(); i++) {
			if (forms.elementAt(i).getInstanceName().equals(fname +"-"+instance)) {				
				return forms.elementAt(i);
			}
		}
		
		NForm nForm;
		if (fname.startsWith("com.")) {
			try {
				nForm=(NForm)Class.forName(fname).newInstance();
				nForm.setArgument(param);			
			} catch (Exception e) {
				nForm = new NForm(fname, param);
			}		 
		}else{
			nForm = new NForm(fname, param);
		}
		
		nForm.setInstance(instance);
		
		forms.addElement(nForm);
		return nForm;	
	}
	
	public static NForm getCurrentForm() {
		return currform;
	}
	
	public static void setCurrentActivity(NFormActivity activity) {
		curractivity = activity;
	}
	public static NFormActivity getCurrentActivity() {
		return curractivity ;
	}
	public static void setCurrentForm(NForm form) {
		currform = form;
	}
 
	public static void removeAllForm() {
		forms.removeAllElements();
	}
	public static void clearTempFile() {
		 try {
			File[] files = new File(Utility.getDefaultPath()).listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().endsWith(".tmp")) {
					try {
						files[i].delete();
					} catch (Exception e) { }
				}
			}
		} catch (Exception e) { }
	}
	public static void showForm(String fname, String instance, Nset parameter){
		for (int i = 0; i < forms.size(); i++) {
			if (forms.elementAt(i).toString().equals("")) {
				
				return;
			}
		}
		NForm nForm = new NForm(fname);
		forms.addElement(nForm);
		
		
		
		
	}
	*/
	public static boolean removeForm(String fname, String instance){
		for (int i = 0; i < forms.size(); i++) {
			//if (forms.elementAt(i).getInstanceName().equals(fname+"-"+instance)) {
			//	forms.removeElementAt(i);
			//	return true;
			//}
		}		
		return false;
	}
	 
	
	public static NCore getInstance(){
		return new NCore();
	}
	 
	public static void addQueue(Activity activity){
		
	}   
	
	 
}
