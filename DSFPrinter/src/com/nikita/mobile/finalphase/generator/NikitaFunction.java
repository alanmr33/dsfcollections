package com.nikita.mobile.finalphase.generator;

import com.rkrzmail.nikita.data.Nset;

public class NikitaFunction {
	public interface OnVariableListener{
		public Object getValue(String var);
	}
	public static Object evaluate(String array, OnVariableListener variableListener ){
		if (array.contains("[")) {
			array = array.substring(array.indexOf("["));
		}
		if (array.contains("]")) {
			array = array.substring(0, array.lastIndexOf("]"));
			
			Nset nv = Nset.readJSON(array);
			Object[] object = new Object[Math.max(0, nv.getSize()-1)];
			for (int i = 1; i < nv.getSize(); i++) {
				object[i-1] = variableListener.getValue(nv.getData(i).toString());
			}
			return evaluateFunction(nv.getData(0).toString(), object);
		}	
		return null;
	}
	
	private static Object evaluateFunction(String fname, Object...arg){
		if (fname.equalsIgnoreCase("exec")) {
			
		}else if (fname.equalsIgnoreCase("format")) {
			
		}
		return null;
	}
}
