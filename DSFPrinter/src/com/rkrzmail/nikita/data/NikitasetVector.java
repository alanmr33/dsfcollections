package com.rkrzmail.nikita.data;

import java.util.Vector;

public class NikitasetVector extends Vector<Vector<String>>{
    private Nikitaset nikitaset;
    
	public NikitasetVector(Nikitaset nikitaset){
		this.nikitaset=nikitaset;
	}
	
	public synchronized int size() {		
		return nikitaset.getRows();
	}
	 
	public synchronized Vector<String> elementAt(int location) {		 
		return nikitaset.getDataAllVector().elementAt(location);
	}

	public Vector<String> get(int location) {		 
		return elementAt(location);
	}
}
