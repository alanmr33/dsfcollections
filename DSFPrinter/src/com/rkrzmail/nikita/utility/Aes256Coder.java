/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rkrzmail.nikita.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import com.rkrzmail.nikita.utility.Base64Coder;

/**
 *
 * @author vincent@indocyber.co.id
 * 
 */
public class Aes256Coder {
    
	public static final byte[] encBytes(byte[] srcBytes, byte[] key, byte[] newIv) throws Exception {
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		IvParameterSpec initVector = new IvParameterSpec(newIv);
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");		
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, initVector);
			byte[] encrypted = cipher.doFinal(srcBytes);
			
			return encrypted;
		} catch (Exception e) {
			throw new RuntimeException(e);			
		}
	}
	
	public static final String encText(String sSrc, byte[] key, byte[] newIv) throws Exception {
		byte[] srcBytes = sSrc.getBytes("UTF-8");
		byte[] encrypted = encBytes(srcBytes, key, newIv);
		
		return new String(Base64Coder.encode(encrypted));
	}
	
	public static final byte[] decBytes(byte[] srcBytes, byte[] key, byte[] newIv) throws Exception {
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		IvParameterSpec initVector = new IvParameterSpec(newIv);
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey, initVector);
			byte[] encrypted = cipher.doFinal(srcBytes);
			
			return encrypted;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static final String decText(String sSrc, byte[] key, byte[] newIv) throws Exception {
		byte[] srcBytes = Base64Coder.decode(sSrc);
		byte[] decrypted = decBytes(srcBytes, key, newIv);
		
		return new String(decrypted, "UTF-8");
	}
	/*
	key 16 digit
	*/
	public static String encryptText(String s, String key) {
		byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		try {
			return encText(s, key.getBytes("UTF-8"), ivk);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static String decryptText(String s, String key) {
		byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		try {
			return decText(s, key.getBytes("UTF-8"), ivk);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}

	public static String generateText(String text, int counter) {
		String counterTxt = "00";
		if (counter > 100) {
		    counter = 0;
		}
		if (counter < 10) {
		    counterTxt = "0" + counter;
		} else {
		    counterTxt = String.valueOf(counter);
		}
		//String key = "mileniumimp@me" + counterTxt;
		String key = "aurorabore@l1s" + counterTxt;
		String string = encryptText(text, key);
		                 
		String regex1 = "(?i)((?:=|U\\s*R\\s*L\\s*\\()\\s*[^>]*\\s*S\\s*C\\s*R\\s*I\\s*P\\s*T\\s*:|&colon;|[\\s\\S]allowscriptaccess[\\s\\S]|[\\s\\S]src[\\s\\S]|[\\s\\S]data:text\\/html[\\s\\S]|[\\s\\S]xlink:href[\\s\\S]|[\\s\\S]base64[\\s\\S]|[\\s\\S]xmlns[\\s\\S]|[\\s\\S]xhtml[\\s\\S]|[\\s\\S]style[\\s\\S]|<style[^>]*>[\\s\\S]*?|[\\s\\S]@import[\\s\\S]|<applet[^>]*>[\\s\\S]*?|<meta[^>]*>[\\s\\S]*?|<object[^>]*>[\\s\\S]*?)";
		Pattern pattern1 = Pattern.compile(regex1);
		Matcher matcher1 = pattern1.matcher(string);
						 
		String regex2 = "(?i)([\\s\\\"\'`;\\/0-9\\=]+on\\w+\\s*=)";		
		Pattern pattern2 = Pattern.compile(regex2);
		Matcher matcher2 = pattern2.matcher(string);
		
		if (!matcher1.find() && !matcher2.find()) {
		    return string + "!" + key;
		} else {
		    return generateText(text, counter+1);
		}
    }
	
	public static String realText(String texts) {
	    String[] text = texts.split("!");
	    
	    return decryptText(text[0], text[1]);
    }
    // Dummy constructor.
    private Aes256Coder() {}
}
